<?php

namespace buwang\jobs;

use buwang\base\BaseJob;
use app\common\model\SysLog;
use buwang\service\WechatMessageService;

/**
 * Class WechatMessageJob
 * @package buwang\jobs
 */
class WechatMessageJob extends BaseJob
{
    /**
     * 发送订阅消息
     * @param mixed ...$param
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function sendSubscribeMessage(...$param): bool
    {
        WechatMessageService::sendSubscribeMessage(...$param);
        return true;
    }

}
