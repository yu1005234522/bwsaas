define(["easy-admin", "QRCode", "easy-admin"], function (ea, QRCode, ea) {
    var Controller = {
        login: function () {
            if (top.location !== self.location) {
                top.location = self.location;
            }
            ea.listen(function (data) {
                // data['keep_login'] = $('.icon-nocheck').hasClass('icon-check') ? 1 : 0;
                return data;
            }, function (res) {
                ea.common.setCookie('token', res.data.data.token, 's' + res.data.data.expires_in);
                ea.msg.success("登录成功", function () {
                    window.location = ea.url('admin/index');
                })
            }, function (res) {
                ea.msg.error(res.msg, function () {
                    $('#captchaImg').trigger("click");
                });
            });
        },
    };
    return Controller;
});