<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

/**
 * 租户应用表
 * Class MemberMiniapp
 * @package app\manage\model
 */
class MemberMiniapp extends BaseModel
{
    // 模型初始化
    protected static function init()
    {
        //TODO:初始化内容
    }

    /**
     * 关联应用
     *
     * @return \think\model\relation\HasOne
     */
    public function miniapp()
    {
        return $this->hasOne('Miniapp', 'id', 'miniapp_id');
    }

    /**
     * 应用后台所属管理员
     * @return \think\model\relation\HasOne
     */
    public function member()
    {
        return $this->hasOne('Member', 'id', 'member_id');
    }

    /**
     * 应用绑定的用户端口创始人
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne('User', 'id', 'member_id');
    }

    /**
     * 用户购买的应用
     * @return \think\model\relation\HasOne
     */
    public function order()
    {
        return $this->hasOne('MemberMiniappOrder', 'id', 'miniapp_order_id');
    }

}
