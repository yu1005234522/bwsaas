# bwsaas

#### Description
* bwsaas框架和原生小程序端（V1.0）源码已经开源，bwmall后台应用源码，请加群后联系群主获取，点星关注不迷路，第一时间收到通知
* 目前已开源：框架bwsaas（saas多租户管理系统：租户管理系统和总平台管理系统）全部开源 + bwmall(saas系统里的一个电商行业单商户b2c应用，包括拼团，秒杀，砍价，分销)+原生微信小程序前端源码

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
