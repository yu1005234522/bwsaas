<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

class WechatKeyword extends TimeModel
{
    protected $name = "wechat_keyword";
    protected $deleteTime = false;

    public function getIsMiniappList()
    {
        return ['0' => '否', '1' => '是',];
    }

    public function getTypeList()
    {
        return ['text' => '文本', 'image' => '图片',];
    }
}