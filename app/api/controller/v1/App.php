<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\api\controller\Basic;
use app\common\model\MemberMiniapp;
use app\common\model\User as ModelUser;
use buwang\exception\MiniappException;
use buwang\facade\WechatMp as Mp;
use app\common\model\Sms;
use buwang\service\UserService;
use filter\Inspect;

/**
 * h5 official app 端的公共方法
 */
class App extends Basic
{
    // 初始化
    protected function initialize()
    {
        parent::initialize();
        if (!in_array($this->request->scopes(), ['app', 'official', 'h5'])) throw new MiniappException('登录用户来源scopes错误！');
    }

    /**
     * 通用注册接口
     */
    public function commonReg()
    {
        if ($this->request->isPost()) {
            $data = [
                'mobile' => $this->request->param('mobile/s'),
                'password' => $this->request->param('password/s'),
                'captcha' => $this->request->param('captcha/d'),
                'invite_code' => $this->request->param('invite_code/s', ''),//用户推荐码
            ];
            validate(\buwang\validate\User::class)->scene('userReg')->check($data);
            $res = ModelUser::where('member_miniapp_id', $this->app_id)->where('mobile', $data['mobile'])->find();
            if ($res) return $this->error('手机号已经被注册');
            //TODO 更多的判断 验证码判断等待
            $insert_data = [
                'username' => 'bw' . $data['mobile'],
                'mobile' => $data['mobile'],
                'password' => password_hash(md5($data['password']), PASSWORD_DEFAULT),
                'status' => 1,
                'member_miniapp_id' => $this->app_id,
                'create_time' => time(),
            ];

            $is_invite = $data['invite_code'] ? ModelUser::isInvite($data['invite_code']) : false;  //查邀请码是否有效
            if ($is_invite) {
                $insert_data['pid'] = de_code($data['invite_code']);//注册用户加入上级推荐人
            }
            $last_id = ModelUser::insertGetId($insert_data);
            if (!$last_id) return $this->error('注册失败');
            if ($last_id) {
                ModelUser::where('id', $last_id)->data(['invite_code' => en_code($last_id)])->update();//每个人的邀请码是根据ID生成的可解密code
            }
            $result = ModelUser::find($last_id);
            /**-------------20200910 jyk 注册成功后创建对应应用的用户副表 start-----------------**/
            // TODO 用户注册成功后事件
            event('UserRegisterSuccess', [$result, $this->request->scopes()]);
            /**-------------20200910 jyk 注册成功后创建对应应用的用户副表 end-----------------**/

            return $this->success('注册成功', $result);

        } else {
            return $this->error('request method error');
        }
    }

    /**
     * 通用登录接口
     * @return \think\Response
     */
    public function commonLogin()
    {
        if ($this->request->isPost()) {
            $data = [
                'username' => $this->request->param('username/s'),
                'password' => $this->request->param('password/s'),
            ];

            validate(\buwang\validate\User::class)->scene('userLogin')->check($data);
            //验证密码是否正确
            $retData = UserService::validateUser($data['username'], $data['password'], $this->app_id);
            $userInfo = $retData['rs'];
            if (!$userInfo) return $this->error($retData['error']);
            $rs = UserService::loginUser($userInfo, $this->scopes, true);
            if (!$rs['rs']) return $this->error($rs['error']);
            $return_data = $rs['rs'];
            $return_data['uid'] = $userInfo['id'];
            $return_data['invite_code'] = $userInfo['invite_code'];//获取小程序用户的邀请码
            $return_data['session_id'] = get_session_id();//通用函数定义
            return $this->success($return_data);
        } else {
            return $this->error('request method error');
        }
    }

    /**
     * 获取公众号JSSDK配置
     */
    public function getJSSDK()
    {
        $app = $this->app_id;
        $config = Mp::jsApiList($app, ['hideAllNonBaseMenuItem', "getNetworkType", 'chooseWXPay']);
        return $this->success('获取公众号配置成功！', $config);
    }

    /**
     *  公众号微信授权登录API
     */
    public function officialLogin()
    {
        $app = $this->app_id;//应用id
        if (!$app) return $this->error("解析service_id对应应用信息失败");
        $open_id = $this->request->param('open_id'); //授权code
        $mobile = $this->request->param('mobile/s','');//手机号
        $invite_code = $this->request->param('invite_code/s');//推荐码
        $validate_code = $this->request->param('validate_code/s'); //验证码
        $code = $this->request->param('code'); //授权code
        if (empty($open_id) && empty($code)) {
            return $this->error('参数错误，关键参数不能为空');
        }
        //判断是否开放平台应用
        $miniapp = MemberMiniapp::where(['id' => $app])->find();
        if (!$miniapp) {
            return $this->error('未找到已授权应用');
        }

        $official = Mp::getWechatObj($app);
        if (!$official) {
            return $this->error('请先授权您的公众号');
        }
        $data['mobile'] = '';
        if ($mobile) {
            //手机号验证码
            if (!Sms::check($mobile, $validate_code, 'bind_mobile')) return $this->error('验证码错误');
            $data['mobile'] = $mobile;
        }

        if($open_id){
            $rel = $official->user->get($open_id);
            $data['unionid'] = empty($rel['unionid']) ? '' : $rel['unionid'];
            $data['official_uid'] = $rel['openid'];
            $nickName = Inspect::filter_Emoji($rel['nickname']);
            $data['avatar'] = $rel['headimgurl'];
        }else{
            $rel = $official->oauth->user();
            $original = $rel->getOriginal();//微信返回原始数据
            $data['unionid'] = empty($original['unionid']) ? '' : $original['unionid'];
            $data['official_uid'] = $rel->getId();
            $nickName = Inspect::filter_Emoji($rel->getName());
            $data['avatar'] = $rel->getAvatar();
        }

        $data['miniapp_id'] = $app;//MemberMiniapp表id
        $data['miniapp_uid'] = '';
        $data['session_key'] = '';
        $data['nickname'] = $nickName ?? 'WX-' . time();

        $data['invite_code'] = $invite_code;//后面添加推荐人用到
        $uid = ModelUser::wechatReg($data, 'official');//注册新用户或者更新用户信息
        if (!$uid) {
            if (ModelUser::getErrorCode() == 400832) {
                //缺少手机号需要重新变更手机号
                $auth = [];
                $auth_data = [];
                $auth['open_id'] = $data['official_uid'];
                $auth['nickname'] = $nickName ?? 'WX-' . time();
                $auth['avatar'] = $data['avatar'];
                $auth['invite_code'] = $invite_code;//后面添加推荐人用到
                $auth_data['user_info'] = $auth;  //应用信息
                $auth_data['type'] = 'official';  //应用信息
                return $this->code(200)->error(ModelUser::getError(), $auth_data, ModelUser::getErrorCode());
            } else {
                return $this->code(200)->error('用户认证失败！');
            }
        }
        $userInfo = ModelUser::find($uid);//得到登录用户信息
        //调用用户登录
        $rs = UserService::loginUser($userInfo, 'official', true);
        if (!$rs['rs']) return $this->error($rs['error']);
        if ($data['mobile']) Sms::clear($data['mobile'], 'bind_mobile');
        $return_data = $rs['rs'];//登录成功token信息
        $return_data['uid'] = $uid;  //用户信息
        $return_data['invite_code'] = $userInfo['invite_code'];//获取小程序用户的邀请码
        $return_data['session_id'] = get_session_id();//返回登录session
        return $this->success('登录成功', $return_data);
    }
    


}