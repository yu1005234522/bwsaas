define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/index',
        add_url: 'member/user/add',
        edit_url: 'member/user/edit',
        edit_pw_url: 'member/user/editPw',
        delete_url: 'member/user/del',
        export_url: 'member/user/export',
        modify_url: 'member/user/modify',
        change_money_url: 'member/user/changeMoney',
        send_url: 'member/message/send',
    };

    function getgetMemberMiniappList() {
        var miniappList = {};
        var select_name = $('#select_name').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }

    var Controller = {

        index: function () {
            var memberMiniappList = getgetMemberMiniappList();//应用搜索列表

            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'member_miniapp_id', title: '应用ID'},
                    // {field: 'password', title: '密码'},
                    {field: 'mobile', title: '手机号'},
                    {field: 'miniapp_uid', title: 'openid',hide:true},
                    {field: 'nickname', title: '昵称'},
                    {field: 'avatar', title: '头像', templet: ea.table.image},
                    {field: 'login_ip', title: '登录IP',hide:true},
                    {field: 'login_time', title: '登录时间', search: 'range',templet: ea.table.date,hide:true},
                    {field: 'status', search: 'select', selectList: ["锁定","正常"], tips: '正常|锁定', title: '状态', templet: ea.table.switch},
                    {field: 'integral', title: '积分',hide:true},
                    {field: 'money', title: '用户余额'},
                    // {field: 'brokerage', title: '佣金',hide:true},
                    {field: 'username', title: '用户名',hide:true},
                    {field: 'memberMiniapp.appname', search: 'select',fieldAlias:'memberMiniapp.id', selectList: memberMiniappList, title: '应用名称'},
                    {field: 'memberMiniapp.head_img', title: 'Logo', templet: ea.table.image,hide:true},
                    {field: 'memberMiniapp.miniapp_head_img', title: '小程序logo', templet: ea.table.image},
                    {field: 'memberMiniapp.mp_head_img', title: '公众号logo', templet: ea.table.image},
                    {width: 300, title: '操作', templet: ea.table.tool
                        ,operat: [
                            [{
                                text: '站内信',
                                url: init.send_url,
                                method: 'open',
                                auth: 'send',
                                class: 'layui-btn layui-btn-xs layui-btn-warm',
                            },{
                                text: '编辑',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                            }, {
                                text: '修改密码',
                                url: init.edit_pw_url,
                                method: 'open',
                                auth: 'editPw',
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                            }, {
                                text: '资金变更',
                                url: init.change_money_url,
                                method: 'open',
                                auth: 'changeMoney',
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                            }],
                            'delete']
                    },
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        editPw: function () {
            ea.listen();
        },
        changeMoney: function () {
            ea.listen();
        },
        sendMessage: function () {
            ea.listen();
        },
    };
    return Controller;
});