<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace addons\addon_demo\controller;

use buwang\base\BaseController;
use buwang\base\MemberBaseController;
use buwang\traits\PluginTrait;
use think\App;

/**
 * 控制器基础类
 */
abstract class PluginBaseController extends MemberBaseController
{
    use PluginTrait;

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->pluginConstruct($app);

        // 父类的调用必须放在设置模板路径之后
        parent::__construct($app);
        //插件访问权限控制
        $this->_initialize();
    }

    /**
     * 插件权限节点鉴权
     * @return mixed|null
     */
    final private function _initialize()
    {
        parent::initialize();
        $this->pluginInitialize();
    }
}
