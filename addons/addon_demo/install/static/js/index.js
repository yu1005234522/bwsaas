define(["jquery", "easy-admin"], function ($, ea) {
    var Controller = {
        index: function () {
            ea.listen();
        },
        login: function () {
            // 登录过期的时候，跳出ifram框架
            if (top.location != self.location) top.location = self.location;

            ea.listen(null, function (res) {
                res.msg = res.msg || '';
                ea.msg.success(res.msg, function () {
                    window.location = ea.url('index/index');
                });
                return false;
            });
        },
    };
    return Controller;
});