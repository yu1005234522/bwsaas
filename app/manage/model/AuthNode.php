<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\model;

use buwang\base\BaseModel;
use buwang\traits\JwtTrait;
use buwang\util\Util;
use think\facade\Db;

/**权限节点控制器
 * Class BwAuthNode
 * @package app\manage\model
 */
class AuthNode extends BaseModel
{
    protected $pk = 'id';
    // 表名
    protected $table = 'bw_auth_node';
    protected $updateTime = '';

    protected $deleteTime = '';

    /**
     * 返回多层栏目
     * @param $data 操作的数组
     * @param int $pid 一级PID的值
     * @param string $html 栏目名称前缀
     * @param string $fieldPri 唯一键名，如果是表则是表的主键
     * @param string $fieldPid 父ID键名
     * @param int $level 不需要传参数（执行时调用）
     * @return array
     */
    static public function channelLevel($data, $pid = 0, $html = "&nbsp;", $fieldPri = 'cid', $fieldPid = 'pid', $level = 1)
    {
        if (empty($data)) {
            return array();
        }
        $arr = array();
        foreach ($data as $v) {
            if ($v[$fieldPid] == $pid) {
                $arr[$v[$fieldPri]] = $v;
                $arr[$v[$fieldPri]]['_level'] = $level;
                $arr[$v[$fieldPri]]['_html'] = str_repeat($html, $level - 1);
                $arr[$v[$fieldPri]]["_data"] = self::channelLevel($data, $v[$fieldPri], $html, $fieldPri, $fieldPid, $level + 1);
            }
        }
        return $arr;
    }

    /**
     * 获得所有子栏目
     * @param $data 栏目数据
     * @param int $pid 操作的栏目
     * @param string $html 栏目名前字符
     * @param string $fieldPri 表主键
     * @param string $fieldPid 父id
     * @param int $level 等级
     * @return array
     */
    static public function channelList($data, $pid = 0, $html = "&nbsp;", $fieldPri = 'cid', $fieldPid = 'pid', $level = 1)
    {
        $data = self::_channelList($data, $pid, $html, $fieldPri, $fieldPid, $level);
        if (empty($data))
            return $data;
        foreach ($data as $n => $m) {
            if ($m['_level'] == 1)
                continue;
            $data[$n]['_first'] = false;
            $data[$n]['_end'] = false;
            if (!isset($data[$n - 1]) || $data[$n - 1]['_level'] != $m['_level']) {
                $data[$n]['_first'] = true;
            }
            if (isset($data[$n + 1]) && $data[$n]['_level'] > $data[$n + 1]['_level']) {
                $data[$n]['_end'] = true;
            }
        }
        //更新key为栏目主键
        $category = array();
        foreach ($data as $d) {
            $category[$d[$fieldPri]] = $d;
        }
        return $category;
    }

    //只供channelList方法使用
    static private function _channelList($data, $pid = 0, $html = "&nbsp;", $fieldPri = 'cid', $fieldPid = 'pid', $level = 1)
    {
        if (empty($data))
            return array();
        $arr = array();
        foreach ($data as $v) {
            $id = $v[$fieldPri];
            if ($v[$fieldPid] == $pid) {
                $v['_level'] = $level;
                $v['_html'] = str_repeat($html, $level - 1);
                array_push($arr, $v);
                $tmp = self::_channelList($data, $id, $html, $fieldPri, $fieldPid, $level + 1);
                $arr = array_merge($arr, $tmp);
            }
        }
        return $arr;
    }

    /**
     * 获得树状数据
     * @param $data 数据
     * @param $title 字段名
     * @param string $fieldPri 主键id
     * @param string $fieldPid 父id
     * @return array
     */
    static public function tree($data, $title, $fieldPri = 'cid', $fieldPid = 'pid')
    {
        if (!is_array($data) || empty($data))
            return array();
        $arr = self::channelList($data, 0, '', $fieldPri, $fieldPid);
        foreach ($arr as $k => $v) {
            $str = "";
            if ($v['_level'] > 2) {
                for ($i = 1; $i < $v['_level'] - 1; $i++) {
                    $str .= "&emsp;│";
                }
            }
            if ($v['_level'] != 1) {
                $t = $title ? $v[$title] : "";
                if (isset($arr[$k + 1]) && $arr[$k + 1]['_level'] >= $arr[$k]['_level']) {
                    $arr[$k]['_name'] = $str . "&emsp;└─ " . $v['_html'] . $t;
                } else {
                    $arr[$k]['_name'] = $str . "&emsp;└─ " . $v['_html'] . $t;
                }
            } else {
                $arr[$k]['_name'] = $v[$title];
            }
        }
        //设置主键为$fieldPri
        $data = array();
        foreach ($arr as $d) {
            $data[$d[$fieldPri]] = $d;
        }
        return $data;
    }

    /**
     * 获得所有父级栏目
     * @param $data 栏目数据
     * @param $sid 子栏目
     * @param string $fieldPri 唯一键名，如果是表则是表的主键
     * @param string $fieldPid 父ID键名
     * @return array
     */
    static public function parentChannel($data, $sid, $fieldPri = 'cid', $fieldPid = 'pid')
    {
        if (empty($data)) {
            return $data;
        } else {
            $arr = array();
            foreach ($data as $v) {
                if ($v[$fieldPri] == $sid) {
                    $arr[] = $v;
                    $_n = self::parentChannel($data, $v[$fieldPid], $fieldPri, $fieldPid);
                    if (!empty($_n)) {
                        $arr = array_merge($arr, $_n);
                    }
                }
            }
            return $arr;
        }
    }

    /**
     * 判断$s_cid是否是$d_cid的子栏目
     * @param $data 栏目数据
     * @param $sid 子栏目id
     * @param $pid 父栏目id
     * @param string $fieldPri 主键
     * @param string $fieldPid 父id字段
     * @return bool
     */
    static function isChild($data, $sid, $pid, $fieldPri = 'cid', $fieldPid = 'pid')
    {
        $_data = self::channelList($data, $pid, '', $fieldPri, $fieldPid);
        foreach ($_data as $c) {
            //目标栏目为源栏目的子栏目
            if ($c[$fieldPri] == $sid)
                return true;
        }
        return false;
    }

    /**
     * 检测是不否有子栏目
     * @param $data 栏目数据
     * @param $cid 要判断的栏目cid
     * @param string $fieldPid 父id表字段名
     * @return bool
     */
    static function hasChild($data, $cid, $fieldPid = 'pid')
    {
        foreach ($data as $d) {
            if ($d[$fieldPid] == $cid) return true;
        }
        return false;
    }

    /**
     * 递归实现迪卡尔乘积
     * @param $arr 操作的数组
     * @param array $tmp
     * @return array
     */
    static function descarte($arr, $tmp = array())
    {
        static $n_arr = array();
        foreach (array_shift($arr) as $v) {
            $tmp[] = $v;
            if ($arr) {
                self::descarte($arr, $tmp);
            } else {
                $n_arr[] = $tmp;
            }
            array_pop($tmp);
        }
        return $n_arr;
    }


    /**得到所有当前有效的权限和子孙权限id
     * @param $uid
     */
    public static function get_nodes($groupids)
    {
        $groupidss = array();
        //得到有效的权限
        $groups = self::where('id', 'in', $groupids)->where('status', 1)->column('id');
        foreach ($groups as $group) {
            //查询所有的有效子权限
            $groupids = self::where('pid', '=', $group)->where('status', 1)->column('id');
            if ($groupids) {
                $groupidsss = self::get_nodes($groupids);
                $groupidss = array_merge($groupidsss, $groupidss);
            }

            $groupidss[] = $group;
        }
        return $groupidss;
    }


    /**
     * 得到需要的数组件
     */
    public static function getTree($where, $pid = 0, $checked_ids = [], $disabled = true, $is_child = false)
    {
        $data = array();
        $self = new self;
        $self = $self->where($where)->order('sort desc,id desc')->field(['id', 'pid', 'title as label']);
        if ($pid) $self = $self->where('pid', $pid);


        //查询所有的分类
        $category = $self->select()->toArray();
        //echo self::getLastSql();die;//打印最后一条sql


        if (!$category) return $data;
        if ($checked_ids) {
            if (is_array($checked_ids) || is_string($checked_ids) || is_int($checked_ids)) {
                if (is_int($checked_ids)) $checked_ids = [$checked_ids];
                if (is_string($checked_ids)) $checked_ids = explode(',', $checked_ids);//字符串分割数组

            } else {
                $checked_ids = [];
            }

        }

        if (is_array($disabled) || is_string($disabled) || is_int($disabled)) {
            if (is_int($disabled)) $disabled = [$disabled];
            if (is_string($disabled)) $disabled = explode(',', $disabled);//字符串分割数组
        }
        $datas = array();
        foreach ($category as &$obj) {
            $datas[] = $obj['id'];
        }


        foreach ($category as &$obj) {
            if (!in_array($obj['pid'], $datas)) $obj['pid'] = 0;


            //是否默认选中
            //只有子节点可以被选中
            if ($is_child) {
                $count = self::where('pid', $obj['id'])->count();
//                if($obj['id'] ==1250){
//                    var_dump($count);die;
//                }
                if (!$count) {
                    if (in_array($obj['id'], $checked_ids)) $obj['checked'] = true;
                }
            } else {
                if (in_array($obj['id'], $checked_ids)) $obj['checked'] = true;
            }

            if (is_array($disabled)) {
                if (in_array($obj['id'], $disabled)) $obj['disabled'] = true;
            } elseif ($disabled === true) {
                //顶级节点禁止选
                if ($obj['pid'] == 0) $obj['disabled'] = true;
            } elseif ($disabled === false) {
                //非顶级节点禁止选
                if ($obj['pid'] != 0) $obj['disabled'] = true;
            }

        }
        //var_dump($category);die;

//
//
//        //var_dump($checked_ids);die;
//        $data= self::getTreeValue($category,$pid,$checked_ids);
//        self::reform_keys($data);

        $data = Util::tree($category, 'id', 'pid', 'children');
        return $data;
    }


    /**
     * 得到当前节点和其子孙节点ids
     */
    public static function getNodeChildIds($nodeIds)
    {
//        $idss = $nodeIds;
//        //得到屏蔽的所有子孙节点
//        foreach ($nodeIds as $id) {
//            //得到子孙节点
//            $ids = Db::query("select id from bw_auth_node   where FIND_IN_SET(id, getChildLst(:id))", ['id' => $id]);
//
//            $nodes = array();
//            foreach ($ids as $value) {
//                $nodes[] = $value['id'];
//            }
//            $idss = array_merge($idss, $nodes);
//
//        }
//        return $idss;
        return self::childIds($nodeIds);
        //
        //   where FIND_IN_SET(id, getChildLst(828));
    }

    /**
     * 得到权限标识
     */
    public static function getAuthName($data)
    {
        //权限标识 = 菜单路由 + 参数
        return substr(preg_replace("/\W/", "_", $data['menu_path'] . $data['param']),1);
    }

    /**
     * 得到当前id的子孙节点
     */
    public static function childIds($ids){
        $return_data = $ids;//返回结果
        $node_ids = $ids;//初始化子孙条件
        while($node_ids) {
            //查询该轮子孙
            $n_ids = self::where('pid','in',$node_ids)->column('id');
            //不存在结束循环
            if(!$n_ids)break;
            //存在将子孙加入结果集
            $return_data = array_merge($return_data, $n_ids);
            //将子孙作为下次查询的条件
            $node_ids = $n_ids;
        }
        return $return_data;
    }


}
