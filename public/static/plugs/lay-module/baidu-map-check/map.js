define(["jquery", "easy-admin"], function ($,ea) {

    var Controller = {
        param:{
            index : 0,
        },
        listen : function (id) {
            if(id){
                //监听按钮提示
                $('#'+id).click(function () {
                    var len = $('#'+id).attr('data-ak');//document.getElementById(botton_id).dataset.inputName;
                    var lon = $('#'+id).attr('data-lon');//document.getElementById(botton_id).dataset.inputName;
                    var lat = $('#'+id).attr('data-lat');//document.getElementById(botton_id).dataset.inputName;
                    var addr_detail = $('#'+id).attr('data-addr');//document.getElementById(botton_id).dataset.inputName;
                    var input = $('#'+id).attr('data-input');//document.getElementById(botton_id).dataset.inputName;
                    !input && (input = '');
                    !len && (len = '');
                    !lon && (lon = '');
                    !lat && (lat = '');
                    !addr_detail && (addr_detail = '');
                    Controller.param.index = ea.open('百度地图定位', '/manage/common/baiduMap?ak='+len+'&lon='+lon+'&lat='+lat+'&addr_detail='+addr_detail+'&id='+id+'&input='+input, '50%', '100%');
                });
            }else{
                //监听按钮提示
                $('.map-location').click(function () {
                    var  len = $(this).attr('data-ak');//document.getElementById(botton_id).dataset.inputName;
                    var  lon = $(this).attr('data-lon');//document.getElementById(botton_id).dataset.inputName;
                    var  lat = $(this).attr('data-lat');//document.getElementById(botton_id).dataset.inputName;
                    var  addr_detail = $(this).attr('data-addr');//document.getElementById(botton_id).dataset.inputName;
                    var input = $(this).attr('data-input');//document.getElementById(botton_id).dataset.inputName;
                    !input && (input = '');
                    !len && (len = '');
                    !lon && (lon = '');
                    !lat && (lat = '');
                    !addr_detail && (addr_detail = '');
                    Controller.param.index = ea.open('百度地图定位', '/manage/common/baiduMap?ak='+len+'&lon='+lon+'&lat='+lat+'&addr_detail='+addr_detail+'&id=0'+'&input='+input, '50%', '100%');
                });
            }

        },
        param:function () {
            body = layui.layer.getChildFrame('body', Controller.param.index);//获取子窗体 body内容
            var userName = body.contents().find("#suggestId").val();
            console.log(userName);
        }
    };
    return Controller;
});