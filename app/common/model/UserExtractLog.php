<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;
use buwang\traits\ModelTrait;

/**
 * 用户提现记录表
 * Class Systemaccount
 * @package app\common\model
 */
class UserExtractLog extends TimeModel
{
    use ModelTrait;

    // 表名
    protected $name = 'user_extract_log';

    protected $deleteTime = false;

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    // 追加属性
    protected $append = [];



    public function user()
    {
        return $this->belongsTo('\app\common\model\User', 'user_id', 'id');
    }


    public function getUserList()
    {
        return \app\common\model\User::column('nickname', 'id');
    }
    public function getExtractTypeList()
    {
        return ['bank'=>'银行卡','alipa'=>'支付宝','wx'=>'微信',];
    }

    public function getTypeList()
    {
        return ['money'=>'余额',];
    }

    public function getStatusList()
    {
        return ['-1'=>'未通过','0'=>'审核中','1'=>'已提现',];
    }

    /**插入提现记录
     * @param $user 用户
     * @param $fee  手续费
     * @param $balance  用户插入前金额
     * @param $amount   提现金额
     * @param $extract_type   提现类型：银行卡bank 支付宝alipay 微信wx
     * @param $type      提现方式：money=余额
     * @param null $mark 备注
     * @return mixed
     */
    public static function insertLog($user, $fee, $balance, $amount, $extract_type, $type, $mark = null)
    {
        $param = array();
        //根据提现类型得到提现信息
        $extractData = UserExtract::getExtractInfo($user['id'], $extract_type);//得到提现信息
        $param['user_id'] = $user['id'];//用户id
        $param['extract_price'] = $amount;//提现金额
        $param['fee'] = $fee;//手续费
        $param['before'] = $balance;//手续费
        $param['after'] = $balance - $amount;//手续费
        $param['mark'] = $mark;//备注
        $param['extract_type'] = $extract_type;//提现方式
        $param['type'] = $type;//提现类型
        $param['status'] = '0';//提现状态
        $log = self::create(array_merge($param, $extractData));
        return $log;
    }


}