define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        index_url: '/manage/member/miniapp/my/index',
    };

    var Controller = {
        index: function () {
            ea.request.get(
                {
                    url: init.index_url,
                    data: {},
                }, function (res) {
                    var list = res.data.data.list;

                    var html = '';
                    list.forEach(v => {
                        var button = '<button type="button" class="layui-btn layui-btn-xs layui-btn-normal" data-dir="' + v.miniapp.dir + '">管理应用</button>';
                        html += '<tr><td>' + v.id + '</td><td>' + v.appname + '</td><td>' + v.service_id + '</td><td>' + v.create_time + '</td><td>' + button + '</td></tr>';
                    });
                    $('#my-miniapp').html(html);
                }
            );

            //监听列表内管理应用按钮被点击
            $('body').off('click', '[data-dir]').on('click', '[data-dir]', function (event) {
                var miniapp_dir = $(this).attr('data-dir');
                //将目标应用的目录写入cookie,在租户首页进行消费
                ea.common.setCookie('miniapp_dir', miniapp_dir, 's60');
                parent.window.location = '/manage/member/index';
            })

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});