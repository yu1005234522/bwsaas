<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace addons\addon_demo\controller;

/**
 * Copyright
 */
class Index extends PluginBaseController
{
//    protected $middleware = [
//        'login' => ['except' => []]
//    ];

    public function index()
    {
        //     var_dump(addons_url('alisms://index/index'));
        //var_dump($this->getInfo());die;
        //$this->assign('test', '123');
        return $this->fetch();

//		echo hook('myhook', ['id'=>1]);
        //var_dump(get_addons_info('firstapp'));
//		$data =addons_url("firstapp://Index/index");
//		var_dump($data);
//        return view();
//        return view('plugin/alisms/index/index');p
    }

    public function login()
    {
        if (request()->isPost()) {
            return $this->success('登陆成功');
        }

        return $this->fetch();
    }

    public function _empty($param)
    {
        var_dump("不存在方法：" . $param);
    }

}