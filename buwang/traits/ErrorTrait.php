<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\traits;

use think\facade\Db;

trait ErrorTrait
{
    /** jyk 20200420 增加自定义错误 方便model方法传递错误信息 （鉴 BaseModel）  start  **/
    private static $errorMsg;
    private static $errorCode;

    /**
     * 设置错误信息
     * @param string $errorMsg
     * @return bool
     */
    public static function setError($errorMsg = '操作失败,请稍候再试!', $errorCode = 0, $rollback = false)
    {
        if ($rollback) Db::rollback();
        self::$errorMsg = $errorMsg;
        self::$errorCode = $errorCode;
        return false;
    }

    /**
     * 获取错误信息
     * @param string $defaultMsg
     * @return string
     */
    public static function getError($defaultMsg = '操作失败,请稍候再试!')
    {
        return !empty(self::$errorMsg) ? self::$errorMsg : $defaultMsg;
    }

    /**
     * 获取错误号
     * @param string $defaultCode
     * @return string
     */
    public static function getErrorCode($defaultCode = 0)
    {
        return !empty(self::$errorCode) ? self::$errorCode : $defaultCode;
    }
}