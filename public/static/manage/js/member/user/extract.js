define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/extract/index',
        add_url: 'member/user/extract/add',
        edit_url: 'member/user/extract/edit',
        delete_url: 'member/user/extract/del',
        export_url: 'member/user/extract/export',
        modify_url: 'member/user/extract/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'user_id', title: '用户ID'},
                    {field: 'user.mobile', title: '手机号',minWidth:150},
                    {field: 'user.nickname', title: '昵称',minWidth:150},
                    {field: 'user.avatar', title: '头像', templet: ea.table.image,minWidth:150},
                    {field: 'bank_name', title: '银行名称',minWidth:150},
                    {field: 'bank_username', title: '银行收款人姓名',minWidth:150},
                    {field: 'bank_zone', title: '开户地址',minWidth:150},
                    {field: 'bank_detail', title: '详细开户地址',minWidth:150},
                    {field: 'bank_card', title: '银行卡号',minWidth:150},
                    {field: 'wx_name', title: '微信账户人姓名',minWidth:150},
                    {field: 'wxImag', title: '微信收款码', templet: ea.table.image,minWidth:150},
                    {field: 'ali_name', title: '阿里账户人姓名',minWidth:150},
                    {field: 'ali_account', title: '支付宝账号',minWidth:150},
                    {field: 'aliImag', title: '支付宝收款码', templet: ea.table.image,minWidth:150},
                    {field: 'add_time', title: '添加时间',minWidth:150,templet: ea.table.date},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});