define(["easy-admin", "QRCode", "easy-admin"], function (ea, QRCode, ea) {
    easy_admin = ea;

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/message/list',
        message_read_url: 'member/message/read',
    };

    var Controller = {
        login: function () {

            if (top.location !== self.location) {
                top.location = self.location;
            }

            $('.bind-password').on('click', function () {
                if ($(this).hasClass('icon-5')) {
                    $(this).removeClass('icon-5');
                    $("input[name='password']").attr('type', 'password');
                } else {
                    $(this).addClass('icon-5');
                    $("input[name='password']").attr('type', 'text');
                }
            });
            $('.reg-member').on('click', function () {
                ea.open('注册租户','/manage/member/add','50%','90%');
            });

            $('.icon-nocheck').on('click', function () {
                if ($(this).hasClass('icon-check')) {
                    $(this).removeClass('icon-check');
                } else {
                    $(this).addClass('icon-check');
                }
            });
            //扫码登录JS  Start
            var isQrcode;
            $(".login_type").click(function(){
                $("#account").toggleClass('account-icon');
                $("#scan").toggle(function(){
                    $("#show_qrcode").empty();
                    if ($(this).css("display") == 'block'){
                        $.getJSON("/api/wechat_auth/createScanCode",function(rel){
                            if(rel.data.errcode == 0){
                                new QRCode(parent.document.getElementById("show_qrcode"),rel.data.data.url);
                            }else {
                                parent.document.getElementById("show_qrcode").innerText = rel.msg;
                            }

                        })
                        isQrcode = setInterval(function(){
                            $.getJSON("/api/wechat_auth/getLoginState",function(data){
                                if(data.code == 200 && data.data.errcode == 400200){
                                    clearInterval(isQrcode);
                                    ea.common.setCookie('token', data.data.data.token, 's'+data.data.data.expires_in);
                                    ea.msg.success("登录成功", function () {
                                        window.location.href = data.data.data.url;
                                    })
                                }
                            })
                        },1000)
                    }else{
                        clearInterval(isQrcode);
                    }
                });
            });
        //扫码登录JS  End
            ea.listen(function (data) {
                data['keep_login'] = $('.icon-nocheck').hasClass('icon-check') ? 1 : 0;
                return data;
            }, function (res) {
                ea.common.setCookie('token', res.data.data.token, 's'+res.data.data.expires_in);
                ea.msg.success("登录成功", function () {
                    window.location = ea.url('member/index');
                })
            }, function (res) {
                ea.msg.error(res.msg, function () {
                    $('#refreshCaptcha').trigger("click");
                });
            });

        },
        messageList: function () {

            var laypage = layui.laypage;

            //执行一个laypage实例
            laypage.render({
                elem: 'test1' //注意，这里的 test1 是 ID，不用加 # 号
                ,count: total //数据总数，从服务端得到
                ,limit: 5
                ,jump: function(obj, first) {
                    getMessageList(obj.curr, obj.limit);
                    //首次不执行
                    if (!first) {
                        //do something
                    }
                }
            });

            ea.listen();
        }
    };


    return Controller;
});