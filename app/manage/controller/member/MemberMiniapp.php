<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use buwang\base\MemberBaseController;
use think\facade\View;

/**
 * @ControllerAnnotation(title="租户应用")
 */
class MemberMiniapp extends MemberBaseController
{
    use \buwang\traits\Crud;

    protected $relationSearch = true;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\MemberMiniapp();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if (input('selectFieds')) {
            return $this->selectList();
        }
        list($page, $limit, $where) = $this->buildTableParames();
        $count = $this->model
            ->withJoin('miniapp')
            ->where("{$this->getTableName()}.member_id", $this->uid)
            ->where($where)
            ->count();
        $list = $this->model
            ->withJoin('miniapp')
            ->where("{$this->getTableName()}.member_id", $this->uid)
            ->where($where)
            ->order($this->sort)
            ->select();

        foreach ($list as &$item) {
            $item['create_time'] = date('Y-m-d H:i:s', $item['create_time']);
        }
        $data = [
            'default_miniapp_dir' => $this->user->default_miniapp_dir,
            'total' => $count,
            'list' => $list,
        ];
        if ($this->request->isAjax()) {
            return $this->success('success', $data);
        }
        View::assign(compact('data'));
        return view();
    }

    public function setDefaultMiniapp($miniapp_dir = '')
    {
        //TODO 应用是否存在
        //应用是否已购买
        //应用是否过期
        \app\common\model\Member::update(['default_miniapp_dir' => $miniapp_dir], ['id' => $this->user->id]);
        return $this->success('设置成功');
    }
}