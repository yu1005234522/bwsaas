define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/miniapp/module/index',
        add_url: 'admin/miniapp/module/add',
        edit_url: 'admin/miniapp/module/edit',
        delete_url: 'admin/miniapp/module/del',
        export_url: 'admin/miniapp/module/export',
        modify_url: 'admin/miniapp/module/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['refresh', 'delete'],
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'miniapp_id', title: '应用id', search: false},
                    {field: 'group_id', title: '管理员组id'},
                    {field: 'name', title: '功能名'},
                    {field: 'type', search: 'select', selectList: {"1":"基础功能","2":"附加功能","3":"插件功能"}, title: '类型'},
                    {field: 'price', title: '价格'},
                    {field: 'valid_days', title: '有效天数'},
                    {field: 'desc', title: '描述'},
                    {field: 'create_time', title: '创建时间', templet: ea.table.date, width: 180},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});