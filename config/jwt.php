<?php

return [
    // 加密key
    'key' => 'buwangyun',
    //加密方式
    'alg' => 'HS256',
    //签发者 可选
    'iss' => 'www.buwangyun.com',
    //接收该JWT的一方，可选
    'aud' => 'buwang',
    //access_token有效时间 单位s
    'accessTokenExp' => 3600 * 24 * 7,
    //refresh_token有效时间 单位s
    'refreshTokenExp' => 3600 * 24 * 7,
    'tokenType' => 'bearer',
];
