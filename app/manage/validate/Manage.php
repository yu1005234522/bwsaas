<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\validate;

use think\Validate;

class Manage extends Validate
{
    protected $rule = [
        'username' => 'require',
        'mobile' => 'require',
        'password' => 'require',
        'captcha' => 'require|captcha',

        'login_id' => 'require|token',
        'login_password' => 'require',
        'password_confirm' => 'require',
        'about' => 'require',
        'id' => 'require|number',
    ];

    protected $message = [
        'username' => '用户名必须填写',
        'mobile' => '手机号必须填写',
        'password' => '密码必须填写',
        'captcha' => '验证码错误',

        'id' => '{%id_error}',
        'password_confirm' => '两次密码输入不一致',
        'about' => '备注必须填写',
        'login_id.require' => '用户名必须填写',
        'login_id.token' => '令牌数据失效,必须刷新页面再试',
    ];

    protected $scene = [
        'admin_login' => ['username', 'password', 'captcha'],
        'member_login' => ['mobile', 'password', 'captcha'],

        'add' => ['username', 'password', 'password_confirm', 'about'],
        'edit' => ['id', 'username', 'about'],
        'password' => ['password', 'password_confirm', 'about'],
    ];
}