<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\admin;

use app\common\model\Message;
use buwang\base\AdminBaseController;
use buwang\traits\CrudControllerTrait;
use app\manage\model\AuthNode;
use app\manage\model\AuthGroupAccess;
use app\manage\model\Member as MemberModel;
use think\App;
use app\Request;
use think\Exception;

class Member extends AdminBaseController
{
    use CrudControllerTrait;

    /*protected $middleware = [
        'login' 	=> ['except' 	=> [] ]
    ];*/

    protected $model = null;//模型实例

    protected function initialize()
    {
        parent::initialize();

        $this->model = new \app\common\model\Member();
    }


    /**
     * 查看
     * @menu true
     */
    public function index()
    {
        if (request()->isAjax()) {
            $page = request()->get('page/d', 1);
            $limit = request()->get('limit/d', 100);

            //TODO 获取搜索条件
            //TODO 如果有不属于表内的字段,查询会报错
            $where = request()->get();
            unset($where['page']);
            unset($where['limit']);
            foreach ($where as $k => $v) {
                if (!$v && $v !== '0' && $v !== 0 && $v !== false) {
                    unset($where[$k]);
                }
            }


            try {
                $total = $this->model->where($where)->count();
                $groups = $this->model->where($where)->order($this->model->getPk(), 'DESC')->page($page, $limit)->select()->toArray();

                //返回数据
                $list = array();
                $memberConfigIds = array();
                //组合树
                foreach ($groups as $val) {
                    $memberConfigIds[] = $val['id'];
                }

                foreach ($groups as $group) {
                    $data = $group;
                    $data['is_top'] = false;
                    if ($group['parent_id'] == 0) $data['is_top'] = true;
                    $data['rules'] = [];
                    $data['auth_group_access'] = [];
                    //查询所有角色，以逗号拼接
                    $rules = AuthGroupAccess::valiWhere()->where('uid', $group['id'])->where('scopes', 'member')->column('group_id');
                    $names = AuthGroupAccess::valiWhere()->where('uid', $group['id'])->where('scopes', 'member')->column('name');
                    if ($rules) {
                        $data['rules'] = $rules;
                    }
                    if ($names) {
                        $data['auth_group_access'] = $names;
                    }
                    //列表中不存在该父id则父id为0
                    if (!in_array($group['parent_id'], $memberConfigIds)) $data['parent_id'] = 0;
                    $list[] = $data;
                }


                $list = array_merge(AuthNode::tree($list, 'username', 'id', 'parent_id'));
                // var_dump($list);die;
//                array_filter($list, function ($content) {
//                    return str_replace("&amp;emsp;", '&emsp;', $content);
//                });

            } catch (\Exception $e) {
                return $this->error('查询失败', ['errorMsg' => $e->getMessage()]);
            }

            $data = compact("total", 'list');
            return $this->success('successful', $data);
        }

        return view();
    }


    /**
     * 编辑管理组
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        //var_dump( bw_config('wetchatopen.aes_key'));
        if ($this->request->isPost()) {
            $param = $this->request->param();
            if (isset($param['mobile'])) $param['username'] = 'bw' . $param['mobile'];
            $result = MemberModel::updateAdmin($param, true);
            if ($result) {
                return $this->success('操作成功');
            } else {
                return $this->error(MemberModel::getError());
            }
        }
        $id = $this->request->get('id');
        $data = AuthGroupAccess::getMemberFind($id);
        $list = MemberModel::select()->toArray();
        // var_dump($list);die;
        return view('form', ['data' => $data, 'list' => $list]);

    }

    /**
     * 添加管理组
     * @return \think\response\View
     */
    public function add(Request $request)
    {

        $member_id = 0;
        if ($this->request->isPost()) {
            $param = $this->request->param();
            if (isset($param['mobile'])) $param['username'] = 'bw' . $param['mobile'];
            if (!$param['safe_password']) $param['safe_password'] = '123456';
            if (!$param['password']) $param['password'] = 'a12345678';
            $result = MemberModel::createAdmin($param, $member_id, true, true, true);
            if ($result) {
                // xn_add_admin_log('添加管理组');
                return $this->success('操作成功');
            } else {
                return $this->error(MemberModel::getError());
            }
        }
        return view('form');
    }


    /**
     * 更改开启状态
     *
     * @return \think\Response
     */
    public function setStatus()
    {
        if (request()->isAjax()) {
            $id = input('id/d');
            $data = MemberModel::find($id);
            if ($data['status'] === 0) {
                $data['status'] = 1;
            } elseif ($data['status'] === 1) {
                $data['status'] = 0;
            };
            $data->save();
            return $this->success('successful');

        }
    }

    /**
     * 发送站内信
     */
    public function sendMessage()
    {
        if (request()->isPost()) {
            $member_ids = request()->post('member_ids/d', 0);
            $msg = request()->post('msg/s', '');

            if (!$member_ids || !$msg) return $this->error('参数有误');

            try {
                Message::send(1, $this->uid, $member_ids, $msg);
                return $this->success('发送成功');
            } catch (Exception $e) {
                return $this->error($e->getMessage());
            }
        }
    }
}
