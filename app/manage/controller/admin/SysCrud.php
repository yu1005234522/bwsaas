<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\admin;

use buwang\base\AdminBaseController;
use buwang\traits\CrudControllerTrait;
use think\facade\Db;
use think\facade\View;

class SysCrud extends AdminBaseController
{
    use CrudControllerTrait;

    /*protected $middleware = [
        'login' 	=> ['except' 	=> [] ]
    ];*/

    protected $model = null;//模型实例

    protected function initialize()
    {
        parent::initialize();

        $this->model = new \app\manage\model\admin\SysCrud();
    }

    /**
     * 新增
     */
    public function add()
    {

        $dirs = array_map('basename', glob(base_path() . '*', GLOB_ONLYDIR));
        if ($dirs === false) return $this->error('应用目录不可读');
        $dirs = array_diff($dirs, ['api', 'common', 'home']);//去除系统应用,获取扩展应用
        if (empty($dirs)) return $this->error('暂无应用');
        $tables = array_column(Db::query("show tables"), 'Tables_in_' . config('database.connections.mysql.database'));
        if (request()->isPost()) {
            $param = request()->post();

            if (!in_array($param['app_name'], $dirs)) return $this->error('应用不存在');
            //组装一下关联表数据
            $relation = [];
            if (isset($param['isRelation']) && $param['isRelation'] == 1 &&
                isset($param['relation']) && !empty($param['relation'])) {
                foreach ($param['relation'] as $k => $v) {
                    $relation[] = [
                        'relation' => $v,
                        'relationmode' => $param['relationmode'][$k],
                        'relationforeignkey' => $param['relationforeignkey'][$k],
                        'relationprimarykey' => $param['relationprimarykey'][$k],
                        'relationfields' => $param['relationfields'][$k],
                    ];
                }
            }
            $param['relations'] = $relation;
            //var_dump($relation);die;
            $this->model->startTrans();
            try {
                $crud = new \buwang\service\CrudService();
                $crud->create($param['app_name'], $param['table_name'], $param['controller_name_diy'], $param['model_name_diy'], $param['relations']);

                //TODO 新增成功记录
                $param['status'] = 1;
                $this->model->save($param);
                $this->model->commit();
            } catch (\Throwable $e) {
                $this->model->rollback();

                //TODO 新增失败记录
                $param['status'] = 0;
                $this->model->save($param);

                return $this->error($e->getMessage() . ' - ' . $e->getLine() . ' - ' . $e->getFile());
            }
            return $this->success();
        }

        View::assign('apps', $dirs);
        View::assign('tables', $tables);
        return view();
    }
}
