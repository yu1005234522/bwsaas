<?php

return [
    'controller_php_template' => "buwang/template/Controller.template.php",
    'model_php_template' => "buwang/template/Model.template.php",
    'index_html_template' => "buwang/template/index.template.php",
    'add_html_template' => "buwang/template/add.template.php",
    'edit_html_template' => "buwang/template/edit.template.php",
];