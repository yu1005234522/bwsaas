define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/extractLog/index',
        add_url: 'member/user/extractLog/add',
        edit_url: 'member/user/extractLog/edit',
        delete_url: 'member/user/extractLog/del',
        export_url: 'member/user/extractLog/export',
        modify_url: 'member/user/extractLog/modify'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'user_id', title: '用户id'},
                    {field: 'user.mobile', title: '手机号',minWidth:150},
                    {field: 'user.nickname', title: '昵称',minWidth:150},
                    {field: 'user.avatar', title: '头像', templet: ea.table.image},
                    {field: 'real_name', title: '提现人姓名',minWidth:150},
                    {field: 'bank_name', title: '银行名称',minWidth:150},
                    {field: 'bank_zone', title: '开户地址',minWidth:150},
                    {field: 'bank_detail', title: '详细开户地址',minWidth:150},
                    {field: 'bank_card', title: '银行卡号',minWidth:150},
                    {field: 'wxImag', title: '微信收款码', templet: ea.table.image},
                    {field: 'aliImag', title: '支付宝收款码', templet: ea.table.image},
                    {field: 'ali_account', title: '支付宝账号',minWidth:150},
                    {field: 'extract_price', title: '提现金额',minWidth:150},
                    {field: 'fee', title: '手续费',minWidth:150},
                    {field: 'before', title: '提现前数额',minWidth:150},
                    {field: 'after', title: '提现后数额',minWidth:150},
                    {field: 'mark', title: '备注',minWidth:150},
                    {field: 'extract_type', search: 'select', selectList: {"bank":"银行卡","alipa":"支付宝","wx":"微信"}, title: '提现方式',minWidth:150},
                    {field: 'type', search: 'select', selectList: {"money":"余额"}, title: '提现类型',minWidth:150},
                    {field: 'status', search: 'select', selectList: {"-1":"未通过","0":"审核中","1":"已提现"}, title: '提现状态',minWidth:150},
                    {field: 'fail_msg', title: '提现失败原因',minWidth:150},
                    {field: 'updatetime', title: '更新时间',minWidth:150,templet: ea.table.date},
                    {field: 'createtime', title: '添加时间',minWidth:150,templet: ea.table.date},
                    {width: 250, title: '操作', templet: ea.table.tool
                        ,operat: [
                            [{
                                text: '提现审核',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                            }],
                            'delete']
                    },
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});