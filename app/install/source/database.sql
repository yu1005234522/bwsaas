/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-02-03 11:21:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bw_admin
-- ----------------------------
DROP TABLE IF EXISTS `bw_admin`;
CREATE TABLE `bw_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像{image}',
  `mobile` varchar(15) DEFAULT '' COMMENT '手机号',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间{date}',
  `loginip` varchar(50) DEFAULT NULL COMMENT '登录IP',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间{date}',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间{date}',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态{switch}(0:封禁,1:正常)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员表';

-- ----------------------------
-- Records of bw_admin
-- ----------------------------
INSERT INTO `bw_admin` VALUES ('1', 'admin', 'Admin', '00ff9815df78587e196f906493ef9112', '814726', '', '', 'admin@admin.com', '0', '1604380088', '127.0.0.1', '1492186163', '1591582951', '1');

-- ----------------------------
-- Table structure for bw_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `bw_admin_log`;
CREATE TABLE `bw_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `name` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=912 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员日志表';

-- ----------------------------
-- Records of bw_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `bw_auth_group`;
CREATE TABLE `bw_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父角色ID',
  `app_name` varchar(40) NOT NULL DEFAULT 'manage' COMMENT '规则所属应用名字或插件名字，如manage,api,home',
  `type` varchar(30) NOT NULL DEFAULT 'system' COMMENT '权限规则分类，请加应用前缀,如system,plugin,app',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态0:禁用1:正常',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `group_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色唯一标识',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `scopes` varchar(100) DEFAULT 'admin' COMMENT '登录类型',
  `member_id` int(10) DEFAULT '0' COMMENT '租户角色id（0表示系统角色）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `group_name` (`group_name`),
  KEY `status` (`status`),
  KEY `pid` (`pid`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of bw_auth_group
-- ----------------------------
INSERT INTO `bw_auth_group` VALUES ('1', '0', 'manage', 'system', '1', '0', '0', '33', '超级管理员组', 'super_admin', '', 'admin', '0');
INSERT INTO `bw_auth_group` VALUES ('62', '0', 'manage', 'system', '1', '1594372603', '0', '0', '系统基础功能', 'member_system_base', '', 'member', '0');
INSERT INTO `bw_auth_group` VALUES ('105', '1', 'manage', 'system', '1', '1594970726', '0', '0', '开发组', 'code', '', 'admin', '0');

-- ----------------------------
-- Table structure for bw_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `bw_auth_group_access`;
CREATE TABLE `bw_auth_group_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL COMMENT '管理员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '角色ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色名',
  `scopes` varchar(100) DEFAULT 'admin' COMMENT '登录类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_group_id_scopes` (`uid`,`group_id`,`scopes`) USING BTREE,
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='权限分组表';

-- ----------------------------
-- Records of bw_auth_group_access
-- ----------------------------
INSERT INTO `bw_auth_group_access` VALUES ('28', '4', '1', '超级管理员组', 'admin');
INSERT INTO `bw_auth_group_access` VALUES ('32', '5', '1', '超级管理员组', 'admin');
INSERT INTO `bw_auth_group_access` VALUES ('43', '1', '1', '超级管理员组', 'admin');
INSERT INTO `bw_auth_group_access` VALUES ('73', '16', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('94', '19', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('118', '2', '105', '开发组', 'admin');
INSERT INTO `bw_auth_group_access` VALUES ('119', '20', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('120', '21', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('184', '22', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('186', '24', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('192', '30', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('193', '31', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('194', '32', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('197', '32', '103', 'Demo基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('199', '34', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('201', '17', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('202', '35', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('203', '50', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('204', '51', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('205', '52', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('208', '54', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('212', '55', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('218', '56', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('220', '57', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('223', '58', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('227', '60', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('228', '61', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('234', '2', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('235', '63', '62', '系统基础功能', 'member');
INSERT INTO `bw_auth_group_access` VALUES ('246', '64', '62', '系统基础功能', 'member');

-- ----------------------------
-- Table structure for bw_auth_group_node
-- ----------------------------
DROP TABLE IF EXISTS `bw_auth_group_node`;
CREATE TABLE `bw_auth_group_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色组ID,非顶级租户时使用',
  `node_id` int(10) unsigned NOT NULL COMMENT '权限节点ID',
  `node_name` varchar(100) NOT NULL DEFAULT '' COMMENT '节点后台url',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类,请加应用前缀,如admin_',
  `auth_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  PRIMARY KEY (`id`),
  KEY `rule_name` (`node_name`) USING BTREE,
  KEY `role_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47324 DEFAULT CHARSET=utf8 COMMENT='权限授权表';

-- ----------------------------
-- Records of bw_auth_group_node
-- ----------------------------
INSERT INTO `bw_auth_group_node` VALUES ('27316', '1', '188', '/plugin', 'system', '_plugin');
INSERT INTO `bw_auth_group_node` VALUES ('27317', '1', '891', '/manage/admin.Plugin', 'system', '_manage_admin_Plugin');
INSERT INTO `bw_auth_group_node` VALUES ('27318', '1', '900', '/manage/admin.Plugin/config', 'system', '/manage/admin.Plugin/config');
INSERT INTO `bw_auth_group_node` VALUES ('27320', '1', '898', '/manage/admin.Plugin/enable', 'app', '/manage/admin.Plugin/enable');
INSERT INTO `bw_auth_group_node` VALUES ('27321', '1', '897', '/manage/admin.Plugin/uninstall', 'app', '/manage/admin.Plugin/uninstall');
INSERT INTO `bw_auth_group_node` VALUES ('27322', '1', '896', '/manage/admin.Plugin/install', 'app', '/manage/admin.Plugin/install');
INSERT INTO `bw_auth_group_node` VALUES ('27323', '1', '895', '/manage/admin.Plugin/del', 'app', '/manage/admin.Plugin/del');
INSERT INTO `bw_auth_group_node` VALUES ('27324', '1', '894', '/manage/admin.Plugin/edit', 'app', '/manage/admin.Plugin/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27325', '1', '893', '/manage/admin.Plugin/add', 'app', '/manage/admin.Plugin/add');
INSERT INTO `bw_auth_group_node` VALUES ('27326', '1', '892', '/manage/admin.Plugin/index', 'app', '/manage/admin.Plugin/index');
INSERT INTO `bw_auth_group_node` VALUES ('27327', '1', '187', '/app', 'system', '_app');
INSERT INTO `bw_auth_group_node` VALUES ('27328', '1', '1124', '/manage/admin.member', 'system', 'manage_admin_member');
INSERT INTO `bw_auth_group_node` VALUES ('27329', '1', '1128', '/manage/admin.member/del', 'app', '/manage/admin.member/del');
INSERT INTO `bw_auth_group_node` VALUES ('27330', '1', '1127', '/manage/admin.Member/edit', 'app', '/manage/admin.Member/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27331', '1', '1126', '/manage/admin.Member/add', 'app', '/manage/admin.Member/add');
INSERT INTO `bw_auth_group_node` VALUES ('27332', '1', '1125', '/manage/admin.member/index', 'app', '/manage/admin.member/index');
INSERT INTO `bw_auth_group_node` VALUES ('27333', '1', '1255', '/manage/admin.member/setStatus', 'system', '/manage/admin/member/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('27334', '1', '1093', '/manage/admin.MemberMiniapp', 'system', '_manage_admin_memberMiniapp');
INSERT INTO `bw_auth_group_node` VALUES ('27335', '1', '1097', '/manage/admin.MemberMiniapp/del', 'app', '/manage/admin.MemberMiniapp/del');
INSERT INTO `bw_auth_group_node` VALUES ('27336', '1', '1096', '/manage/admin.MemberMiniapp/edit', 'app', '/manage/admin.MemberMiniapp/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27337', '1', '1095', '/manage/admin.MemberMiniapp/add', 'app', '/manage/admin.MemberMiniapp/add');
INSERT INTO `bw_auth_group_node` VALUES ('27338', '1', '1094', '/manage/admin.MemberMiniapp/index', 'app', '/manage/admin.MemberMiniapp/index');
INSERT INTO `bw_auth_group_node` VALUES ('27339', '1', '1020', '/manage/admin.Miniapp', 'system', '_manage_admin_miniapp');
INSERT INTO `bw_auth_group_node` VALUES ('27340', '1', '1026', '/manage/admin.Miniapp/buy', 'app', '/manage/admin.Miniapp/buy');
INSERT INTO `bw_auth_group_node` VALUES ('27341', '1', '1025', '/manage/admin.Miniapp/install', 'app', '/manage/admin.Miniapp/install');
INSERT INTO `bw_auth_group_node` VALUES ('27342', '1', '1024', '/manage/admin.Miniapp/uninstall', 'app', '/manage/admin.Miniapp/uninstall');
INSERT INTO `bw_auth_group_node` VALUES ('27343', '1', '1023', '/manage/admin.Miniapp/edit', 'app', '/manage/admin.Miniapp/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27344', '1', '1022', '/manage/admin.Miniapp/add', 'app', '/manage/admin.Miniapp/add');
INSERT INTO `bw_auth_group_node` VALUES ('27345', '1', '1021', '/manage/admin.Miniapp/index', 'app', '/manage/admin.Miniapp/index');
INSERT INTO `bw_auth_group_node` VALUES ('27346', '1', '186', '/manage', 'system', '_manage');
INSERT INTO `bw_auth_group_node` VALUES ('27352', '1', '1114', '/manage/crudDemo', 'system', 'manage_crudDemo');
INSERT INTO `bw_auth_group_node` VALUES ('27353', '1', '1118', '/manage/crudDemo/del', 'system', 'manage_crudDemo_del');
INSERT INTO `bw_auth_group_node` VALUES ('27354', '1', '1117', '/manage/crudDemo/edit', 'app', '/manage/crudDemo/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27355', '1', '1116', '/manage/crudDemo/add', 'app', '/manage/crudDemo/add');
INSERT INTO `bw_auth_group_node` VALUES ('27356', '1', '1115', '/manage/crudDemo/index', 'app', '/manage/crudDemo/index');
INSERT INTO `bw_auth_group_node` VALUES ('27357', '1', '851', '/manage/admin.SysArticle', 'system', 'manage_admin_sysArticle');
INSERT INTO `bw_auth_group_node` VALUES ('27358', '1', '856', '/manage/admin.SysArticleCategory', 'system', 'manage_admin_sysArticleCategory');
INSERT INTO `bw_auth_group_node` VALUES ('27359', '1', '860', '/manage/admin.SysArticleCategory/del', 'app', '/manage/admin.SysArticleCategory/del');
INSERT INTO `bw_auth_group_node` VALUES ('27360', '1', '859', '/manage/admin.SysArticleCategory/edit', 'app', '/manage/admin.SysArticleCategory/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27361', '1', '858', '/manage/admin.SysArticleCategory/add', 'app', '/manage/admin.SysArticleCategory/add');
INSERT INTO `bw_auth_group_node` VALUES ('27362', '1', '857', '/manage/admin.SysArticleCategory/index', 'app', '/manage/admin.SysArticleCategory/index');
INSERT INTO `bw_auth_group_node` VALUES ('27363', '1', '855', '/manage/admin.SysArticle/del', 'app', '/manage/admin.SysArticle/del');
INSERT INTO `bw_auth_group_node` VALUES ('27364', '1', '854', '/manage/admin.SysArticle/edit', 'app', '/manage/admin.SysArticle/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27365', '1', '853', '/manage/admin.SysArticle/add', 'app', '/manage/admin.SysArticle/add');
INSERT INTO `bw_auth_group_node` VALUES ('27366', '1', '852', '/manage/admin.SysArticle/index', 'app', '/manage/admin.SysArticle/index');
INSERT INTO `bw_auth_group_node` VALUES ('27367', '1', '807', '/manage/admin.SysCrud', 'system', 'manage_admin_sysCrud');
INSERT INTO `bw_auth_group_node` VALUES ('27368', '1', '811', '/manage/admin.SysCrud/del', 'app', '/manage/admin.SysCrud/del');
INSERT INTO `bw_auth_group_node` VALUES ('27369', '1', '810', '/manage/admin.SysCrud/edit', 'app', '/manage/admin.SysCrud/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27370', '1', '809', '/manage/admin.SysCrud/add', 'app', '/manage/admin.SysCrud/add');
INSERT INTO `bw_auth_group_node` VALUES ('27371', '1', '808', '/manage/admin.SysCrud/index', 'app', '/manage/admin.SysCrud/index');
INSERT INTO `bw_auth_group_node` VALUES ('27372', '1', '190', '/manage/weihu', 'system', 'manage_ceshi');
INSERT INTO `bw_auth_group_node` VALUES ('27373', '1', '206', '/manage/config/configindex', 'system', '_manage_config_configindex');
INSERT INTO `bw_auth_group_node` VALUES ('27374', '1', '1098', '/manage/Config/editconfig', 'system', '/manage/Config/editconfig');
INSERT INTO `bw_auth_group_node` VALUES ('27375', '1', '874', '/manage/Config/addconfig', 'system', 'manageConfigaddconfig');
INSERT INTO `bw_auth_group_node` VALUES ('27376', '1', '506', '/manage/Config/getconfiglist', 'system', 'manageConfiggetconfiglist');
INSERT INTO `bw_auth_group_node` VALUES ('27377', '1', '1193', '/manage/Config/configsoftdleting', 'system', '/manage/Config/configsoftdleting');
INSERT INTO `bw_auth_group_node` VALUES ('27378', '1', '1192', '/manage/Config/setconfigshow', 'system', '/manage/Config/setconfigshow');
INSERT INTO `bw_auth_group_node` VALUES ('27379', '1', '196', '/manage/Gruop/set', 'system', '_manage_gruop_set');
INSERT INTO `bw_auth_group_node` VALUES ('27380', '1', '211', '/manage/GroupData/index', 'system', '_manage_GroupData_index');
INSERT INTO `bw_auth_group_node` VALUES ('27381', '1', '513', '/manage/GroupData/edit', 'system', 'manageGroupDataedit');
INSERT INTO `bw_auth_group_node` VALUES ('27382', '1', '505', '/manage/GroupData/add', 'system', 'manageGroupDataadd');
INSERT INTO `bw_auth_group_node` VALUES ('27383', '1', '504', '/manage/GroupData/getconfiglist', 'system', 'manageGroupDatagetconfiglist');
INSERT INTO `bw_auth_group_node` VALUES ('27384', '1', '210', '/manage/Group', 'system', '_manage_group');
INSERT INTO `bw_auth_group_node` VALUES ('27385', '1', '1166', '/manage/Group/index', 'system', '/manage/Group/index');
INSERT INTO `bw_auth_group_node` VALUES ('27386', '1', '512', '/manage/Group/editconfig', 'system', 'manageGroupeditconfig');
INSERT INTO `bw_auth_group_node` VALUES ('27387', '1', '511', '/manage/Group/edit', 'system', 'manageGroupedit');
INSERT INTO `bw_auth_group_node` VALUES ('27388', '1', '507', '/manage/Group/addconfig', 'system', 'manageGroupaddconfig');
INSERT INTO `bw_auth_group_node` VALUES ('27389', '1', '503', '/manage/Group/add', 'system', 'manageGroupadd');
INSERT INTO `bw_auth_group_node` VALUES ('27390', '1', '497', '/manage/Group/getconfiglist', 'system', 'manageGroupgetconfiglist');
INSERT INTO `bw_auth_group_node` VALUES ('27391', '1', '1194', '/manage/Group/configsoftdleting', 'system', '/manage/Group/configsoftdleting');
INSERT INTO `bw_auth_group_node` VALUES ('27392', '1', '193', '/manage/config/index', 'system', '_manage_config_index');
INSERT INTO `bw_auth_group_node` VALUES ('27393', '1', '200', '/manage/Config/softdleting', 'system', '/manage/Config/softdleting');
INSERT INTO `bw_auth_group_node` VALUES ('27394', '1', '199', '/manage/Config/getlist', 'system', 'config_set_select');
INSERT INTO `bw_auth_group_node` VALUES ('27395', '1', '198', '/manage/Config/edit', 'system', '/manage/Config/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27396', '1', '197', '/manage/Config/add', 'system', 'manageConfigadd');
INSERT INTO `bw_auth_group_node` VALUES ('27397', '1', '1191', '/manage/Config/setshow', 'system', '/manage/Config/setshow');
INSERT INTO `bw_auth_group_node` VALUES ('27398', '1', '202', '/manage/Config/showconfig', 'system', '_manage_config_showconfig');
INSERT INTO `bw_auth_group_node` VALUES ('27399', '1', '508', '/manage/Config/setValues', 'system', '/manage/Config/setValues');
INSERT INTO `bw_auth_group_node` VALUES ('27400', '1', '185', '/manage/auth', 'system', 'manage_auth');
INSERT INTO `bw_auth_group_node` VALUES ('27401', '1', '183', '/manage/Node/index', 'system', 'manage_node_index');
INSERT INTO `bw_auth_group_node` VALUES ('27402', '1', '1167', '/manage/Node/delete', 'system', '/manage/Node/delete');
INSERT INTO `bw_auth_group_node` VALUES ('27403', '1', '510', '/manage/Node/edit', 'system', 'manageNodeedit');
INSERT INTO `bw_auth_group_node` VALUES ('27404', '1', '1274', '/manage/Node/getMemberNodeTree', 'system', '/manage/Node/getMemberNodeTree');
INSERT INTO `bw_auth_group_node` VALUES ('27405', '1', '1272', '/manage/Node/editMember', 'system', '/manage/Node/editMember');
INSERT INTO `bw_auth_group_node` VALUES ('27406', '1', '1271', '/manage/Node/addMember', 'system', '/manage/Node/addMember');
INSERT INTO `bw_auth_group_node` VALUES ('27407', '1', '1270', '/manage/Node/memberIndex', 'system', '/manage/Node/add');
INSERT INTO `bw_auth_group_node` VALUES ('27408', '1', '1176', '/manage/Node/getNodeTree', 'system', '/manage/Node/getNodeTree');
INSERT INTO `bw_auth_group_node` VALUES ('27409', '1', '1175', '/manage/Node/setStatus', 'system', '/manage/Node/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('27410', '1', '1174', '/manage/Node/setShow', 'system', '/manage/Node/setShow');
INSERT INTO `bw_auth_group_node` VALUES ('27411', '1', '1173', '/manage/Node/add', 'system', 'manageNodeadd');
INSERT INTO `bw_auth_group_node` VALUES ('27412', '1', '1', '/manage/Role/index', 'system', 'manage_role_index');
INSERT INTO `bw_auth_group_node` VALUES ('27413', '1', '1168', '/manage/Role/edit', 'system', '/manage/role/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27414', '1', '1066', '/manage/Role/groupRule', 'system', '/manage/Role/groupRule');
INSERT INTO `bw_auth_group_node` VALUES ('27415', '1', '1259', '/manage/Role/getNodeTree', 'system', '/manage/Role/getNodeTree');
INSERT INTO `bw_auth_group_node` VALUES ('27416', '1', '1258', '/manage/Role/editMember', 'system', '/manage/Role/editMember');
INSERT INTO `bw_auth_group_node` VALUES ('27417', '1', '1257', '/manage/Role/addMember', 'system', '/manage/Role/addMember');
INSERT INTO `bw_auth_group_node` VALUES ('27418', '1', '1256', '/manage/Role/memberRole', 'system', '/manage/Role/memberRole');
INSERT INTO `bw_auth_group_node` VALUES ('27419', '1', '1249', '/manage/Role/getRoleMemberTree', 'system', '/manage/Role/getRoleMemberTree');
INSERT INTO `bw_auth_group_node` VALUES ('27420', '1', '1247', '/manage/Role/getRoleModelTree', 'system', '/manage/Role/getRoleModelTree');
INSERT INTO `bw_auth_group_node` VALUES ('27421', '1', '1246', '/manage/Role/editModel', 'system', '/manage/Role/editModel');
INSERT INTO `bw_auth_group_node` VALUES ('27422', '1', '1245', '/manage/Role/addModel', 'system', '/manage/Role/addModel');
INSERT INTO `bw_auth_group_node` VALUES ('27423', '1', '1244', '/manage/Role/modelRole', 'system', '/manage/Role/modelRole');
INSERT INTO `bw_auth_group_node` VALUES ('27424', '1', '1181', '/manage/Role/setStatus', 'system', '/manage/Role/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('27425', '1', '1180', '/manage/Role/delete', 'system', '/manage/Role/delete');
INSERT INTO `bw_auth_group_node` VALUES ('27426', '1', '1179', '/manage/Role/getRoleTree', 'system', '/manage/Role/getRoleTree');
INSERT INTO `bw_auth_group_node` VALUES ('27427', '1', '1178', '/manage/Role/add', 'system', '/manage/Role/add');
INSERT INTO `bw_auth_group_node` VALUES ('27428', '1', '182', '/manage/Admin/user', 'system', 'manage_admin_user');
INSERT INTO `bw_auth_group_node` VALUES ('27429', '1', '1185', '/manage/Admin/delete', 'system', '/manage/Admin/delete');
INSERT INTO `bw_auth_group_node` VALUES ('27430', '1', '1184', '/manage/Admin/edit', 'system', '/manage/Admin/edit');
INSERT INTO `bw_auth_group_node` VALUES ('27431', '1', '1183', '/manage/Admin/setStatus', 'system', '/manage/Admin/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('27432', '1', '1182', '/manage/Admin/add', 'system', '/manage/Admin/add');
INSERT INTO `bw_auth_group_node` VALUES ('27433', '1', '3', '/manage/admin.Index/dashboard', 'system', '_manage_admin_dashboard');
INSERT INTO `bw_auth_group_node` VALUES ('27434', '1', '1170', '/manage/common/clearCache', 'system', '/manage/common/clearCache');
INSERT INTO `bw_auth_group_node` VALUES ('27435', '1', '1165', '/manage/admin/dashboard', 'system', '/manage/admin/dashboard');
INSERT INTO `bw_auth_group_node` VALUES ('27436', '1', '1164', '/manage/admin.Index/menu', 'system', '/manage/admin.Index/menu');
INSERT INTO `bw_auth_group_node` VALUES ('27437', '1', '1163', '/manage/admin.Index/index', 'system', '/manage/admin.Index/index');
INSERT INTO `bw_auth_group_node` VALUES ('27438', '1', '514', '/manage/admin.index/logout', 'system', '/manage/admin/logout');
INSERT INTO `bw_auth_group_node` VALUES ('27439', '1', '509', '/manage/index/clear', 'system', 'manageindexclear');
INSERT INTO `bw_auth_group_node` VALUES ('27440', '1', '2', '/manage/index/index', 'system', 'manage_index_index');
INSERT INTO `bw_auth_group_node` VALUES ('27441', '1', '1177', '/manage/PublicCommon/icon', 'system', '/manage/PublicCommon/icon');
INSERT INTO `bw_auth_group_node` VALUES ('46989', '105', '186', '/manage', 'system', '_manage');
INSERT INTO `bw_auth_group_node` VALUES ('46990', '105', '3', '/manage/admin.Index/dashboard', 'system', 'manage_admin_dashboard');
INSERT INTO `bw_auth_group_node` VALUES ('46991', '105', '1170', '/manage/common/clearCache', 'system', '/manage/common/clearCache');
INSERT INTO `bw_auth_group_node` VALUES ('46992', '105', '1165', '/manage/admin/dashboard', 'system', '/manage/admin/dashboard');
INSERT INTO `bw_auth_group_node` VALUES ('46993', '105', '1164', '/manage/admin.Index/menu', 'system', '/manage/admin.Index/menu');
INSERT INTO `bw_auth_group_node` VALUES ('46994', '105', '1163', '/manage/admin.Index/index', 'system', '/manage/admin.Index/index');
INSERT INTO `bw_auth_group_node` VALUES ('46995', '105', '514', '/manage/admin.index/logout', 'system', '/manage/admin/logout');
INSERT INTO `bw_auth_group_node` VALUES ('46996', '105', '509', '/manage/index/clear', 'system', 'manageindexclear');
INSERT INTO `bw_auth_group_node` VALUES ('46997', '105', '2', '/manage/index/index', 'system', 'manage_index_index');
INSERT INTO `bw_auth_group_node` VALUES ('46998', '105', '1514', '/manage/admin.Index/getAdminLoginInfo', 'system', '_manage_admin_loginInfo');
INSERT INTO `bw_auth_group_node` VALUES ('46999', '105', '1330', '/manage/admin.Index/userPassword', 'system', '/manage/admin/userPassword');
INSERT INTO `bw_auth_group_node` VALUES ('47000', '105', '1329', '/manage/admin.Index/userSetting', 'system', '/manage/admin/userSetting');
INSERT INTO `bw_auth_group_node` VALUES ('47001', '105', '185', '/manage/auth', 'system', 'manage_auth');
INSERT INTO `bw_auth_group_node` VALUES ('47002', '105', '183', '/manage/Node/index', 'system', 'manage_node_index');
INSERT INTO `bw_auth_group_node` VALUES ('47003', '105', '1167', '/manage/Node/delete', 'system', '/manage/Node/delete');
INSERT INTO `bw_auth_group_node` VALUES ('47004', '105', '510', '/manage/Node/edit', 'system', 'manageNodeedit');
INSERT INTO `bw_auth_group_node` VALUES ('47005', '105', '1274', '/manage/Node/getMemberNodeTree', 'system', '/manage/Node/getMemberNodeTree');
INSERT INTO `bw_auth_group_node` VALUES ('47006', '105', '1272', '/manage/Node/editMember', 'system', '/manage/Node/editMember');
INSERT INTO `bw_auth_group_node` VALUES ('47007', '105', '1271', '/manage/Node/addMember', 'system', '/manage/Node/addMember');
INSERT INTO `bw_auth_group_node` VALUES ('47008', '105', '1270', '/manage/Node/memberIndex', 'system', '/manage/Node/add');
INSERT INTO `bw_auth_group_node` VALUES ('47009', '105', '1', '/manage/Role/index', 'system', 'manage_Role_indexid_2');
INSERT INTO `bw_auth_group_node` VALUES ('47010', '105', '1168', '/manage/Role/edit', 'system', '/manage/role/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47011', '105', '1066', '/manage/Role/groupRule', 'system', '/manage/Role/groupRule');
INSERT INTO `bw_auth_group_node` VALUES ('47012', '105', '1259', '/manage/Role/getNodeTree', 'system', '/manage/Role/getNodeTree');
INSERT INTO `bw_auth_group_node` VALUES ('47013', '105', '1258', '/manage/Role/editMember', 'system', '/manage/Role/editMember');
INSERT INTO `bw_auth_group_node` VALUES ('47014', '105', '1257', '/manage/Role/addMember', 'system', '/manage/Role/addMember');
INSERT INTO `bw_auth_group_node` VALUES ('47015', '105', '1256', '/manage/Role/memberRole', 'system', '/manage/Role/memberRole');
INSERT INTO `bw_auth_group_node` VALUES ('47016', '105', '1249', '/manage/Role/getRoleMemberTree', 'system', '/manage/Role/getRoleMemberTree');
INSERT INTO `bw_auth_group_node` VALUES ('47017', '105', '1247', '/manage/Role/getRoleModelTree', 'system', '/manage/Role/getRoleModelTree');
INSERT INTO `bw_auth_group_node` VALUES ('47018', '105', '1246', '/manage/Role/editModel', 'system', '/manage/Role/editModel');
INSERT INTO `bw_auth_group_node` VALUES ('47019', '105', '1245', '/manage/Role/addModel', 'system', '/manage/Role/addModel');
INSERT INTO `bw_auth_group_node` VALUES ('47020', '105', '1244', '/manage/Role/modelRole', 'system', '/manage/Role/modelRole');
INSERT INTO `bw_auth_group_node` VALUES ('47021', '105', '182', '/manage/Admin/user', 'system', 'manage_admin_user');
INSERT INTO `bw_auth_group_node` VALUES ('47022', '105', '1185', '/manage/Admin/delete', 'system', '/manage/Admin/delete');
INSERT INTO `bw_auth_group_node` VALUES ('47023', '105', '1184', '/manage/Admin/edit', 'system', '/manage/Admin/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47024', '105', '1183', '/manage/Admin/setStatus', 'system', '/manage/Admin/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('47025', '105', '1182', '/manage/Admin/add', 'system', '/manage/Admin/add');
INSERT INTO `bw_auth_group_node` VALUES ('47026', '105', '1501', '/manage/admin.sys.Log/index', 'system', 'manage_admin_sys_log_index');
INSERT INTO `bw_auth_group_node` VALUES ('47027', '105', '1506', '/manage/admin.sys.Log/del', 'system', '_manage_admin_sys_log_del');
INSERT INTO `bw_auth_group_node` VALUES ('47028', '105', '1505', '/manage/admin.sys.Log/modify', 'system', '_manage_admin_sys_log_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47029', '105', '1504', '/manage/admin.sys.Log/edit', 'system', '_manage_admin_sys_log_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47030', '105', '1503', '/manage/admin.sys.Log/add', 'system', '_manage_admin_sys_log_add');
INSERT INTO `bw_auth_group_node` VALUES ('47031', '105', '1502', '/manage/admin.sys.Log/export', 'system', '_manage_admin_sys_log_export');
INSERT INTO `bw_auth_group_node` VALUES ('47032', '105', '1545', '/', 'system', '');
INSERT INTO `bw_auth_group_node` VALUES ('47033', '105', '1341', '/manage/admin.member.walletBill', 'system', 'manage_admin_member_walletBill');
INSERT INTO `bw_auth_group_node` VALUES ('47034', '105', '1345', '/manage/admin.member.walletBill/del', 'system', '/manage/admin.member.walletBill/del');
INSERT INTO `bw_auth_group_node` VALUES ('47035', '105', '1344', '/manage/admin.member.walletBill/edit', 'system', '/manage/admin.member.walletBill/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47036', '105', '1343', '/manage/admin.member.walletBill/add', 'system', '/manage/admin.member.walletBill/add');
INSERT INTO `bw_auth_group_node` VALUES ('47037', '105', '1342', '/manage/admin.member.walletBill/index', 'system', '/manage/admin.member.walletBill/index');
INSERT INTO `bw_auth_group_node` VALUES ('47038', '105', '1331', '/manage/admin.member.wallet', 'system', 'manage_admin_member_wallet');
INSERT INTO `bw_auth_group_node` VALUES ('47039', '105', '1335', '/manage/admin.member.wallet/del', 'system', '/manage/admin.member.wallet/del');
INSERT INTO `bw_auth_group_node` VALUES ('47040', '105', '1334', '/manage/admin.member.wallet/edit', 'system', '/manage/admin.member.wallet/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47041', '105', '1333', '/manage/admin.member.wallet/add', 'system', '/manage/admin.member.wallet/add');
INSERT INTO `bw_auth_group_node` VALUES ('47042', '105', '1332', '/manage/admin.member.wallet/index', 'system', '/manage/admin.member.wallet/index');
INSERT INTO `bw_auth_group_node` VALUES ('47043', '105', '1124', '/manage/admin.member', 'system', 'manage_admin_member');
INSERT INTO `bw_auth_group_node` VALUES ('47044', '105', '1128', '/manage/admin.member/del', 'system', '/manage/admin.member/del');
INSERT INTO `bw_auth_group_node` VALUES ('47045', '105', '1127', '/manage/admin.Member/edit', 'system', '/manage/admin.Member/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47046', '105', '1126', '/manage/admin.Member/add', 'system', '/manage/admin.Member/add');
INSERT INTO `bw_auth_group_node` VALUES ('47047', '105', '1125', '/manage/admin.member/index', 'system', '/manage/admin.member/index');
INSERT INTO `bw_auth_group_node` VALUES ('47048', '105', '1587', '/manage/admin.member/sendMessage', 'system', 'manage_admin_message_send');
INSERT INTO `bw_auth_group_node` VALUES ('47049', '105', '1255', '/manage/admin.member/setStatus', 'system', '/manage/admin/member/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('47050', '105', '190', '/manage/weihu', 'system', 'manage_ceshi');
INSERT INTO `bw_auth_group_node` VALUES ('47051', '105', '206', '/manage/config/configindex', 'system', '_manage_config_configindex');
INSERT INTO `bw_auth_group_node` VALUES ('47052', '105', '1098', '/manage/Config/editconfig', 'system', '/manage/Config/editconfig');
INSERT INTO `bw_auth_group_node` VALUES ('47053', '105', '874', '/manage/Config/addconfig', 'system', 'manageConfigaddconfig');
INSERT INTO `bw_auth_group_node` VALUES ('47054', '105', '506', '/manage/Config/getconfiglist', 'system', 'manageConfiggetconfiglist');
INSERT INTO `bw_auth_group_node` VALUES ('47055', '105', '193', '/manage/config/index', 'system', '_manage_config_index');
INSERT INTO `bw_auth_group_node` VALUES ('47056', '105', '200', '/manage/Config/softdleting', 'system', '/manage/Config/softdleting');
INSERT INTO `bw_auth_group_node` VALUES ('47057', '105', '199', '/manage/Config/getlist', 'system', 'config_set_select');
INSERT INTO `bw_auth_group_node` VALUES ('47058', '105', '198', '/manage/Config/edit', 'system', '/manage/Config/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47059', '105', '197', '/manage/Config/add', 'system', 'manageConfigadd');
INSERT INTO `bw_auth_group_node` VALUES ('47060', '105', '202', '/manage/Config/showconfig', 'system', '_manage_config_showconfig');
INSERT INTO `bw_auth_group_node` VALUES ('47061', '105', '508', '/manage/Config/setValues', 'system', '/manage/Config/setValues');
INSERT INTO `bw_auth_group_node` VALUES ('47062', '105', '196', '/manage/Gruop/set', 'system', 'manage_gruop_set');
INSERT INTO `bw_auth_group_node` VALUES ('47063', '105', '211', '/manage/GroupData/index', 'system', '_manage_GroupData_index');
INSERT INTO `bw_auth_group_node` VALUES ('47064', '105', '513', '/manage/GroupData/edit', 'system', 'manageGroupDataedit');
INSERT INTO `bw_auth_group_node` VALUES ('47065', '105', '505', '/manage/GroupData/add', 'system', 'manageGroupDataadd');
INSERT INTO `bw_auth_group_node` VALUES ('47066', '105', '504', '/manage/GroupData/getconfiglist', 'system', 'manageGroupDatagetconfiglist');
INSERT INTO `bw_auth_group_node` VALUES ('47067', '105', '1534', '/manage/GroupData/softdleting', 'system', '_manage_GroupData_softdleting');
INSERT INTO `bw_auth_group_node` VALUES ('47068', '105', '1362', '/manage/GroupData/setConfigShow', 'system', '/manage/GroupData/setConfigShow');
INSERT INTO `bw_auth_group_node` VALUES ('47069', '105', '210', '/manage/Group', 'system', 'manage_group');
INSERT INTO `bw_auth_group_node` VALUES ('47070', '105', '1166', '/manage/Group/index', 'system', '/manage/Group/index');
INSERT INTO `bw_auth_group_node` VALUES ('47071', '105', '512', '/manage/Group/editconfig', 'system', 'manageGroupeditconfig');
INSERT INTO `bw_auth_group_node` VALUES ('47072', '105', '511', '/manage/Group/edit', 'system', 'manageGroupedit');
INSERT INTO `bw_auth_group_node` VALUES ('47073', '105', '507', '/manage/Group/addconfig', 'system', 'manageGroupaddconfig');
INSERT INTO `bw_auth_group_node` VALUES ('47074', '105', '503', '/manage/Group/add', 'system', 'manageGroupadd');
INSERT INTO `bw_auth_group_node` VALUES ('47075', '105', '497', '/manage/Group/getconfiglist', 'system', 'manageGroupgetconfiglist');
INSERT INTO `bw_auth_group_node` VALUES ('47076', '105', '2170', '/manage/admin.sys.City/index', 'system', 'manage_admin_sys_city_index');
INSERT INTO `bw_auth_group_node` VALUES ('47077', '105', '2176', '/manage/admin.sys.City/download', 'system', 'manage_admin_sys_city_download');
INSERT INTO `bw_auth_group_node` VALUES ('47078', '105', '2175', '/manage/admin.sys.City/del', 'system', 'manage_admin_sys_city_del');
INSERT INTO `bw_auth_group_node` VALUES ('47079', '105', '2174', '/manage/admin.sys.City/modify', 'system', 'manage_admin_sys_city_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47080', '105', '2173', '/manage/admin.sys.City/edit', 'system', 'manage_admin_sys_city_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47081', '105', '2172', '/manage/admin.sys.City/add', 'system', 'manage_admin_sys_city_add');
INSERT INTO `bw_auth_group_node` VALUES ('47082', '105', '2171', '/manage/admin.sys.City/export', 'system', 'manage_admin_sys_city_export');
INSERT INTO `bw_auth_group_node` VALUES ('47083', '105', '1586', '/manage/sysArticle', 'system', 'manage_sysArticle');
INSERT INTO `bw_auth_group_node` VALUES ('47084', '105', '856', '/manage/admin.SysArticleCategory', 'system', 'manage_admin_sysArticleCategory');
INSERT INTO `bw_auth_group_node` VALUES ('47085', '105', '860', '/manage/admin.SysArticleCategory/del', 'system', '/manage/admin.SysArticleCategory/del');
INSERT INTO `bw_auth_group_node` VALUES ('47086', '105', '859', '/manage/admin.SysArticleCategory/edit', 'system', '/manage/admin.SysArticleCategory/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47087', '105', '858', '/manage/admin.SysArticleCategory/add', 'system', '/manage/admin.SysArticleCategory/add');
INSERT INTO `bw_auth_group_node` VALUES ('47088', '105', '857', '/manage/admin.SysArticleCategory/index', 'system', '/manage/admin.SysArticleCategory/index');
INSERT INTO `bw_auth_group_node` VALUES ('47089', '105', '851', '/manage/admin.SysArticle', 'system', 'manage_admin_sysArticle');
INSERT INTO `bw_auth_group_node` VALUES ('47090', '105', '855', '/manage/admin.SysArticle/del', 'system', '/manage/admin.SysArticle/del');
INSERT INTO `bw_auth_group_node` VALUES ('47091', '105', '854', '/manage/admin.SysArticle/edit', 'system', '/manage/admin.SysArticle/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47092', '105', '853', '/manage/admin.SysArticle/add', 'system', '/manage/admin.SysArticle/add');
INSERT INTO `bw_auth_group_node` VALUES ('47093', '105', '852', '/manage/admin.SysArticle/index', 'system', '/manage/admin.SysArticle/index');
INSERT INTO `bw_auth_group_node` VALUES ('47094', '105', '1516', '/manage/admin.sys.Notice/index', 'system', 'manage_admin_sys_notice_index');
INSERT INTO `bw_auth_group_node` VALUES ('47095', '105', '1521', '/manage/admin.sys.Notice/del', 'system', '_manage_admin_sys_notice_del');
INSERT INTO `bw_auth_group_node` VALUES ('47096', '105', '1520', '/manage/admin.sys.Notice/modify', 'system', '_manage_admin_sys_notice_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47097', '105', '1519', '/manage/admin.sys.Notice/edit', 'system', '_manage_admin_sys_notice_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47098', '105', '1518', '/manage/admin.sys.Notice/add', 'system', '_manage_admin_sys_notice_add');
INSERT INTO `bw_auth_group_node` VALUES ('47099', '105', '1517', '/manage/admin.sys.Notice/export', 'system', '_manage_admin_sys_notice_export');
INSERT INTO `bw_auth_group_node` VALUES ('47100', '105', '1391', '/manage/admin.Uploadfile/index', 'system', 'manage_uploadfile_index');
INSERT INTO `bw_auth_group_node` VALUES ('47101', '105', '1394', '/manage/admin.Uploadfile/export', 'system', '_manage_uploadfile_export');
INSERT INTO `bw_auth_group_node` VALUES ('47102', '105', '1393', '/manage/admin.Uploadfile/del', 'system', '_manage_uploadfile_del');
INSERT INTO `bw_auth_group_node` VALUES ('47103', '105', '1392', '/manage/admin.Uploadfile/add', 'system', '_manage_uploadfile_add');
INSERT INTO `bw_auth_group_node` VALUES ('47104', '105', '1365', '/manage/publicCommon/uploadimg', 'system', '/manage/publicCommon/uploadimg');
INSERT INTO `bw_auth_group_node` VALUES ('47105', '105', '1307', '/manage/publicCommon/upload', 'system', 'public_common_upload');
INSERT INTO `bw_auth_group_node` VALUES ('47106', '105', '1114', '/manage/crudDemo', 'system', 'manage_crudDemo');
INSERT INTO `bw_auth_group_node` VALUES ('47107', '105', '1118', '/manage/crudDemo/del', 'system', 'manage_crudDemo_del');
INSERT INTO `bw_auth_group_node` VALUES ('47108', '105', '1117', '/manage/crudDemo/edit', 'system', '/manage/crudDemo/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47109', '105', '1116', '/manage/crudDemo/add', 'system', '/manage/crudDemo/add');
INSERT INTO `bw_auth_group_node` VALUES ('47110', '105', '1115', '/manage/crudDemo/index', 'system', '/manage/crudDemo/index');
INSERT INTO `bw_auth_group_node` VALUES ('47111', '105', '807', '/manage/admin.SysCrud', 'system', 'manage_admin_sysCrud');
INSERT INTO `bw_auth_group_node` VALUES ('47112', '105', '811', '/manage/admin.SysCrud/del', 'system', '/manage/admin.SysCrud/del');
INSERT INTO `bw_auth_group_node` VALUES ('47113', '105', '810', '/manage/admin.SysCrud/edit', 'system', '/manage/admin.SysCrud/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47114', '105', '809', '/manage/admin.SysCrud/add', 'system', '/manage/admin.SysCrud/add');
INSERT INTO `bw_auth_group_node` VALUES ('47115', '105', '808', '/manage/admin.SysCrud/index', 'system', '/manage/admin.SysCrud/index');
INSERT INTO `bw_auth_group_node` VALUES ('47116', '105', '188', '/plugin', 'system', '_plugin');
INSERT INTO `bw_auth_group_node` VALUES ('47117', '105', '891', '/manage/admin.Plugin', 'system', '_manage_admin_Plugin');
INSERT INTO `bw_auth_group_node` VALUES ('47118', '105', '900', '/manage/admin.Plugin/config', 'system', '/manage/admin.Plugin/config');
INSERT INTO `bw_auth_group_node` VALUES ('47119', '105', '898', '/manage/admin.Plugin/enable', 'system', '/manage/admin.Plugin/enable');
INSERT INTO `bw_auth_group_node` VALUES ('47120', '105', '897', '/manage/admin.Plugin/uninstall', 'system', '/manage/admin.Plugin/uninstall');
INSERT INTO `bw_auth_group_node` VALUES ('47121', '105', '896', '/manage/admin.Plugin/install', 'system', '/manage/admin.Plugin/install');
INSERT INTO `bw_auth_group_node` VALUES ('47122', '105', '895', '/manage/admin.Plugin/del', 'system', '/manage/admin.Plugin/del');
INSERT INTO `bw_auth_group_node` VALUES ('47123', '105', '894', '/manage/admin.Plugin/edit', 'system', '/manage/admin.Plugin/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47124', '105', '893', '/manage/admin.Plugin/add', 'system', '/manage/admin.Plugin/add');
INSERT INTO `bw_auth_group_node` VALUES ('47125', '105', '892', '/manage/admin.Plugin/index', 'system', '/manage/admin.Plugin/index');
INSERT INTO `bw_auth_group_node` VALUES ('47126', '105', '1526', '/manage/admin.Plugin/disable', 'system', '/manage/admin.Plugin/disable');
INSERT INTO `bw_auth_group_node` VALUES ('47127', '105', '1306', 'cloud', 'system', 'cloud');
INSERT INTO `bw_auth_group_node` VALUES ('47128', '105', '1295', '/manage/admin.cloud.order', 'system', 'manage_admin_cloud_order');
INSERT INTO `bw_auth_group_node` VALUES ('47129', '105', '1299', '/manage/admin.cloud.order/del', 'system', '/manage/admin.cloud.order/del');
INSERT INTO `bw_auth_group_node` VALUES ('47130', '105', '1298', '/manage/admin.cloud.order/edit', 'system', '/manage/admin.cloud.order/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47131', '105', '1297', '/manage/admin.cloud.order/add', 'system', '/manage/admin.cloud.order/add');
INSERT INTO `bw_auth_group_node` VALUES ('47132', '105', '1296', '/manage/admin.cloud.order/index', 'system', '/manage/admin.cloud.order/index');
INSERT INTO `bw_auth_group_node` VALUES ('47133', '105', '1290', '/manage/admin.cloud.walletBill', 'system', '/manage/admin.cloud.walletBill');
INSERT INTO `bw_auth_group_node` VALUES ('47134', '105', '1294', '/manage/admin.cloud.walletBill/del', 'system', '/manage/admin.cloud.walletBill/del');
INSERT INTO `bw_auth_group_node` VALUES ('47135', '105', '1293', '/manage/admin.cloud.walletBill/edit', 'system', '/manage/admin.cloud.walletBill/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47136', '105', '1292', '/manage/admin.cloud.walletBill/add', 'system', '/manage/admin.cloud.walletBill/add');
INSERT INTO `bw_auth_group_node` VALUES ('47137', '105', '1291', '/manage/admin.cloud.walletBill/index', 'system', '/manage/admin.cloud.walletBill/index');
INSERT INTO `bw_auth_group_node` VALUES ('47138', '105', '1285', '/manage/admin.cloud.wallet', 'system', '/manage/admin.cloud.wallet');
INSERT INTO `bw_auth_group_node` VALUES ('47139', '105', '1289', '/manage/admin.cloud.wallet/del', 'system', '/manage/admin.cloud.wallet/del');
INSERT INTO `bw_auth_group_node` VALUES ('47140', '105', '1288', '/manage/admin.cloud.wallet/edit', 'system', '/manage/admin.cloud.wallet/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47141', '105', '1287', '/manage/admin.cloud.wallet/add', 'system', '/manage/admin.cloud.wallet/add');
INSERT INTO `bw_auth_group_node` VALUES ('47142', '105', '1286', '/manage/admin.cloud.wallet/index', 'system', '/manage/admin.cloud.wallet/index');
INSERT INTO `bw_auth_group_node` VALUES ('47143', '105', '1280', '/manage/admin.cloud.packet', 'system', '/manage/admin.cloud.packet');
INSERT INTO `bw_auth_group_node` VALUES ('47144', '105', '1284', '/manage/admin.cloud.packet/del', 'system', '/manage/admin.cloud.packet/del');
INSERT INTO `bw_auth_group_node` VALUES ('47145', '105', '1283', '/manage/admin.cloud.packet/edit', 'system', '/manage/admin.cloud.packet/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47146', '105', '1282', '/manage/admin.cloud.packet/add', 'system', '/manage/admin.cloud.packet/add');
INSERT INTO `bw_auth_group_node` VALUES ('47147', '105', '1281', '/manage/admin.cloud.packet/index', 'system', '/manage/admin.cloud.packet/index');
INSERT INTO `bw_auth_group_node` VALUES ('47148', '105', '1275', '/manage/admin.cloud.service', 'system', '_manage_admin_cloud_service');
INSERT INTO `bw_auth_group_node` VALUES ('47149', '105', '1279', '/manage/admin.cloud.service/del', 'system', '/manage/admin.cloud.service/del');
INSERT INTO `bw_auth_group_node` VALUES ('47150', '105', '1278', '/manage/admin.cloud.service/edit', 'system', '/manage/admin.cloud.service/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47151', '105', '1277', '/manage/admin.cloud.service/add', 'system', '/manage/admin.cloud.service/add');
INSERT INTO `bw_auth_group_node` VALUES ('47152', '105', '1276', '/manage/admin.cloud.service/index', 'system', '/manage/admin.cloud.service/index');
INSERT INTO `bw_auth_group_node` VALUES ('47153', '105', '187', '/app', 'system', '_app');
INSERT INTO `bw_auth_group_node` VALUES ('47154', '105', '1093', '/manage/admin.MemberMiniapp', 'system', '_manage_admin_memberMiniapp');
INSERT INTO `bw_auth_group_node` VALUES ('47155', '105', '1097', '/manage/admin.MemberMiniapp/del', 'system', '/manage/admin.MemberMiniapp/del');
INSERT INTO `bw_auth_group_node` VALUES ('47156', '105', '1096', '/manage/admin.MemberMiniapp/edit', 'system', '/manage/admin.MemberMiniapp/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47157', '105', '1095', '/manage/admin.MemberMiniapp/add', 'system', '/manage/admin.MemberMiniapp/add');
INSERT INTO `bw_auth_group_node` VALUES ('47158', '105', '1094', '/manage/admin.MemberMiniapp/index', 'system', '/manage/admin.MemberMiniapp/index');
INSERT INTO `bw_auth_group_node` VALUES ('47159', '105', '1020', '/manage/admin.Miniapp', 'system', '_manage_admin_miniapp');
INSERT INTO `bw_auth_group_node` VALUES ('47160', '105', '1026', '/manage/admin.Miniapp/buy', 'system', '/manage/admin.Miniapp/buy');
INSERT INTO `bw_auth_group_node` VALUES ('47161', '105', '1025', '/manage/admin.Miniapp/install', 'system', '/manage/admin.Miniapp/install');
INSERT INTO `bw_auth_group_node` VALUES ('47162', '105', '1024', '/manage/admin.Miniapp/uninstall', 'system', '/manage/admin.Miniapp/uninstall');
INSERT INTO `bw_auth_group_node` VALUES ('47163', '105', '1023', '/manage/admin.Miniapp/edit', 'system', '/manage/admin.Miniapp/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47164', '105', '1022', '/manage/admin.Miniapp/add', 'system', '/manage/admin.Miniapp/add');
INSERT INTO `bw_auth_group_node` VALUES ('47165', '105', '1021', '/manage/admin.Miniapp/index', 'system', '/manage/admin.Miniapp/index');
INSERT INTO `bw_auth_group_node` VALUES ('47166', '105', '1528', '/bwmall/admin.Template/index', 'system', '_manage_admin_miniapp_module_index');
INSERT INTO `bw_auth_group_node` VALUES ('47167', '105', '1533', '/bwmall/admin.Template/modify', 'system', 'manage_admin_miniapp_module_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47168', '105', '1532', '/bwmall/admin.Template/del', 'system', 'manage_admin_miniapp_module_del');
INSERT INTO `bw_auth_group_node` VALUES ('47169', '105', '1531', '/bwmall/admin.Template/edit', 'system', 'manage_admin_miniapp_module_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47170', '105', '1530', '/bwmall/admin.Template/add', 'system', 'manage_admin_miniapp_module_add');
INSERT INTO `bw_auth_group_node` VALUES ('47171', '105', '1529', '/bwmall/admin.Template/export', 'system', 'manage_admin_miniapp_module_export');
INSERT INTO `bw_auth_group_node` VALUES ('47172', '62', '828', '/manage/member.index/index', 'system', '_manage_member_index_index');
INSERT INTO `bw_auth_group_node` VALUES ('47173', '62', '1443', '/manage/member.Index/dashboard', 'system', '_manage_member_dashboard');
INSERT INTO `bw_auth_group_node` VALUES ('47174', '62', '1585', '/manage/member.Index/messageRead', 'system', 'manage_member_message_read');
INSERT INTO `bw_auth_group_node` VALUES ('47175', '62', '1584', '/manage/member.Index/messageList', 'system', 'manage_member_message_list');
INSERT INTO `bw_auth_group_node` VALUES ('47176', '62', '1523', '/manage/member.Index/getEchartsInfo', 'system', '_manage_member_echartsInfo');
INSERT INTO `bw_auth_group_node` VALUES ('47177', '62', '1522', '/manage/member.Index/getNoticeList', 'system', '_manage_member_noticeList');
INSERT INTO `bw_auth_group_node` VALUES ('47178', '62', '1515', '/manage/member.Index/getMemberLoginInfo', 'system', '_manage_member_loginInfo');
INSERT INTO `bw_auth_group_node` VALUES ('47179', '62', '1349', '/manage/member.walletBill', 'system', 'manage_member_walletBill');
INSERT INTO `bw_auth_group_node` VALUES ('47180', '62', '1350', '/manage/member.walletBill/index', 'system', '/manage/member.walletBill/index');
INSERT INTO `bw_auth_group_node` VALUES ('47181', '62', '830', '/manage/member.Role/index', 'system', '_manage_member_Role_index');
INSERT INTO `bw_auth_group_node` VALUES ('47182', '62', '863', '/manage/member.Role/getRoleTree', 'system', '/manage/member.Role/getRoleTree');
INSERT INTO `bw_auth_group_node` VALUES ('47183', '62', '1238', '/manage/member.Role/edit', 'system', '/manage/member.Role/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47184', '62', '1237', '/manage/member.Role/setStatus', 'system', '/manage/member.Role/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('47185', '62', '1236', '/manage/member.Role/delete', 'system', '/manage/member.Role/delete');
INSERT INTO `bw_auth_group_node` VALUES ('47186', '62', '1221', '/manage/member.Role/getNodeTree', 'system', '/manage/member.Role/getNodeTree');
INSERT INTO `bw_auth_group_node` VALUES ('47187', '62', '1161', '/manage/member.Role/add', 'system', '/manage/member.Role/add');
INSERT INTO `bw_auth_group_node` VALUES ('47188', '62', '829', '/manage/member.Login/user', 'system', 'manage_member_Login_user');
INSERT INTO `bw_auth_group_node` VALUES ('47189', '62', '1242', '/manage/member.Login/setStatus', 'system', '/manage/member.Login/setStatus');
INSERT INTO `bw_auth_group_node` VALUES ('47190', '62', '1240', '/manage/member.Login/edit', 'system', '/manage/member.Login/edit');
INSERT INTO `bw_auth_group_node` VALUES ('47191', '62', '1239', '/manage/member.Login/add', 'system', '/manage/member.Login/add');
INSERT INTO `bw_auth_group_node` VALUES ('47192', '62', '1346', '/config', 'system', '_config');
INSERT INTO `bw_auth_group_node` VALUES ('47193', '62', '1396', '/manage/member.GroupData/index', 'system', '_manage_member_groupData');
INSERT INTO `bw_auth_group_node` VALUES ('47194', '62', '1604', '/manage/member.GroupData/softdleting', 'system', 'manage_member_GroupData_del');
INSERT INTO `bw_auth_group_node` VALUES ('47195', '62', '1363', '/manage/member.GroupData/setConfigShow', 'system', '_manage_member_groupData_setConfigShow');
INSERT INTO `bw_auth_group_node` VALUES ('47196', '62', '1361', '/manage/member.GroupData/getConfigList', 'system', '_manage_member_groupData_getConfigList');
INSERT INTO `bw_auth_group_node` VALUES ('47197', '62', '1360', '/manage/member.GroupData/softdleting', 'system', '_manage_member_groupData_softdleting');
INSERT INTO `bw_auth_group_node` VALUES ('47198', '62', '1359', '/manage/member.GroupData/add', 'system', '_manage_member_groupData_add');
INSERT INTO `bw_auth_group_node` VALUES ('47199', '62', '1358', '/manage/member.GroupData/edit', 'system', '_manage_member_groupData_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47200', '62', '1347', '/manage/member.Config/showMemberConfig', 'system', '_manage_member_config_showMemberConfig_1');
INSERT INTO `bw_auth_group_node` VALUES ('47201', '62', '1348', '/manage/member.Config/setMemberValues', 'system', '_manage_member_config_setMemberValues');
INSERT INTO `bw_auth_group_node` VALUES ('47202', '62', '1408', '/manage/member.Recharge/index', 'system', '_manage_member_recharge');
INSERT INTO `bw_auth_group_node` VALUES ('47203', '62', '1411', '/manage/member.Recharge/pay', 'system', '_manage_member_recharge_pay');
INSERT INTO `bw_auth_group_node` VALUES ('47204', '62', '1508', '/manage/member.sys.Log/index', 'system', 'manage_member_sys_log_index');
INSERT INTO `bw_auth_group_node` VALUES ('47205', '62', '1512', '/manage/member.sys.Log/modify', 'system', '_manage_member_sys_log_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47206', '62', '1511', '/manage/member.sys.Log/edit', 'system', '_manage_member_sys_log_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47207', '62', '1510', '/manage/member.sys.Log/add', 'system', '_manage_member_sys_log_add');
INSERT INTO `bw_auth_group_node` VALUES ('47208', '62', '1509', '/manage/member.sys.Log/export', 'system', '_manage_member_sys_log_export');
INSERT INTO `bw_auth_group_node` VALUES ('47209', '62', '1172', '/app/member', 'system', '_app_member');
INSERT INTO `bw_auth_group_node` VALUES ('47210', '62', '1105', '/manage/member.miniapp/index', 'system', 'manage_member_miniapp_index');
INSERT INTO `bw_auth_group_node` VALUES ('47211', '62', '1108', '/manage/member.miniapp/buy', 'system', '/manage/member.miniapp/buy');
INSERT INTO `bw_auth_group_node` VALUES ('47212', '62', '1107', '/manage/member.miniapp/detail', 'system', '/manage/member.miniapp/detail');
INSERT INTO `bw_auth_group_node` VALUES ('47213', '62', '1589', '/manage/member.MemberMiniapp/index', 'system', 'manage_member_miniapp_my_index');
INSERT INTO `bw_auth_group_node` VALUES ('47214', '62', '2101', '/manage/member.MemberMiniapp/setDefaultMiniapp', 'system', '_manage_member_member_miniapp_default');
INSERT INTO `bw_auth_group_node` VALUES ('47215', '62', '834', '/bwmall/admin/confg', 'system', 'bwmall_admin_confg');
INSERT INTO `bw_auth_group_node` VALUES ('47216', '62', '1250', '/manage/member.wechatKeyword/index', 'system', 'manage_member_wechatKeyword_index');
INSERT INTO `bw_auth_group_node` VALUES ('47217', '62', '1254', '/manage/member.wechatKeyword/del', 'system', 'manage_member_wechatKeyword_del');
INSERT INTO `bw_auth_group_node` VALUES ('47218', '62', '1253', '/manage/member.wechatKeyword/edit', 'system', 'manage_member_wechatKeyword_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47219', '62', '1252', '/manage/member.wechatKeyword/add', 'system', 'manage_member_wechatKeyword_add');
INSERT INTO `bw_auth_group_node` VALUES ('47220', '62', '1567', '/manage/member.wechatKeyword/modify', 'system', 'manage_member_wechatKeyword_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47221', '62', '1565', '/manage/member.wechatKeyword/previewVoice', 'system', 'manage_member_wechatKeyword_previewVoice');
INSERT INTO `bw_auth_group_node` VALUES ('47222', '62', '1564', '/manage/member.wechatKeyword/previewVideo', 'system', 'manage_member_wechatKeyword_previewVideo');
INSERT INTO `bw_auth_group_node` VALUES ('47223', '62', '1563', '/manage/member.wechatKeyword/previewMusic', 'system', 'manage_member_wechatKeyword_previewMusic');
INSERT INTO `bw_auth_group_node` VALUES ('47224', '62', '1562', '/manage/member.wechatKeyword/previewImage', 'system', 'manage_member_wechatKeyword_previewImage');
INSERT INTO `bw_auth_group_node` VALUES ('47225', '62', '1561', '/manage/member.wechatKeyword/select', 'system', 'manage_member_wechatKeyword_select');
INSERT INTO `bw_auth_group_node` VALUES ('47226', '62', '1560', '/manage/member.wechatKeyword/previewNews', 'system', 'manage_member_wechatKeyword_previewNews');
INSERT INTO `bw_auth_group_node` VALUES ('47227', '62', '1559', '/manage/member.wechatKeyword/previewText', 'system', 'manage_member_wechatKeyword_previewText');
INSERT INTO `bw_auth_group_node` VALUES ('47228', '62', '1558', '/manage/member.wechatKeyword/addKeys', 'system', 'manage_member_wechatKeyword_addKeys');
INSERT INTO `bw_auth_group_node` VALUES ('47229', '62', '1069', '/manage/member.official/index', 'system', 'manage_member_official_index');
INSERT INTO `bw_auth_group_node` VALUES ('47230', '62', '1081', '/manage/member.official/sort', 'system', 'manage_member_official_sort');
INSERT INTO `bw_auth_group_node` VALUES ('47231', '62', '1079', '/manage/member.official/sync', 'system', 'manage_member_official_sync');
INSERT INTO `bw_auth_group_node` VALUES ('47232', '62', '1078', '/manage/member.official/del', 'system', 'manage_member_official_del');
INSERT INTO `bw_auth_group_node` VALUES ('47233', '62', '1077', '/manage/member.official/edit', 'system', 'manage_member_official_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47234', '62', '1076', '/manage/member.official/add', 'system', 'manage_member_official_add');
INSERT INTO `bw_auth_group_node` VALUES ('47235', '62', '1458', '/manage/member.official/materialList', 'system', 'manage_member_official_materialList');
INSERT INTO `bw_auth_group_node` VALUES ('47236', '62', '835', '/manage/member.setting/index', 'system', 'manage_member_setting_index');
INSERT INTO `bw_auth_group_node` VALUES ('47237', '62', '1068', '/manage/member.setting/alipay', 'system', 'manage_member_setting_alipay');
INSERT INTO `bw_auth_group_node` VALUES ('47238', '62', '1067', '/manage/member.setting/wechat', 'system', 'manage_member_setting_wechat');
INSERT INTO `bw_auth_group_node` VALUES ('47239', '62', '1054', '/manage/member.setting/edit', 'system', 'manage_member_setting_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47240', '62', '1583', '/manage/member.WechatKeyword/subscribe', 'system', 'manage_member_WechatKeyword_subscribe');
INSERT INTO `bw_auth_group_node` VALUES ('47241', '62', '1500', '/manage/member.wechatKeyword/materialList', 'system', 'manage_member_wechatKeyword_materialList');
INSERT INTO `bw_auth_group_node` VALUES ('47242', '62', '1590', '/manage/member.wechatKeyword/editMaterial', 'system', 'manage_member_wechatKeyword_editMaterial');
INSERT INTO `bw_auth_group_node` VALUES ('47243', '62', '1566', '/manage/member.wechatKeyword/addMaterial', 'system', 'manage_member_wechatKeyword_addMaterial');
INSERT INTO `bw_auth_group_node` VALUES ('47244', '62', '1507', '/manage/member.wechatKeyword/removeMaterial', 'system', 'manage_member_wechatKeyword_removeMaterial');
INSERT INTO `bw_auth_group_node` VALUES ('47245', '62', '1539', '/manage/member.sys.Express/index', 'system', 'manage_member_sys_express_index');
INSERT INTO `bw_auth_group_node` VALUES ('47246', '62', '1544', '/manage/member.sys.Express/del', 'system', 'manage_member_sys_express_del');
INSERT INTO `bw_auth_group_node` VALUES ('47247', '62', '1543', '/manage/member.sys.Express/modify', 'system', 'manage_member_sys_express_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47248', '62', '1542', '/manage/member.sys.Express/edit', 'system', 'manage_member_sys_express_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47249', '62', '1541', '/manage/member.sys.Express/add', 'system', 'manage_member_sys_express_add');
INSERT INTO `bw_auth_group_node` VALUES ('47250', '62', '1540', '/manage/member.sys.Express/export', 'system', 'manage_member_sys_express_export');
INSERT INTO `bw_auth_group_node` VALUES ('47251', '62', '1471', '/app/all/user', 'system', 'app_all_user');
INSERT INTO `bw_auth_group_node` VALUES ('47252', '62', '1485', '/manage/member/extract', 'system', '_manage_member_extract');
INSERT INTO `bw_auth_group_node` VALUES ('47253', '62', '1493', '/manage/member.user.ExtractLog/index', 'system', 'manage_member_user_extractLog_index');
INSERT INTO `bw_auth_group_node` VALUES ('47254', '62', '1498', '/manage/member.user.ExtractLog/del', 'system', '_manage_member_user_extractLog_del');
INSERT INTO `bw_auth_group_node` VALUES ('47255', '62', '1497', '/manage/member.user.ExtractLog/modify', 'system', '_manage_member_user_extractLog_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47256', '62', '1496', '/manage/member.user.ExtractLog/edit', 'system', '_manage_member_user_extractLog_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47257', '62', '1495', '/manage/member.user.ExtractLog/add', 'system', '_manage_member_user_extractLog_add');
INSERT INTO `bw_auth_group_node` VALUES ('47258', '62', '1494', '/manage/member.user.ExtractLog/export', 'system', '_manage_member_user_extractLog_export');
INSERT INTO `bw_auth_group_node` VALUES ('47259', '62', '1487', '/manage/member.user.Extract/index', 'system', 'manage_member_user_extract_index');
INSERT INTO `bw_auth_group_node` VALUES ('47260', '62', '1492', '/manage/member.user.Extract/del', 'system', '_manage_member_user_extract_del');
INSERT INTO `bw_auth_group_node` VALUES ('47261', '62', '1491', '/manage/member.user.Extract/modify', 'system', '_manage_member_user_extract_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47262', '62', '1490', '/manage/member.user.Extract/edit', 'system', '_manage_member_user_extract_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47263', '62', '1489', '/manage/member.user.Extract/add', 'system', '_manage_member_user_extract_add');
INSERT INTO `bw_auth_group_node` VALUES ('47264', '62', '1488', '/manage/member.user.Extract/export', 'system', '_manage_member_user_extract_export');
INSERT INTO `bw_auth_group_node` VALUES ('47265', '62', '1486', '/manage/member/config.ShowMemberConfig', 'system', 'manage_member_config_showMemberConfig_0_55');
INSERT INTO `bw_auth_group_node` VALUES ('47266', '62', '1472', '/manage/member.user.Bill/index', 'system', 'manage_member_user_bill_index');
INSERT INTO `bw_auth_group_node` VALUES ('47267', '62', '1476', '/manage/member.user.Bill/modify', 'system', '_manage_member_user_bill_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47268', '62', '1475', '/manage/member.user.Bill/edit', 'system', '_manage_member_user_bill_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47269', '62', '1474', '/manage/member.user.Bill/add', 'system', '_manage_member_user_bill_add');
INSERT INTO `bw_auth_group_node` VALUES ('47270', '62', '1473', '/manage/member.user.Bill/export', 'system', '_manage_member_user_bill_export');
INSERT INTO `bw_auth_group_node` VALUES ('47271', '62', '1462', 'rechange/config', 'system', 'rechange_config');
INSERT INTO `bw_auth_group_node` VALUES ('47272', '62', '1465', '/manage/member.user.Recharge/index', 'system', 'manage_member_user_recharge_index');
INSERT INTO `bw_auth_group_node` VALUES ('47273', '62', '1469', '/manage/member.user.Recharge/modify', 'system', '_manage_member_user_recharge_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47274', '62', '1468', '/manage/member.user.Recharge/export', 'system', '_manage_member_user_recharge_export');
INSERT INTO `bw_auth_group_node` VALUES ('47275', '62', '1467', '/manage/member.user.Recharge/edit', 'system', '_manage_member_user_recharge_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47276', '62', '1466', '/manage/member.user.Recharge/add', 'system', '_manage_member_user_recharge_add');
INSERT INTO `bw_auth_group_node` VALUES ('47277', '62', '1464', '/manage/member.GroupData/index', 'system', 'manage_member_groupData_recharge_select');
INSERT INTO `bw_auth_group_node` VALUES ('47278', '62', '1463', '/manage/member/config.ShowMemberConfig', 'system', 'manage_member_config_showMemberConfig_0_54');
INSERT INTO `bw_auth_group_node` VALUES ('47279', '62', '1412', '/manage/member.User/index', 'system', 'manage_member_user_index');
INSERT INTO `bw_auth_group_node` VALUES ('47280', '62', '1588', '/manage/member.User/sendMessage', 'system', 'manage_member_message_send');
INSERT INTO `bw_auth_group_node` VALUES ('47281', '62', '1419', '/manage/member.User/changeMoney', 'system', '_manage_member_user_changeMoney');
INSERT INTO `bw_auth_group_node` VALUES ('47282', '62', '1418', '/manage/member.User/editPw', 'system', '_manage_member_user_editPw');
INSERT INTO `bw_auth_group_node` VALUES ('47283', '62', '1417', '/manage/member.User/modify', 'system', '_manage_member_user_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47284', '62', '1416', '/manage/member.User/export', 'system', '_manage_member_user_export');
INSERT INTO `bw_auth_group_node` VALUES ('47285', '62', '1414', '/manage/member.User/edit', 'system', '_manage_member_user_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47286', '62', '1413', '/manage/member.User/add', 'system', '_manage_member_user_add');
INSERT INTO `bw_auth_group_node` VALUES ('47287', '62', '1314', 'member/addons', 'system', 'member/addons');
INSERT INTO `bw_auth_group_node` VALUES ('47288', '62', '1840', '/manage/member.plugin.Plugin/index', 'system', '');
INSERT INTO `bw_auth_group_node` VALUES ('47289', '62', '1842', '/manage/member.plugin.Plugin/buy', 'system', '');
INSERT INTO `bw_auth_group_node` VALUES ('47290', '62', '1841', '/manage/member.plugin.Plugin/detail', 'system', '');
INSERT INTO `bw_auth_group_node` VALUES ('47291', '62', '1315', 'member/cloud', 'system', 'member_cloud');
INSERT INTO `bw_auth_group_node` VALUES ('47292', '62', '1354', '/manage/member.cloud.order', 'system', '/manage/member.cloud.order');
INSERT INTO `bw_auth_group_node` VALUES ('47293', '62', '1355', '/manage/member.cloud.order/index', 'system', '/manage/member.cloud.order/index');
INSERT INTO `bw_auth_group_node` VALUES ('47294', '62', '1324', '/manage/member.cloud.walletBill', 'system', '/manage/member.cloud.walletBill');
INSERT INTO `bw_auth_group_node` VALUES ('47295', '62', '1325', '/manage/member.cloud.walletBill/index', 'system', '/manage/member.cloud.walletBill/index');
INSERT INTO `bw_auth_group_node` VALUES ('47296', '62', '1317', '/manage/member.cloud.packet', 'system', '/manage/member.cloud.packet');
INSERT INTO `bw_auth_group_node` VALUES ('47297', '62', '1322', '/manage/member.cloud.packet/buy', 'system', '/manage/member.cloud.packet/buy');
INSERT INTO `bw_auth_group_node` VALUES ('47298', '62', '1318', '/manage/member.cloud.packet/index', 'system', '/manage/member.cloud.packet/index');
INSERT INTO `bw_auth_group_node` VALUES ('47299', '62', '1309', '/manage/member.cloud.service', 'system', '_manage_member_cloud_service');
INSERT INTO `bw_auth_group_node` VALUES ('47300', '62', '1310', '/manage/member.cloud.service/index', 'system', '/manage/member.cloud.service/index');
INSERT INTO `bw_auth_group_node` VALUES ('47301', '62', '862', '/member', 'system', 'member');
INSERT INTO `bw_auth_group_node` VALUES ('47302', '62', '1171', '/manage/member.Index/dashboard', 'system', '/manage/member/dashboard');
INSERT INTO `bw_auth_group_node` VALUES ('47303', '62', '1169', '/manage/member.Index/menu', 'system', '/manage/member/menu');
INSERT INTO `bw_auth_group_node` VALUES ('47304', '62', '861', '/manage/member.index/logout', 'system', '/manage/member/logout');
INSERT INTO `bw_auth_group_node` VALUES ('47305', '62', '1537', '/manage/member.Miniapp/resetAudit', 'system', 'manage_member_resetAudit');
INSERT INTO `bw_auth_group_node` VALUES ('47306', '62', '1536', '/manage/member.Miniapp/publishCode', 'system', 'manage_member_publishCode');
INSERT INTO `bw_auth_group_node` VALUES ('47307', '62', '1535', '/manage/member.Miniapp/getAuditStatus', 'system', 'manage_member_getAuditStatus');
INSERT INTO `bw_auth_group_node` VALUES ('47308', '62', '1527', '/manage/member.Miniapp/submitAudit', 'system', 'manage_member_submitAudit');
INSERT INTO `bw_auth_group_node` VALUES ('47309', '62', '1364', '/manage/publicCommon/uploadimg', 'system', '_manage_publicCommon_uploadimg');
INSERT INTO `bw_auth_group_node` VALUES ('47310', '62', '1323', '/manage/member.Login/userPassword', 'system', '/manage/member/login/userPassword');
INSERT INTO `bw_auth_group_node` VALUES ('47311', '62', '1316', '/manage/member.Login/userSetting', 'system', '/manage/member/login/userSetting');
INSERT INTO `bw_auth_group_node` VALUES ('47312', '62', '1308', '/manage/publicCommon/upload', 'system', '_manage_publicCommon_upload');
INSERT INTO `bw_auth_group_node` VALUES ('47313', '62', '1273', '/manage/member.Miniapp/getQrCode', 'system', 'manage_member_getQrCode');
INSERT INTO `bw_auth_group_node` VALUES ('47314', '62', '1248', '/manage/member.Miniapp/upCode', 'system', 'manage_member_upCode');
INSERT INTO `bw_auth_group_node` VALUES ('47315', '62', '1243', '/manage/member.Miniapp/setDomain', 'system', 'manage_member_setDomain');
INSERT INTO `bw_auth_group_node` VALUES ('47316', '62', '1206', '/manage/member.WechatAuth/openAuthCallback', 'system', 'manage_member_openAuthCallback');
INSERT INTO `bw_auth_group_node` VALUES ('47317', '62', '1200', '/manage/member.WechatAuth/openAuth', 'system', 'manage_member_openAuth');
INSERT INTO `bw_auth_group_node` VALUES ('47318', '62', '2375', '/manage/member.WechatMessageTemplate/index', 'system', 'manage_member_wechat_message_template_index');
INSERT INTO `bw_auth_group_node` VALUES ('47319', '62', '2380', '/manage/member.WechatMessageTemplate/del', 'system', 'manage_member_wechat_message_template_del');
INSERT INTO `bw_auth_group_node` VALUES ('47320', '62', '2379', '/manage/member.WechatMessageTemplate/modify', 'system', 'manage_member_wechat_message_template_modify');
INSERT INTO `bw_auth_group_node` VALUES ('47321', '62', '2378', '/manage/member.WechatMessageTemplate/edit', 'system', 'manage_member_wechat_message_template_edit');
INSERT INTO `bw_auth_group_node` VALUES ('47322', '62', '2377', '/manage/member.WechatMessageTemplate/add', 'system', 'manage_member_wechat_message_template_add');
INSERT INTO `bw_auth_group_node` VALUES ('47323', '62', '2376', '/manage/member.WechatMessageTemplate/export', 'system', 'manage_member_wechat_message_template_export');

-- ----------------------------
-- Table structure for bw_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `bw_auth_node`;
CREATE TABLE `bw_auth_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父ID',
  `app_name` varchar(40) NOT NULL DEFAULT '' COMMENT '规则所属应用名字或插件名字，如manage,api,home',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如system,plugin,app',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述标题',
  `menu_path` varchar(100) NOT NULL DEFAULT '' COMMENT '路由地址',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'url',
  `auth_name` varchar(100) DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) NOT NULL DEFAULT '' COMMENT '额外url参数',
  `target` varchar(100) NOT NULL DEFAULT '_self' COMMENT '菜单窗口打开方式(_self,_blank,_parent,_top)',
  `condition` varchar(200) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否菜单',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `sort` int(5) DEFAULT '999' COMMENT '排序权重',
  `scopes` varchar(100) DEFAULT 'admin' COMMENT '登录类型',
  PRIMARY KEY (`id`),
  KEY `module` (`app_name`,`status`,`type`),
  KEY `ismenu` (`ismenu`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2381 DEFAULT CHARSET=utf8mb4 COMMENT='权限规则表';

-- ----------------------------
-- Records of bw_auth_node
-- ----------------------------
INSERT INTO `bw_auth_node` VALUES ('1', '185', 'manage', 'system', '权限角色', '/manage/Role/index', '/manage/Role/index', 'manage_Role_indexid_2', 'id=2', '_self', '', '1', '', '哈', '1', '0', '0', '200', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2', '3', 'manage', 'system', '会员管理', '/manage/index/index', '/manage/index/index', 'manage_index_index', '', '_self', '', '0', '', '', '1', '0', '0', '10', 'admin');
INSERT INTO `bw_auth_node` VALUES ('3', '186', 'manage', 'system', '平台首页', '/manage/admin/dashboard', '/manage/admin.Index/dashboard', 'manage_admin_dashboard', '', '_self', '', '1', 'fa fa-bar-chart-o', '', '1', '0', '0', '9999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('182', '185', 'manage', 'system', '管理员管理', '/manage/admin/user', '/manage/Admin/user', 'manage_admin_user', '', '_self', '', '1', '', '', '1', '0', '0', '111', 'admin');
INSERT INTO `bw_auth_node` VALUES ('183', '185', 'manage', 'system', '菜单节点', '/manage/node/index', '/manage/Node/index', 'manage_node_index', '', '_self', '', '1', '', '', '1', '0', '0', '911', 'admin');
INSERT INTO `bw_auth_node` VALUES ('185', '186', 'manage', 'system', '权限用户', '/manage/auth', '/manage/auth', 'manage_auth', '', '_self', '', '1', 'fa fa-lock', '', '1', '0', '0', '2000', 'admin');
INSERT INTO `bw_auth_node` VALUES ('186', '0', 'manage', 'system', '系统管理', '/manage', '/manage', '_manage', '', '_self', '', '1', 'fa fa-bank', '', '1', '0', '0', '1006', 'admin');
INSERT INTO `bw_auth_node` VALUES ('187', '0', 'manage', 'system', '应用管理', '/app', '/app', '_app', '', '_self', '', '1', 'fa fa-connectdevelop', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('188', '0', 'manage', 'system', '插件管理', '/plugin', '/plugin', '_plugin', '', '_self', '', '1', 'fa fa-plug', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('190', '186', 'manage', 'system', '配置管理', '/manage/weihu', '/manage/weihu', 'manage_ceshi', '', '_self', '', '1', 'fa fa-cog', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('193', '190', 'manage', 'system', '配置分类', '/manage/config/index', '/manage/config/index', '_manage_config_index', '', '_self', '', '1', 'fa fa-database', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('196', '190', 'manage', 'system', '组合配置', '/manage/gruop/set', '/manage/Gruop/set', 'manage_gruop_set', '', '_self', '', '1', 'fa fa-cubes', '', '1', '0', '0', '997', 'admin');
INSERT INTO `bw_auth_node` VALUES ('197', '193', 'manage', 'system', '添加', '/manage/Config/add', '/manage/Config/add', 'manageConfigadd', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('198', '193', 'manage', 'system', '修改', '/manage/Config/edit', '/manage/Config/edit', '/manage/Config/edit', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('199', '193', 'manage', 'system', '查询', '/manage/Config/getlist', '/manage/Config/getlist', 'config_set_select', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('200', '193', 'manage', 'system', '删除', '/manage/config/softdleting', '/manage/Config/softdleting', '/manage/Config/softdleting', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('202', '190', 'manage', 'system', '配置设置', '/manage/config/showconfig', '/manage/Config/showconfig', '_manage_config_showconfig', '', '_self', '', '1', 'fa fa-asterisk', '', '1', '0', '0', '998', 'admin');
INSERT INTO `bw_auth_node` VALUES ('206', '190', 'manage', 'system', '配置列表', '/manage/config/configindex', '/manage/config/configindex', '_manage_config_configindex', '', '_self', '', '1', 'fa fa-gears', '', '1', '1588931407', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('210', '196', 'manage', 'system', '组合分类', '/manage/group', '/manage/Group', 'manage_group', '', '_self', '', '1', '', '', '1', '1589338068', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('211', '196', 'manage', 'system', '组合数据权限', '/manage/GroupData/index', '/manage/GroupData/index', '_manage_GroupData_index', '', '_self', '', '0', 'fa fa-delicious', '', '1', '1589338171', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('497', '210', 'manage', 'system', '查看列表', '/manage/Group/getconfiglist', '/manage/Group/getconfiglist', 'manageGroupgetconfiglist', '', '_self', '', '0', '', '', '1', '1589957956', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('503', '210', 'manage', 'system', '添加', '/manage/Group/add', '/manage/Group/add', 'manageGroupadd', '', '_self', '', '0', '', '', '1', '1589961667', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('504', '211', 'manage', 'system', '列表', '/manage/GroupData/getconfiglist', '/manage/GroupData/getconfiglist', 'manageGroupDatagetconfiglist', '', '_self', '', '0', '', '', '1', '1589962104', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('505', '211', 'manage', 'system', '添加', '/manage/GroupData/add', '/manage/GroupData/add', 'manageGroupDataadd', '', '_self', '', '0', '', '', '1', '1589962212', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('506', '206', 'manage', 'system', '列表', '/manage/Config/getconfiglist', '/manage/Config/getconfiglist', 'manageConfiggetconfiglist', '', '_self', '', '0', '', '', '1', '1589962506', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('507', '210', 'manage', 'system', '添加字段', '/manage/Group/addconfig', '/manage/Group/addconfig', 'manageGroupaddconfig', '', '_self', '', '0', '', '', '1', '1589962799', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('508', '202', 'manage', 'system', '保存配置', '/manage/config/setvalues', '/manage/Config/setValues', '/manage/Config/setValues', '', '_self', '', '0', '', '', '1', '1589963167', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('509', '3', 'manage', 'system', '清理缓存', '/manage/index/clear', '/manage/index/clear', 'manageindexclear', '', '_self', '', '0', '', '', '1', '1590028966', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('510', '183', 'manage', 'system', '编辑', '/manage/Node/edit', '/manage/Node/edit', 'manageNodeedit', '', '_self', '', '0', '', '', '1', '1590039400', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('511', '210', 'manage', 'system', '编辑', '/manage/Group/edit', '/manage/Group/edit', 'manageGroupedit', '', '_self', '', '0', '', '', '1', '1590040075', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('512', '210', 'manage', 'system', '编辑字段', '/manage/Group/editconfig', '/manage/Group/editconfig', 'manageGroupeditconfig', '', '_self', '', '0', '', '', '1', '1590040245', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('513', '211', 'manage', 'system', '编辑', '/manage/GroupData/edit', '/manage/GroupData/edit', 'manageGroupDataedit', '', '_self', '', '0', '', '', '1', '1590040376', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('514', '3', 'manage', 'system', '退出登录', '/manage/admin/logout', '/manage/admin.index/logout', '/manage/admin/logout', '', '_self', '', '0', '', '', '1', '1590043364', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('807', '186', 'manage', 'system', '一键生成管理', '/manage/admin/sysCrud', '/manage/admin.SysCrud', 'manage_admin_sysCrud', '', '_self', '', '1', 'fa fa-yelp', '', '0', '1590131579', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('808', '807', 'manage', 'system', '查看', '/manage/admin/sysCrud/index', '/manage/admin.SysCrud/index', '/manage/admin.SysCrud/index', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('809', '807', 'manage', 'system', '新增', '/manage/admin/sysCrud/add', '/manage/admin.SysCrud/add', '/manage/admin.SysCrud/add', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('810', '807', 'manage', 'system', '编辑', '/manage/admin/sysCrud/edit', '/manage/admin.SysCrud/edit', '/manage/admin.SysCrud/edit', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('811', '807', 'manage', 'system', '删除', '/manage/admin/sysCrud/del', '/manage/admin.SysCrud/del', '/manage/admin.SysCrud/del', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('828', '0', 'manage', 'system', '系统管理', '/manage/member.index/index', '/manage/member.index/index', '_manage_member_index_index', '', '_self', '', '1', 'fa fa-bank', '', '1', '1590398679', '0', '5000', 'member');
INSERT INTO `bw_auth_node` VALUES ('829', '828', 'manage', 'system', '租户管理', '/manage/member.Login/user', '/manage/member.Login/user', 'manage_member_Login_user', '', '_self', '', '1', 'fa fa-user-secret', '', '1', '1590398975', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('830', '828', 'manage', 'system', '角色管理', '/manage/member.Role/index', '/manage/member.Role/index', '_manage_member_Role_index', '', '_self', '', '1', 'fa fa-joomla', '', '1', '1590399132', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('834', '1172', 'manage', 'system', '应用设置', '/bwmall/admin/confg', '/bwmall/admin/confg', 'bwmall_admin_confg', '', '_self', '', '0', 'fa fa-gear', '', '1', '1590399842', '0', '5', 'member');
INSERT INTO `bw_auth_node` VALUES ('835', '834', 'manage', 'system', '关于应用', '/manage/member.setting/index', '/manage/member.setting/index', 'manage_member_setting_index', '', '_self', '', '0', 'fa fa-cubes', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('851', '1586', 'manage', 'system', '文章列表', '/manage/admin/sysArticle', '/manage/admin.SysArticle', 'manage_admin_sysArticle', '', '_self', '', '0', 'fa fa-bars', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('852', '851', 'manage', 'system', '查看', '/manage/admin/sysArticle/index', '/manage/admin.SysArticle/index', '/manage/admin.SysArticle/index', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('853', '851', 'manage', 'system', '新增', '/manage/admin/sysArticle/add', '/manage/admin.SysArticle/add', '/manage/admin.SysArticle/add', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('854', '851', 'manage', 'system', '编辑', '/manage/admin/sysArticle/edit', '/manage/admin.SysArticle/edit', '/manage/admin.SysArticle/edit', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('855', '851', 'manage', 'system', '删除', '/manage/admin/sysArticle/del', '/manage/admin.SysArticle/del', '/manage/admin.SysArticle/del', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('856', '1586', 'manage', 'system', '分类管理', '/manage/admin/sysArticleCategory', '/manage/admin.SysArticleCategory', 'manage_admin_sysArticleCategory', '', '_self', '', '1', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('857', '856', 'manage', 'system', '查看', '/manage/admin/sysArticleCategory/index', '/manage/admin.SysArticleCategory/index', '/manage/admin.SysArticleCategory/index', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('858', '856', 'manage', 'system', '新增', '/manage/admin/sysArticleCategory/add', '/manage/admin.SysArticleCategory/add', '/manage/admin.SysArticleCategory/add', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('859', '856', 'manage', 'system', '编辑', '/manage/admin/sysArticleCategory/edit', '/manage/admin.SysArticleCategory/edit', '/manage/admin.SysArticleCategory/edit', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('860', '856', 'manage', 'system', '删除', '/manage/admin/sysArticleCategory/del', '/manage/admin.SysArticleCategory/del', '/manage/admin.SysArticleCategory/del', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('861', '862', 'manage', 'system', '退出登录', '/manage/member/logout', '/manage/member.index/logout', '/manage/member/logout', '', '_self', '', '0', '', '', '1', '1590478428', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('862', '0', 'manage', 'system', '租户后台通用节点', '/member', '/member', 'member', '', '_self', '', '0', '', '', '1', '1590479098', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('863', '830', 'manage', 'system', '分配权限', '/manage/member.Role/getRoleTree', '/manage/member.Role/getRoleTree', '/manage/member.Role/getRoleTree', '', '_self', '', '0', '', '', '1', '1590479262', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('874', '206', 'manage', 'system', '添加配置', '/manage/Config/addconfig', '/manage/Config/addconfig', 'manageConfigaddconfig', '', '_self', '', '0', '', '', '1', '1590566080', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('891', '188', 'manage', 'system', '系统插件', '/manage/admin.Plugin', '/manage/admin.Plugin', '_manage_admin_Plugin', '', '_self', '', '1', 'fa fa-cubes', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('892', '891', 'manage', 'system', '查看', '/manage/admin.Plugin/index', '/manage/admin.Plugin/index', '/manage/admin.Plugin/index', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('893', '891', 'manage', 'system', '新增', '/manage/admin.Plugin/add', '/manage/admin.Plugin/add', '/manage/admin.Plugin/add', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('894', '891', 'manage', 'system', '编辑', '/manage/admin.Plugin/edit', '/manage/admin.Plugin/edit', '/manage/admin.Plugin/edit', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('895', '891', 'manage', 'system', '删除', '/manage/admin.Plugin/del', '/manage/admin.Plugin/del', '/manage/admin.Plugin/del', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('896', '891', 'manage', 'system', '安装', '/manage/admin.Plugin/install', '/manage/admin.Plugin/install', '/manage/admin.Plugin/install', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('897', '891', 'manage', 'system', '卸载', '/manage/admin.Plugin/uninstall', '/manage/admin.Plugin/uninstall', '/manage/admin.Plugin/uninstall', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('898', '891', 'manage', 'system', '启用', '/manage/admin.Plugin/enable', '/manage/admin.Plugin/enable', '/manage/admin.Plugin/enable', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('900', '891', 'manage', 'system', '配置', '/manage/admin.Plugin/config', '/manage/admin.Plugin/config', '/manage/admin.Plugin/config', '', '_self', '', '0', '', '', '1', '1590714186', '0', '1000', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1020', '187', 'manage', 'system', '系统应用', '/manage/admin/miniapp', '/manage/admin.Miniapp', '_manage_admin_miniapp', '', '_self', '', '1', 'fa fa-share-alt-square', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1021', '1020', 'manage', 'system', '查看', '/manage/admin/miniapp/index', '/manage/admin.Miniapp/index', '/manage/admin.Miniapp/index', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1022', '1020', 'manage', 'system', '新增', '/manage/admin/miniapp/add', '/manage/admin.Miniapp/add', '/manage/admin.Miniapp/add', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1023', '1020', 'manage', 'system', '编辑', '/manage/admin/miniapp/edit', '/manage/admin.Miniapp/edit', '/manage/admin.Miniapp/edit', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1024', '1020', 'manage', 'system', '卸载', '/manage/admin/miniapp/uninstall', '/manage/admin.Miniapp/uninstall', '/manage/admin.Miniapp/uninstall', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1025', '1020', 'manage', 'system', '安装', '/manage/admin/miniapp/install', '/manage/admin.Miniapp/install', '/manage/admin.Miniapp/install', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1026', '1020', 'manage', 'system', '购买', '/manage/admin/miniapp/buy', '/manage/admin.Miniapp/buy', '/manage/admin.Miniapp/buy', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1054', '835', 'manage', 'system', '编辑', '/manage/member.setting/edit', '/manage/member.setting/edit', 'manage_member_setting_edit', '', '_self', '', '0', '', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1066', '1', 'manage', 'system', '分配权限', '/manage/role/groupRule', '/manage/Role/groupRule', '/manage/Role/groupRule', '', '_self', '', '0', '', '', '1', '1591146947', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1067', '835', 'manage', 'system', '微信支付', '/manage/member.setting/wechat', '/manage/member.setting/wechat', 'manage_member_setting_wechat', '', '_self', '', '0', '', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1068', '835', 'manage', 'system', '支付宝支付', '/manage/member.setting/alipay', '/manage/member.setting/alipay', 'manage_member_setting_alipay', '', '_self', '', '0', '', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1069', '834', 'manage', 'system', '公众号菜单', '/manage/member.official/index', '/manage/member.official/index', 'manage_member_official_index', '', '_self', '', '0', 'fa fa-wechat', '', '1', '1591234617', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1076', '1069', 'manage', 'system', '新增', '/manage/member.official/add', '/manage/member.official/add', 'manage_member_official_add', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1077', '1069', 'manage', 'system', '编辑', '/manage/member.official/edit', '/manage/member.official/edit', 'manage_member_official_edit', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1078', '1069', 'manage', 'system', '删除', '/manage/member.official/del', '/manage/member.official/del', 'manage_member_official_del', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1079', '1069', 'manage', 'system', '删除', '/manage/member.official/sync', '/manage/member.official/sync', 'manage_member_official_sync', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1081', '1069', 'manage', 'system', '排序', '/manage/member.official/sort', '/manage/member.official/sort', 'manage_member_official_sort', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1093', '187', 'manage', 'system', '租户应用', '/manage/admin/memberMiniapp', '/manage/admin.MemberMiniapp', '_manage_admin_memberMiniapp', '', '_self', '', '1', 'fa fa-codepen', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1094', '1093', 'manage', 'system', '查看', '/manage/admin/memberMiniapp/index', '/manage/admin.MemberMiniapp/index', '/manage/admin.MemberMiniapp/index', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1095', '1093', 'manage', 'system', '新增', '/manage/admin/memberMiniapp/add', '/manage/admin.MemberMiniapp/add', '/manage/admin.MemberMiniapp/add', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1096', '1093', 'manage', 'system', '编辑', '/manage/admin/memberMiniapp/edit', '/manage/admin.MemberMiniapp/edit', '/manage/admin.MemberMiniapp/edit', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1097', '1093', 'manage', 'system', '删除', '/manage/admin/memberMiniapp/del', '/manage/admin.MemberMiniapp/del', '/manage/admin.MemberMiniapp/del', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1098', '206', 'manage', 'system', '编辑', '/manage/Config/editconfig', '/manage/Config/editconfig', '/manage/Config/editconfig', '', '_self', '', '0', '', '', '1', '1591670413', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1105', '1172', 'manage', 'system', '应用商店', '/manage/member/miniapp/index', '/manage/member.miniapp/index', 'manage_member_miniapp_index', '', '_self', '', '1', 'fa fa-gift', '', '1', '1591779441', '0', '1800', 'member');
INSERT INTO `bw_auth_node` VALUES ('1107', '1105', 'manage', 'system', '详情', '/manage/member/miniapp/detail', '/manage/member.miniapp/detail', '/manage/member.miniapp/detail', '', '_self', '', '0', '', '', '1', '1591779441', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1108', '1105', 'manage', 'system', '购买', '/manage/member/miniapp/buy', '/manage/member.miniapp/buy', '/manage/member.miniapp/buy', '', '_self', '', '0', '', '', '1', '1591779441', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1114', '186', 'manage', 'system', 'crud测试管理', '/manage/crudDemo', '/manage/crudDemo', 'manage_crudDemo', '', '_self', '', '1', 'fa fa-tachometer', '', '1', '1591844500', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1115', '1114', 'manage', 'system', '查看', '/manage/crudDemo/index', '/manage/crudDemo/index', '/manage/crudDemo/index', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1116', '1114', 'manage', 'system', '新增', '/manage/crudDemo/add', '/manage/crudDemo/add', '/manage/crudDemo/add', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1117', '1114', 'manage', 'system', '编辑', '/manage/crudDemo/edit', '/manage/crudDemo/edit', '/manage/crudDemo/edit', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1118', '1114', 'manage', 'system', '删除', '/manage/crudDemo/del', '/manage/crudDemo/del', 'manage_crudDemo_del', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1124', '1545', 'manage', 'system', '租户用户', '/manage/admin/member', '/manage/admin.member', 'manage_admin_member', '', '_self', '', '1', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1125', '1124', 'manage', 'system', '查看', '/manage/admin/member/index', '/manage/admin.member/index', '/manage/admin.member/index', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1126', '1124', 'manage', 'system', '新增', '/manage/admin/Member/add', '/manage/admin.Member/add', '/manage/admin.Member/add', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1127', '1124', 'manage', 'system', '编辑', '/manage/admin/Member/edit', '/manage/admin.Member/edit', '/manage/admin.Member/edit', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1128', '1124', 'manage', 'system', '删除', '/manage/admin.Member/del', '/manage/admin.member/del', '/manage/admin.member/del', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1161', '830', 'manage', 'system', '新增角色', '/manage/member.Role/add', '/manage/member.Role/add', '/manage/member.Role/add', '', '_self', '', '0', '', '', '1', '1592208844', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1163', '3', 'manage', 'system', '首页', '/manage/admin.Index/index', '/manage/admin.Index/index', '/manage/admin.Index/index', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1164', '3', 'manage', 'system', '菜单', '/manage/admin.Index/menu', '/manage/admin.Index/menu', '/manage/admin.Index/menu', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1165', '3', 'manage', 'system', '后台首页', '/manage/admin/dashboard', '/manage/admin/dashboard', '/manage/admin/dashboard', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1166', '210', 'manage', 'system', '查看', '/manage/group/index', '/manage/Group/index', '/manage/Group/index', '', '_self', '', '0', 'fa fa-life-saver', '', '1', '1589338068', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1167', '183', 'manage', 'system', '删除', '/manage/Node/delete', '/manage/Node/delete', '/manage/Node/delete', '', '_self', '', '0', '', '', '1', '1590039400', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1168', '1', 'manage', 'system', '编辑', '/manage/role/edit', '/manage/Role/edit', '/manage/role/edit', '', '_self', '', '0', '', '', '1', '1591146947', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1169', '862', 'manage', 'system', '菜单', '/manage/member/menu', '/manage/member.Index/menu', '/manage/member/menu', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1170', '3', 'manage', 'system', '清理缓存', '/manage/common/clearCache', '/manage/common/clearCache', '/manage/common/clearCache', '', '_self', '', '0', '', '', '1', '1590043364', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1171', '862', 'manage', 'system', '控制台', '/manage/member/dashboard', '/manage/member.Index/dashboard', '/manage/member/dashboard', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1172', '0', 'manage', 'system', '应用管理', '/app/member', '/app/member', '_app_member', '', '_self', '', '1', '', '', '1', '0', '0', '4000', 'member');
INSERT INTO `bw_auth_node` VALUES ('1182', '182', 'manage', 'system', '添加', '/manage/admin/add', '/manage/Admin/add', '/manage/Admin/add', '', '_self', '', '0', '', '', '1', '1594361599', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1183', '182', 'manage', 'system', '设置状态', '/manage/admin/setStatus', '/manage/Admin/setStatus', '/manage/Admin/setStatus', '', '_self', '', '0', '', '', '1', '1594362339', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1184', '182', 'manage', 'system', '编辑', '/manage/admin/edit', '/manage/Admin/edit', '/manage/Admin/edit', '', '_self', '', '0', '', '', '1', '1594362616', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1185', '182', 'manage', 'system', '删除', '/manage/admin/delete', '/manage/Admin/delete', '/manage/Admin/delete', '', '_self', '', '0', '', '', '1', '1594362797', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1200', '862', 'manage', 'system', '开放平台授权', '/manage/member/openAuth', '/manage/member.WechatAuth/openAuth', 'manage_member_openAuth', '', '_self', '', '0', '', '', '1', '1594371589', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1206', '862', 'manage', 'system', '开放平台授权回调', '/manage/member/openAuthCallback', '/manage/member.WechatAuth/openAuthCallback', 'manage_member_openAuthCallback', '', '_blank', '', '0', '', '', '1', '1594397684', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1221', '830', 'manage', 'system', '租户权限列表', '/manage/member.Role/getNodeTree', '/manage/member.Role/getNodeTree', '/manage/member.Role/getNodeTree', '', '_self', '', '0', '', '', '1', '1594629488', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1236', '830', 'manage', 'system', '删除', '/manage/member.Role/delete', '/manage/member.Role/delete', '/manage/member.Role/delete', '', '_self', '', '0', '', '', '1', '1594631484', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1237', '830', 'manage', 'system', '状态', '/manage/member.Role/setStatus', '/manage/member.Role/setStatus', '/manage/member.Role/setStatus', '', '_self', '', '0', '', '', '1', '1594631863', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1238', '830', 'manage', 'system', '编辑', '/manage/member.Role/edit', '/manage/member.Role/edit', '/manage/member.Role/edit', '', '_self', '', '0', '', '', '1', '1594632056', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1239', '829', 'manage', 'system', '添加', '/manage/member.Login/add', '/manage/member.Login/add', '/manage/member.Login/add', '', '_self', '', '0', '', '', '1', '1594635443', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1240', '829', 'manage', 'system', '编辑', '/manage/member.Login/edit', '/manage/member.Login/edit', '/manage/member.Login/edit', '', '_self', '', '0', '', '', '1', '1594637416', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1242', '829', 'manage', 'system', '设置状态', '/manage/member.Login/setStatus', '/manage/member.Login/setStatus', '/manage/member.Login/setStatus', '', '_self', '', '0', '', '', '1', '1594639953', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1243', '862', 'manage', 'system', '小程序修改授权域名', '/manage/member/setDomain', '/manage/member.Miniapp/setDomain', 'manage_member_setDomain', '', '_self', '', '0', '', '', '1', '1594653668', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1244', '1', 'manage', 'system', '[应用/功能/模块]列表', '/manage/Role/modelRole', '/manage/Role/modelRole', '/manage/Role/modelRole', '', '_self', '', '0', '', '', '1', '1594692608', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1245', '1', 'manage', 'system', '添加[应用/功能/模块]组', '/manage/Role/addModel', '/manage/Role/addModel', '/manage/Role/addModel', '', '_self', '', '0', '', '', '1', '1594693362', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1246', '1', 'manage', 'system', '编辑[应用/功能/模块]组', '/manage/Role/editModel', '/manage/Role/editModel', '/manage/Role/editModel', '', '_self', '', '0', '', '', '1', '1594693413', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1247', '1', 'manage', 'system', '应用功能组树', '/manage/Role/getRoleModelTree', '/manage/Role/getRoleModelTree', '/manage/Role/getRoleModelTree', '', '_self', '', '0', '', '', '1', '1594695179', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1248', '862', 'manage', 'system', '提交代码', '/manage/member/upCode', '/manage/member.Miniapp/upCode', 'manage_member_upCode', '', '_self', '', '0', '', '', '1', '1594733819', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1249', '1', 'manage', 'system', '租户角色树', '/manage/Role/getRoleMemberTree', '/manage/Role/getRoleMemberTree', '/manage/Role/getRoleMemberTree', '', '_self', '', '0', '', '', '1', '1594783298', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1250', '834', 'manage', 'system', '回复规则管理', '/manage/member.wechatKeyword/index', '/manage/member.wechatKeyword/index', 'manage_member_wechatKeyword_index', '', '_self', '', '0', 'fa fa-paint-brush', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1252', '1250', 'manage', 'system', '新增', '/manage/member.wechatKeyword/add', '/manage/member.wechatKeyword/add', 'manage_member_wechatKeyword_add', '', '_self', '', '0', '', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1253', '1250', 'manage', 'system', '编辑', '/manage/member.wechatKeyword/edit', '/manage/member.wechatKeyword/edit', 'manage_member_wechatKeyword_edit', '', '_self', '', '0', '', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1254', '1250', 'manage', 'system', '删除', '/manage/member.wechatKeyword/del', '/manage/member.wechatKeyword/del', 'manage_member_wechatKeyword_del', '', '_self', '', '0', '', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1255', '1124', 'manage', 'system', '状态', '/manage/admin/member/setStatus', '/manage/admin.member/setStatus', '/manage/admin/member/setStatus', '', '_self', '', '0', '', '', '1', '1594793850', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1256', '1', 'manage', 'system', '租户角色', '/manage/Role/memberRole', '/manage/Role/memberRole', '/manage/Role/memberRole', '', '_self', '', '0', '', '', '1', '1594800045', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1257', '1', 'manage', 'system', '租户添加', '/manage/Role/addMember', '/manage/Role/addMember', '/manage/Role/addMember', '', '_self', '', '0', '', '', '1', '1594801463', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1258', '1', 'manage', 'system', '租户编辑', '/manage/Role/editMember', '/manage/Role/editMember', '/manage/Role/editMember', '', '_self', '', '0', '', '', '1', '1594801517', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1259', '1', 'manage', 'system', '顶级租户节点树', '/manage/Role/getNodeTree', '/manage/Role/getNodeTree', '/manage/Role/getNodeTree', '', '_self', '', '0', '', '', '1', '1594804763', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1270', '183', 'manage', 'system', '租户节点', '/manage/node/memberIndex', '/manage/Node/memberIndex', '/manage/Node/add', '', '_self', '', '0', '', '', '1', '1594877533', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1271', '183', 'manage', 'system', '添加租户节点', '/manage/node/addMember', '/manage/Node/addMember', '/manage/Node/addMember', '', '_self', '', '0', '', '', '1', '1594878879', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1272', '183', 'manage', 'system', '编辑租户节点', '/manage/node/editMember', '/manage/Node/editMember', '/manage/Node/editMember', '', '_self', '', '0', '', '', '1', '1594878964', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1273', '862', 'manage', 'system', '获取小程序体验二维码', '/manage/member/getQrCode', '/manage/member.Miniapp/getQrCode', 'manage_member_getQrCode', '', '_self', '', '0', '', '', '1', '1594879921', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1274', '183', 'manage', 'system', '租户节点树', '/manage/node/getMemberNodeTree', '/manage/Node/getMemberNodeTree', '/manage/Node/getMemberNodeTree', '', '_self', '', '0', '', '', '1', '1594880801', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1275', '1306', 'manage', 'system', '云市场服务管理', '/manage/admin/cloud/service', '/manage/admin.cloud.service', '_manage_admin_cloud_service', '', '_self', '', '1', 'fa fa-cloud-upload', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1276', '1275', 'manage', 'system', '查看', '/manage/admin/cloud/service/index', '/manage/admin.cloud.service/index', '/manage/admin.cloud.service/index', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1277', '1275', 'manage', 'system', '新增', '/manage/admin/cloud/service/add', '/manage/admin.cloud.service/add', '/manage/admin.cloud.service/add', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1278', '1275', 'manage', 'system', '编辑', '/manage/admin/cloud/service/edit', '/manage/admin.cloud.service/edit', '/manage/admin.cloud.service/edit', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1279', '1275', 'manage', 'system', '删除', '/manage/admin/cloud/service/del', '/manage/admin.cloud.service/del', '/manage/admin.cloud.service/del', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1280', '1306', 'manage', 'system', '云市场套餐管理', '/manage/admin/cloud/packet', '/manage/admin.cloud.packet', '/manage/admin.cloud.packet', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1281', '1280', 'manage', 'system', '查看', '/manage/admin/cloud/packet/index', '/manage/admin.cloud.packet/index', '/manage/admin.cloud.packet/index', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1282', '1280', 'manage', 'system', '新增', '/manage/admin/cloud/packet/add', '/manage/admin.cloud.packet/add', '/manage/admin.cloud.packet/add', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1283', '1280', 'manage', 'system', '编辑', '/manage/admin/cloud/packet/edit', '/manage/admin.cloud.packet/edit', '/manage/admin.cloud.packet/edit', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1284', '1280', 'manage', 'system', '删除', '/manage/admin/cloud/packet/del', '/manage/admin.cloud.packet/del', '/manage/admin.cloud.packet/del', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1285', '1306', 'manage', 'system', '云市场租户余额', '/manage/admin/cloud/wallet', '/manage/admin.cloud.wallet', '/manage/admin.cloud.wallet', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1286', '1285', 'manage', 'system', '查看', '/manage/admin/cloud/wallet/index', '/manage/admin.cloud.wallet/index', '/manage/admin.cloud.wallet/index', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1287', '1285', 'manage', 'system', '新增', '/manage/admin/cloud/wallet/add', '/manage/admin.cloud.wallet/add', '/manage/admin.cloud.wallet/add', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1288', '1285', 'manage', 'system', '编辑', '/manage/admin/cloud/wallet/edit', '/manage/admin.cloud.wallet/edit', '/manage/admin.cloud.wallet/edit', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1289', '1285', 'manage', 'system', '删除', '/manage/admin/cloud/wallet/del', '/manage/admin.cloud.wallet/del', '/manage/admin.cloud.wallet/del', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1290', '1306', 'manage', 'system', '云市场钱包记录管理', '/manage/admin/cloud/walletBill', '/manage/admin.cloud.walletBill', '/manage/admin.cloud.walletBill', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1291', '1290', 'manage', 'system', '查看', '/manage/admin/cloud/walletBill/index', '/manage/admin.cloud.walletBill/index', '/manage/admin.cloud.walletBill/index', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1292', '1290', 'manage', 'system', '新增', '/manage/admin/cloud/walletBill/add', '/manage/admin.cloud.walletBill/add', '/manage/admin.cloud.walletBill/add', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1293', '1290', 'manage', 'system', '编辑', '/manage/admin/cloud/walletBill/edit', '/manage/admin.cloud.walletBill/edit', '/manage/admin.cloud.walletBill/edit', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1294', '1290', 'manage', 'system', '删除', '/manage/admin/cloud/walletBill/del', '/manage/admin.cloud.walletBill/del', '/manage/admin.cloud.walletBill/del', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1295', '1306', 'manage', 'system', '云市场订单管理', '/manage/admin/cloud/order', '/manage/admin.cloud.order', 'manage_admin_cloud_order', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1296', '1295', 'manage', 'system', '查看', '/manage/admin/cloud/order/index', '/manage/admin.cloud.order/index', '/manage/admin.cloud.order/index', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1297', '1295', 'manage', 'system', '新增', '/manage/admin/cloud/order/add', '/manage/admin.cloud.order/add', '/manage/admin.cloud.order/add', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1298', '1295', 'manage', 'system', '编辑', '/manage/admin/cloud/order/edit', '/manage/admin.cloud.order/edit', '/manage/admin.cloud.order/edit', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1299', '1295', 'manage', 'system', '删除', '/manage/admin/cloud/order/del', '/manage/admin.cloud.order/del', '/manage/admin.cloud.order/del', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1306', '188', 'manage', 'system', '云市场', 'cloud', 'cloud', 'cloud', '', '_self', '', '1', 'fa fa-skyatlas', '', '1', '1594970291', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1307', '186', 'manage', 'system', '上传文件', '/manage/publicCommon/upload', '/manage/publicCommon/upload', 'public_common_upload', '', '_self', '', '0', '', '', '1', '1594974304', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1308', '862', 'manage', 'system', '上传文件', '/manage/publicCommon/upload', '/manage/publicCommon/upload', '_manage_publicCommon_upload', '', '_self', '', '0', '', '', '1', '1594974394', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1309', '1315', 'manage', 'system', '云市场服务管理', '/manage/member/cloud/service', '/manage/member.cloud.service', '_manage_member_cloud_service', '', '_self', '', '1', 'fa fa-cloud-upload', '', '1', '1595034953', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1310', '1309', 'manage', 'system', '查看', '/manage/member/cloud/service/index', '/manage/member.cloud.service/index', '/manage/member.cloud.service/index', '', '_self', '', '0', '', '', '1', '1595034953', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1314', '0', 'manage', 'system', '插件管理', 'member/addons', 'member/addons', 'member/addons', '', '_self', '', '1', '', '', '1', '1595035502', '0', '3000', 'member');
INSERT INTO `bw_auth_node` VALUES ('1315', '1314', 'manage', 'system', '云市场', 'member/cloud', 'member/cloud', 'member_cloud', '', '_self', '', '1', 'fa fa-skyatlas', '', '1', '1595035578', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1316', '862', 'manage', 'system', '基本资料', '/manage/member/login/userSetting', '/manage/member.Login/userSetting', '/manage/member/login/userSetting', '', '_self', '', '0', '', '', '1', '1595038359', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1317', '1315', 'manage', 'system', '云市场套餐管理', '/manage/member/cloud/packet', '/manage/member.cloud.packet', '/manage/member.cloud.packet', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1318', '1317', 'manage', 'system', '查看', '/manage/member/cloud/packet/index', '/manage/member.cloud.packet/index', '/manage/member.cloud.packet/index', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1322', '1317', 'manage', 'system', '购买', '/manage/member/cloud/packet/buy', '/manage/member.cloud.packet/buy', '/manage/member.cloud.packet/buy', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1323', '862', 'manage', 'system', '修改密码', '/manage/member/login/userPassword', '/manage/member.Login/userPassword', '/manage/member/login/userPassword', '', '_self', '', '0', '', '', '1', '1595040696', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1324', '1315', 'manage', 'system', '云市场钱包记录管理', '/manage/member/cloud/walletBill', '/manage/member.cloud.walletBill', '/manage/member.cloud.walletBill', '', '_self', '', '0', '', '', '1', '1595042219', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1325', '1324', 'manage', 'system', '查看', '/manage/member/cloud/walletBill/index', '/manage/member.cloud.walletBill/index', '/manage/member.cloud.walletBill/index', '', '_self', '', '0', '', '', '1', '1595042219', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1329', '3', 'manage', 'system', '基本资料', '/manage/admin/userSetting', '/manage/admin.Index/userSetting', '/manage/admin/userSetting', '', '_self', '', '0', '', '', '1', '1595049242', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1330', '3', 'manage', 'system', '修改密码', '/manage/admin/userPassword', '/manage/admin.Index/userPassword', '/manage/admin/userPassword', '', '_self', '', '0', '', '', '1', '1595049296', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1331', '1545', 'manage', 'system', '租户钱包', '/manage/admin/member/wallet', '/manage/admin.member.wallet', 'manage_admin_member_wallet', '', '_self', '', '1', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1332', '1331', 'manage', 'system', '查看', '/manage/admin/member/wallet/index', '/manage/admin.member.wallet/index', '/manage/admin.member.wallet/index', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1333', '1331', 'manage', 'system', '新增', '/manage/admin/member/wallet/add', '/manage/admin.member.wallet/add', '/manage/admin.member.wallet/add', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1334', '1331', 'manage', 'system', '编辑', '/manage/admin/member/wallet/edit', '/manage/admin.member.wallet/edit', '/manage/admin.member.wallet/edit', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1335', '1331', 'manage', 'system', '删除', '/manage/admin/member/wallet/del', '/manage/admin.member.wallet/del', '/manage/admin.member.wallet/del', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1341', '1545', 'manage', 'system', '租户财务', '/manage/admin/member/walletBill', '/manage/admin.member.walletBill', 'manage_admin_member_walletBill', '', '_self', '', '1', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1342', '1341', 'manage', 'system', '查看', '/manage/admin/member/walletBill/index', '/manage/admin.member.walletBill/index', '/manage/admin.member.walletBill/index', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1343', '1341', 'manage', 'system', '新增', '/manage/admin/member/walletBill/add', '/manage/admin.member.walletBill/add', '/manage/admin.member.walletBill/add', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1344', '1341', 'manage', 'system', '编辑', '/manage/admin/member/walletBill/edit', '/manage/admin.member.walletBill/edit', '/manage/admin.member.walletBill/edit', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1345', '1341', 'manage', 'system', '删除', '/manage/admin/member/walletBill/del', '/manage/admin.member.walletBill/del', '/manage/admin.member.walletBill/del', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1346', '828', 'manage', 'system', '配置管理', '/config', '/config', '_config', '', '_self', '', '0', 'fa fa-gear', '', '1', '1595054309', '0', '888', 'member');
INSERT INTO `bw_auth_node` VALUES ('1347', '1346', 'manage', 'system', '配置管理', '/manage/member/config/showMemberConfig/1', '/manage/member.Config/showMemberConfig', '_manage_member_config_showMemberConfig_1', '', '_self', '', '0', 'fa fa-eyedropper', '', '1', '1595054503', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1348', '1347', 'manage', 'system', '保存配置', '/manage/member/config/setMemberValues', '/manage/member.Config/setMemberValues', '_manage_member_config_setMemberValues', '', '_self', '', '0', '', '', '1', '1595054662', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1349', '828', 'manage', 'system', '财务管理', '/manage/member/walletBill', '/manage/member.walletBill', 'manage_member_walletBill', '', '_self', '', '1', 'fa fa-edit', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1350', '1349', 'manage', 'system', '查看', '/manage/member/walletBill/index', '/manage/member.walletBill/index', '/manage/member.walletBill/index', '', '_self', '', '0', '', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1351', '1349', 'manage', 'system', '新增', '/manage/member/walletBill/add', '/manage/member.walletBill/add', '/manage/member.walletBill/add', '', '_self', '', '0', '', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1352', '1349', 'manage', 'system', '编辑', '/manage/member/walletBill/edit', '/manage/member.walletBill/edit', '/manage/member.walletBill/edit', '', '_self', '', '0', '', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1354', '1315', 'manage', 'system', '云市场订单管理', '/manage/member/cloud/order', '/manage/member.cloud.order', '/manage/member.cloud.order', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1355', '1354', 'manage', 'system', '查看', '/manage/member/cloud/order/index', '/manage/member.cloud.order/index', '/manage/member.cloud.order/index', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1358', '1396', 'manage', 'system', '编辑', '/manage/member/groupData/edit', '/manage/member.GroupData/edit', '_manage_member_groupData_edit', '', '_self', '', '0', '', '', '1', '1595296632', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1359', '1396', 'manage', 'system', '添加', '/manage/member/groupData/add', '/manage/member.GroupData/add', '_manage_member_groupData_add', '', '_self', '', '0', '', '', '1', '1595296688', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1360', '1396', 'manage', 'system', '删除', '/manage/member/groupData/softdleting', '/manage/member.GroupData/softdleting', '_manage_member_groupData_softdleting', '', '_self', '', '0', '', '', '1', '1595296730', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1361', '1396', 'manage', 'system', '列表', '/manage/member/groupData/getConfigList', '/manage/member.GroupData/getConfigList', '_manage_member_groupData_getConfigList', '', '_self', '', '0', '', '', '1', '1595296788', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1362', '211', 'manage', 'system', '状态变更', '/manage/groupData/setConfigShow', '/manage/GroupData/setConfigShow', '/manage/GroupData/setConfigShow', '', '_self', '', '0', '', '', '1', '1595297231', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1363', '1396', 'manage', 'system', '更改状态', '/manage/member/groupData/setConfigShow', '/manage/member.GroupData/setConfigShow', '_manage_member_groupData_setConfigShow', '', '_self', '', '0', '', '', '1', '1595297407', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1364', '862', 'manage', 'system', '上传图片(layui返回格式)', '/manage/publicCommon/uploadimg', '/manage/publicCommon/uploadimg', '_manage_publicCommon_uploadimg', '', '_self', '', '0', '', '', '1', '1595298389', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1365', '186', 'manage', 'system', '上传图片(layui返回格式)', '/manage/publicCommon/uploadimg', '/manage/publicCommon/uploadimg', '/manage/publicCommon/uploadimg', '', '_self', '', '0', '', '', '1', '1595298467', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1391', '186', 'manage', 'system', '附件管理', '/manage/uploadfile/index', '/manage/admin.Uploadfile/index', 'manage_uploadfile_index', '', '_self', '', '1', 'fa fa-cloud-upload', '', '1', '1598422130', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1392', '1391', 'manage', 'system', '文件添加', '/manage/uploadfile/add', '/manage/admin.Uploadfile/add', '_manage_uploadfile_add', '', '_self', '', '0', '', '', '1', '1598422471', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1393', '1391', 'manage', 'system', '文件删除', '/manage/uploadfile/del', '/manage/admin.Uploadfile/del', '_manage_uploadfile_del', '', '_self', '', '0', '', '', '1', '1598422620', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1394', '1391', 'manage', 'system', '文件导出', '/manage/uploadfile/export', '/manage/admin.Uploadfile/export', '_manage_uploadfile_export', '', '_self', '', '0', '', '', '1', '1598422670', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1396', '1346', 'manage', 'system', '组合数据管理', '/manage/member/groupData', '/manage/member.GroupData/index', '_manage_member_groupData', '', '_self', '', '0', '', '', '1', '1598424995', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1408', '828', 'manage', 'system', '充值', '/manage/member/recharge', '/manage/member.Recharge/index', '_manage_member_recharge', '', '_self', '', '0', '', '', '1', '1599015300', '0', '666', 'member');
INSERT INTO `bw_auth_node` VALUES ('1411', '1408', 'manage', 'system', '提交', '/manage/member/recharge/pay', '/manage/member.Recharge/pay', '_manage_member_recharge_pay', '', '_self', '', '0', '', '', '1', '1599027718', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1412', '1471', 'manage', 'system', '用户列表', '/manage/member/user/index', '/manage/member.User/index', 'manage_member_user_index', '', '_self', '', '1', 'fa fa-user-plus', '', '1', '1599114629', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1413', '1412', 'manage', 'system', '新增', '/manage/member/user/add', '/manage/member.User/add', '_manage_member_user_add', '', '_self', '', '0', '', '', '1', '1599114909', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1414', '1412', 'manage', 'system', '修改', '/manage/member/user/edit', '/manage/member.User/edit', '_manage_member_user_edit', '', '_self', '', '0', '', '', '1', '1599114949', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1416', '1412', 'manage', 'system', '导出', '/manage/member/user/export', '/manage/member.User/export', '_manage_member_user_export', '', '_self', '', '0', '', '', '1', '1599115319', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1417', '1412', 'manage', 'system', '修改', '/manage/member/user/modify', '/manage/member.User/modify', '_manage_member_user_modify', '', '_self', '', '0', '', '', '1', '1599115360', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1418', '1412', 'manage', 'system', '修改密码', '/manage/member/user/editPw', '/manage/member.User/editPw', '_manage_member_user_editPw', '', '_self', '', '0', '', '', '1', '1599120886', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1419', '1412', 'manage', 'system', '资金余额', '/manage/member/user/changeMoney', '/manage/member.User/changeMoney', '_manage_member_user_changeMoney', '', '_self', '', '0', '', '', '1', '1599130844', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1443', '828', 'manage', 'system', '首页', '/manage/member/dashboard', '/manage/member.Index/dashboard', '_manage_member_dashboard', '', '_self', '', '1', 'fa fa-bar-chart-o', '', '1', '1599546960', '0', '9999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1458', '1069', 'manage', 'system', '素材列表', '/manage/member.official/materialList', '/manage/member.official/materialList', 'manage_member_official_materialList', '', '_self', '', '0', '', '', '1', '1600239056', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1462', '1471', 'manage', 'system', '用户充值', 'rechange/config', 'rechange/config', 'rechange_config', '', '_self', '', '1', 'fa fa-cny', '', '1', '1600509225', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1463', '1462', 'manage', 'system', '充值设置', '/manage/member/config/showMemberConfig/0/54', '/manage/member/config.ShowMemberConfig', 'manage_member_config_showMemberConfig_0_54', '', '_self', '', '1', '', '', '1', '1600509355', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1464', '1462', 'manage', 'system', '充值套餐', '/manage/member/groupData/recharge_select', '/manage/member.GroupData/index', 'manage_member_groupData_recharge_select', '', '_self', '', '1', '', '', '1', '1600509525', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1465', '1462', 'manage', 'system', '充值记录', '/manage/member/user/recharge/index', '/manage/member.user.Recharge/index', 'manage_member_user_recharge_index', '', '_self', '', '1', '', '', '1', '1600669249', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1466', '1465', 'manage', 'system', '添加', '/manage/member/user/recharge/add', '/manage/member.user.Recharge/add', '_manage_member_user_recharge_add', '', '_self', '', '0', '', '', '1', '1600669582', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1467', '1465', 'manage', 'system', '编辑', '/manage/member/user/recharge/edit', '/manage/member.user.Recharge/edit', '_manage_member_user_recharge_edit', '', '_self', '', '0', '', '', '1', '1600669661', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1468', '1465', 'manage', 'system', '导出', '/manage/member/user/recharge/export', '/manage/member.user.Recharge/export', '_manage_member_user_recharge_export', '', '_self', '', '0', '', '', '1', '1600669752', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1469', '1465', 'manage', 'system', '修改', '/manage/member/user/recharge/modify', '/manage/member.user.Recharge/modify', '_manage_member_user_recharge_modify', '', '_self', '', '0', '', '', '1', '1600669842', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1471', '1172', 'manage', 'system', '全部用户', '/app/all/user', '/app/all/user', 'app_all_user', '', '_self', '', '1', 'fa fa-male', '', '1', '1600680925', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1472', '1471', 'manage', 'system', '财务记录', '/manage/member/user/bill/index', '/manage/member.user.Bill/index', 'manage_member_user_bill_index', '', '_self', '', '1', 'fa fa-pencil', '', '1', '1600681941', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1473', '1472', 'manage', 'system', '导出', '/manage/member/user/bill/export', '/manage/member.user.Bill/export', '_manage_member_user_bill_export', '', '_self', '', '0', '', '', '1', '1600682018', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1474', '1472', 'manage', 'system', '添加', '/manage/member/user/bill/add', '/manage/member.user.Bill/add', '_manage_member_user_bill_add', '', '_self', '', '0', '', '', '1', '1600682140', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1475', '1472', 'manage', 'system', '编辑', '/manage/member/user/bill/edit', '/manage/member.user.Bill/edit', '_manage_member_user_bill_edit', '', '_self', '', '0', '', '', '1', '1600682206', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1476', '1472', 'manage', 'system', '修改', '/manage/member/user/bill/modify', '/manage/member.user.Bill/modify', '_manage_member_user_bill_modify', '', '_self', '', '0', '', '', '1', '1600682264', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1485', '1471', 'manage', 'system', '用户提现', '/manage/member/extract', '/manage/member/extract', '_manage_member_extract', '', '_self', '', '1', 'fa fa-money', '', '1', '1600767960', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1486', '1485', 'manage', 'system', '提现配置', '/manage/member/config/showMemberConfig/0/55', '/manage/member/config.ShowMemberConfig', 'manage_member_config_showMemberConfig_0_55', '', '_self', '', '1', '', '', '1', '1600768150', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1487', '1485', 'manage', 'system', '绑定管理', '/manage/member/user/extract/index', '/manage/member.user.Extract/index', 'manage_member_user_extract_index', '', '_self', '', '1', '', '', '1', '1600844996', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1488', '1487', 'manage', 'system', '导出', '/manage/member/user/extract/export', '/manage/member.user.Extract/export', '_manage_member_user_extract_export', '', '_self', '', '0', '', '', '1', '1600845091', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1489', '1487', 'manage', 'system', '添加', '/manage/member/user/extract/add', '/manage/member.user.Extract/add', '_manage_member_user_extract_add', '', '_self', '', '0', '', '', '1', '1600845293', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1490', '1487', 'manage', 'system', '编辑', '/manage/member/user/extract/edit', '/manage/member.user.Extract/edit', '_manage_member_user_extract_edit', '', '_self', '', '0', '', '', '1', '1600845388', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1491', '1487', 'manage', 'system', '修改', '/manage/member/user/extract/modify', '/manage/member.user.Extract/modify', '_manage_member_user_extract_modify', '', '_self', '', '0', '', '', '1', '1600845433', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1492', '1487', 'manage', 'system', '删除', '/manage/member/user/extract/del', '/manage/member.user.Extract/del', '_manage_member_user_extract_del', '', '_self', '', '0', '', '', '1', '1600845502', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1493', '1485', 'manage', 'system', '提现管理', '/manage/member/user/extractLog/index', '/manage/member.user.ExtractLog/index', 'manage_member_user_extractLog_index', '', '_self', '', '1', '', '', '1', '1600845647', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1494', '1493', 'manage', 'system', '导出', '/manage/member/user/extractLog/export', '/manage/member.user.ExtractLog/export', '_manage_member_user_extractLog_export', '', '_self', '', '0', '', '', '1', '1600845718', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1495', '1493', 'manage', 'system', '添加', '/manage/member/user/extractLog/add', '/manage/member.user.ExtractLog/add', '_manage_member_user_extractLog_add', '', '_self', '', '0', '', '', '1', '1600845981', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1496', '1493', 'manage', 'system', '编辑', '/manage/member/user/extractLog/edit', '/manage/member.user.ExtractLog/edit', '_manage_member_user_extractLog_edit', '', '_self', '', '0', '', '', '1', '1600846099', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1497', '1493', 'manage', 'system', '修改', '/manage/member/user/extractLog/modify', '/manage/member.user.ExtractLog/modify', '_manage_member_user_extractLog_modify', '', '_self', '', '0', '', '', '1', '1600846183', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1498', '1493', 'manage', 'system', '删除', '/manage/member/user/extractLog/del', '/manage/member.user.ExtractLog/del', '_manage_member_user_extractLog_del', '', '_self', '', '0', '', '', '1', '1600846240', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1500', '834', 'manage', 'system', '微信图文管理', '/manage/member.wechatKeyword/materialList', '/manage/member.wechatKeyword/materialList', 'manage_member_wechatKeyword_materialList', '', '_self', '', '0', 'fa fa-weixin', '', '1', '1600929033', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1501', '185', 'manage', 'system', '操作日志', '/manage/admin/sys/log/index', '/manage/admin.sys.Log/index', 'manage_admin_sys_log_index', '', '_self', '', '1', '', '', '1', '1600936300', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1502', '1501', 'manage', 'system', '导出', '/manage/admin/sys/log/export', '/manage/admin.sys.Log/export', '_manage_admin_sys_log_export', '', '_self', '', '0', '', '', '1', '1600936381', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1503', '1501', 'manage', 'system', '添加', '/manage/admin/sys/log/add', '/manage/admin.sys.Log/add', '_manage_admin_sys_log_add', '', '_self', '', '0', '', '', '1', '1600936473', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1504', '1501', 'manage', 'system', '编辑', '/manage/admin/sys/log/edit', '/manage/admin.sys.Log/edit', '_manage_admin_sys_log_edit', '', '_self', '', '0', '', '', '1', '1600936641', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1505', '1501', 'manage', 'system', '修改', '/manage/admin/sys/log/modify', '/manage/admin.sys.Log/modify', '_manage_admin_sys_log_modify', '', '_self', '', '0', '', '', '1', '1600936688', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1506', '1501', 'manage', 'system', '删除', '/manage/admin/sys/log/del', '/manage/admin.sys.Log/del', '_manage_admin_sys_log_del', '', '_self', '', '0', '', '', '1', '1600936742', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1507', '1500', 'manage', 'system', '删除图文', '/manage/member.wechatKeyword/removeMaterial', '/manage/member.wechatKeyword/removeMaterial', 'manage_member_wechatKeyword_removeMaterial', '', '_self', '', '0', '', '', '1', '1600936810', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1508', '828', 'manage', 'system', '操作日志', '/manage/member/sys/log/index', '/manage/member.sys.Log/index', 'manage_member_sys_log_index', '', '_self', '', '1', 'fa fa-book', '', '1', '1600938881', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1509', '1508', 'manage', 'system', '导出', '/manage/member/sys/log/export', '/manage/member.sys.Log/export', '_manage_member_sys_log_export', '', '_self', '', '0', '', '', '1', '1600938928', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1510', '1508', 'manage', 'system', '添加', '/manage/member/sys/log/add', '/manage/member.sys.Log/add', '_manage_member_sys_log_add', '', '_self', '', '0', '', '', '1', '1600938998', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1511', '1508', 'manage', 'system', '编辑', '/manage/member/sys/log/edit', '/manage/member.sys.Log/edit', '_manage_member_sys_log_edit', '', '_self', '', '0', '', '', '1', '1600939043', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1512', '1508', 'manage', 'system', '修改', '/manage/member/sys/log/modify', '/manage/member.sys.Log/modify', '_manage_member_sys_log_modify', '', '_self', '', '0', '', '', '1', '1600939126', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1514', '3', 'manage', 'system', '登录日志', '/manage/admin/loginInfo', '/manage/admin.Index/getAdminLoginInfo', '_manage_admin_loginInfo', '', '_self', '', '0', '', '', '1', '1600951437', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1515', '1443', 'manage', 'system', '登录日志', '/manage/member/loginInfo', '/manage/member.Index/getMemberLoginInfo', '_manage_member_loginInfo', '', '_self', '', '0', '', '', '1', '1600998614', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1516', '1586', 'manage', 'system', '租户公告', '/manage/admin/sys/notice/index', '/manage/admin.sys.Notice/index', 'manage_admin_sys_notice_index', '', '_self', '', '1', '', '', '1', '1601015082', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1517', '1516', 'manage', 'system', '导出', '/manage/admin/sys/notice/export', '/manage/admin.sys.Notice/export', '_manage_admin_sys_notice_export', '', '_self', '', '0', '', '', '1', '1601015138', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1518', '1516', 'manage', 'system', '添加', '/manage/admin/sys/notice/add', '/manage/admin.sys.Notice/add', '_manage_admin_sys_notice_add', '', '_self', '', '0', '', '', '1', '1601015217', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1519', '1516', 'manage', 'system', '编辑', '/manage/admin/sys/notice/edit', '/manage/admin.sys.Notice/edit', '_manage_admin_sys_notice_edit', '', '_self', '', '0', '', '', '1', '1601015279', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1520', '1516', 'manage', 'system', '修改', '/manage/admin/sys/notice/modify', '/manage/admin.sys.Notice/modify', '_manage_admin_sys_notice_modify', '', '_self', '', '0', '', '', '1', '1601015357', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1521', '1516', 'manage', 'system', '删除', '/manage/admin/sys/notice/del', '/manage/admin.sys.Notice/del', '_manage_admin_sys_notice_del', '', '_self', '', '0', '', '', '1', '1601015420', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1522', '1443', 'manage', 'system', '系统公告列表', '/manage/member/noticeList', '/manage/member.Index/getNoticeList', '_manage_member_noticeList', '', '_self', '', '0', '', '', '1', '1601016826', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1523', '1443', 'manage', 'system', '用户图表数据', '/manage/member/echartsInfo', '/manage/member.Index/getEchartsInfo', '_manage_member_echartsInfo', '', '_self', '', '0', '', '', '1', '1601035845', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1526', '891', 'manage', 'system', '禁用', '/manage/admin.Plugin/disable', '/manage/admin.Plugin/disable', '/manage/admin.Plugin/disable', '', '_self', '', '0', '', '', '1', '1601275727', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1527', '862', 'manage', 'system', '提交代码给微信官网审核', '/manage/member/submitAudit', '/manage/member.Miniapp/submitAudit', 'manage_member_submitAudit', '', '_self', '', '0', '', '', '1', '1601276072', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1528', '1020', 'manage', 'system', '应用功能', '/manage/admin/miniapp/module/index', '/bwmall/admin.Template/index', '_manage_admin_miniapp_module_index', '', '_self', '', '0', 'fa fa-clipboard', '', '1', '1598843234', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1529', '1528', 'manage', 'system', '导出', '/manage/admin/miniapp/module/export', '/bwmall/admin.Template/export', 'manage_admin_miniapp_module_export', '', '_self', '', '0', '', '', '1', '1598843541', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1530', '1528', 'manage', 'system', '新增', '/manage/admin/miniapp/module/add', '/bwmall/admin.Template/add', 'manage_admin_miniapp_module_add', '', '_self', '', '0', '', '', '1', '1598843630', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1531', '1528', 'manage', 'system', '编辑', '/manage/admin/miniapp/module/edit', '/bwmall/admin.Template/edit', 'manage_admin_miniapp_module_edit', '', '_self', '', '0', '', '', '1', '1598843679', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1532', '1528', 'manage', 'system', '删除', '/manage/admin/miniapp/module/del', '/bwmall/admin.Template/del', 'manage_admin_miniapp_module_del', '', '_self', '', '0', '', '', '1', '1598843807', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1533', '1528', 'manage', 'system', '修改', '/manage/admin/miniapp/module/modify', '/bwmall/admin.Template/modify', 'manage_admin_miniapp_module_modify', '', '_self', '', '0', '', '', '1', '1598843884', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1534', '211', 'manage', 'system', '删除', '/manage/GroupData/softdleting', '/manage/GroupData/softdleting', '_manage_GroupData_softdleting', '', '_self', '', '0', '', '', '1', '1601295884', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1535', '862', 'manage', 'system', '获取代码审核状态', '/manage/member/getAuditStatus', '/manage/member.Miniapp/getAuditStatus', 'manage_member_getAuditStatus', '', '_self', '', '0', '', '', '1', '1601308907', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1536', '862', 'manage', 'system', '发布小程序', '/manage/member/publishCode', '/manage/member.Miniapp/publishCode', 'manage_member_publishCode', '', '_self', '', '0', '', '', '1', '1601311068', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1537', '862', 'manage', 'system', '强制撤销审核', '/manage/member/resetAudit', '/manage/member.Miniapp/resetAudit', 'manage_member_resetAudit', '', '_self', '', '0', '', '', '1', '1601311131', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1539', '1172', 'manage', 'system', '物流快递管理', '/manage/member/sys/express/index', '/manage/member.sys.Express/index', 'manage_member_sys_express_index', '', '_self', '', '1', 'fa fa-bus', '', '0', '1602124262', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1540', '1539', 'manage', 'system', '导出', '/manage/member/sys/express/export', '/manage/member.sys.Express/export', 'manage_member_sys_express_export', '', '_self', '', '0', '', '', '1', '1602124367', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1541', '1539', 'manage', 'system', '添加', '/manage/member/sys/express/add', '/manage/member.sys.Express/add', 'manage_member_sys_express_add', '', '_self', '', '0', '', '', '1', '1602124487', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1542', '1539', 'manage', 'system', '编辑', '/manage/member/sys/express/edit', '/manage/member.sys.Express/edit', 'manage_member_sys_express_edit', '', '_self', '', '0', '', '', '1', '1602124567', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1543', '1539', 'manage', 'system', '修改', '/manage/member/sys/express/modify', '/manage/member.sys.Express/modify', 'manage_member_sys_express_modify', '', '_self', '', '0', '', '', '1', '1602124635', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1544', '1539', 'manage', 'system', '删除', '/manage/member/sys/express/del', '/manage/member.sys.Express/del', 'manage_member_sys_express_del', '', '_self', '', '0', '', '', '1', '1602124720', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1545', '186', 'manage', 'system', '租户管理', '/', '/', '', '', '_self', '', '1', 'fa fa-user', '', '1', '1602253898', '0', '1500', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1558', '1250', 'manage', 'system', '添加规则', '/manage/member.wechatKeyword/addKeys', '/manage/member.wechatKeyword/addKeys', 'manage_member_wechatKeyword_addKeys', '', '_self', '', '0', '', '', '1', '1602837519', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1559', '1250', 'manage', 'system', '预览text', '/manage/member.wechatKeyword/previewText', '/manage/member.wechatKeyword/previewText', 'manage_member_wechatKeyword_previewText', '', '_self', '', '0', '', '', '1', '1603100713', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1560', '1250', 'manage', 'system', '预览news', '/manage/member.wechatKeyword/previewNews', '/manage/member.wechatKeyword/previewNews', 'manage_member_wechatKeyword_previewNews', '', '_self', '', '0', '', '', '1', '1603113498', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1561', '1250', 'manage', 'system', '选择图文', '/manage/member.wechatKeyword/select', '/manage/member.wechatKeyword/select', 'manage_member_wechatKeyword_select', '', '_self', '', '0', '', '', '1', '1603115169', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1562', '1250', 'manage', 'system', '预览图片', '/manage/member.wechatKeyword/previewImage', '/manage/member.wechatKeyword/previewImage', 'manage_member_wechatKeyword_previewImage', '', '_self', '', '0', '', '', '1', '1603119741', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1563', '1250', 'manage', 'system', '预览music', '/manage/member.wechatKeyword/previewMusic', '/manage/member.wechatKeyword/previewMusic', 'manage_member_wechatKeyword_previewMusic', '', '_self', '', '0', '', '', '1', '1603126619', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1564', '1250', 'manage', 'system', '预览video', '/manage/member.wechatKeyword/previewVideo', '/manage/member.wechatKeyword/previewVideo', 'manage_member_wechatKeyword_previewVideo', '', '_self', '', '0', '', '', '1', '1603158266', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1565', '1250', 'manage', 'system', '预览语音', '/manage/member.wechatKeyword/previewVoice', '/manage/member.wechatKeyword/previewVoice', 'manage_member_wechatKeyword_previewVoice', '', '_self', '', '0', '', '', '1', '1603159167', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1566', '1500', 'manage', 'system', '添加图文', '/manage/member.wechatKeyword/addMaterial', '/manage/member.wechatKeyword/addMaterial', 'manage_member_wechatKeyword_addMaterial', '', '_self', '', '0', '', '', '1', '1603170723', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1567', '1250', 'manage', 'system', '状态修改', '/manage/member.wechatKeyword/modify', '/manage/member.wechatKeyword/modify', 'manage_member_wechatKeyword_modify', '', '_self', '', '0', '', '', '1', '1603174720', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1583', '834', 'manage', 'system', '关注回复配置', '/manage/member.WechatKeyword/subscribe', '/manage/member.WechatKeyword/subscribe', 'manage_member_WechatKeyword_subscribe', '', '_self', '', '0', 'fa fa-wechat', '', '1', '1603194063', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1584', '1443', 'manage', 'system', '站内信列表', '/manage/member/message/list', '/manage/member.Index/messageList', 'manage_member_message_list', '', '_self', '', '0', '', '', '1', '1603273452', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1585', '1443', 'manage', 'system', '站内信已读', '/manage/member/message/read', '/manage/member.Index/messageRead', 'manage_member_message_read', '', '_self', '', '0', '', '', '1', '1603273452', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1586', '186', 'manage', 'system', '公告管理', '/manage/sysArticle', '/manage/sysArticle', 'manage_sysArticle', '', '_self', '', '1', 'fa fa-bell-o', '', '1', '1603345080', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1587', '1124', 'manage', 'system', '发送站内信', '/manage/admin/message/send', '/manage/admin.member/sendMessage', 'manage_admin_message_send', '', '_self', '', '0', '', '', '1', '1603352194', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('1588', '1412', 'manage', 'system', '发送站内信', '/manage/member/message/send', '/manage/member.User/sendMessage', 'manage_member_message_send', '', '_self', '', '0', '', '', '1', '1603352194', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1589', '1172', 'manage', 'system', '已购应用', '/manage/member/miniapp/my/index', '/manage/member.MemberMiniapp/index', 'manage_member_miniapp_my_index', '', '_self', '', '1', 'fa fa-gift', '', '1', '1591779441', '0', '999', 'member');
INSERT INTO `bw_auth_node` VALUES ('1590', '1500', 'manage', 'system', '编辑图文', '/manage/member.wechatKeyword/editMaterial', '/manage/member.wechatKeyword/editMaterial', 'manage_member_wechatKeyword_editMaterial', '', '_self', '', '0', '', '', '1', '1603949972', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1604', '1396', 'manage', 'system', '删除', '/manage/member.GroupData/del', '/manage/member.GroupData/softdleting', 'manage_member_GroupData_del', '', '_self', '', '0', '', '', '1', '1605072083', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1840', '1314', 'manage', 'system', '插件市场', '/manage/member/plugin', '/manage/member.plugin.Plugin/index', '', '', '_self', '', '1', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1841', '1840', 'manage', 'system', '详情', '/manage/member/plugin/detail', '/manage/member.plugin.Plugin/detail', '', '', '_self', '', '0', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('1842', '1840', 'manage', 'system', '购买', '/manage/member/plugin/buy', '/manage/member.plugin.Plugin/buy', '', '', '_self', '', '0', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2101', '1589', 'manage', 'system', '设置默认应用', '/manage/member/miniapp/my/default', '/manage/member.MemberMiniapp/setDefaultMiniapp', '_manage_member_member_miniapp_default', '', '_self', '', '0', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2170', '190', 'manage', 'system', '城市数据', '/manage/admin/sys/city/index', '/manage/admin.sys.City/index', 'manage_admin_sys_city_index', '', '_self', '', '1', 'fa fa-subway', '', '1', '1600936300', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2171', '2170', 'manage', 'system', '导出', '/manage/admin/sys/city/export', '/manage/admin.sys.City/export', 'manage_admin_sys_city_export', '', '_self', '', '0', '', '', '1', '1600936381', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2172', '2170', 'manage', 'system', '添加', '/manage/admin/sys/city/add', '/manage/admin.sys.City/add', 'manage_admin_sys_city_add', '', '_self', '', '0', '', '', '1', '1600936473', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2173', '2170', 'manage', 'system', '编辑', '/manage/admin/sys/city/edit', '/manage/admin.sys.City/edit', 'manage_admin_sys_city_edit', '', '_self', '', '0', '', '', '1', '1600936641', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2174', '2170', 'manage', 'system', '修改', '/manage/admin/sys/city/modify', '/manage/admin.sys.City/modify', 'manage_admin_sys_city_modify', '', '_self', '', '0', '', '', '1', '1600936688', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2175', '2170', 'manage', 'system', '删除', '/manage/admin/sys/city/del', '/manage/admin.sys.City/del', 'manage_admin_sys_city_del', '', '_self', '', '0', '', '', '1', '1600936742', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2176', '2170', 'manage', 'system', '下载城市数据sql', '/manage/admin/sys/city/download', '/manage/admin.sys.City/download', 'manage_admin_sys_city_download', '', '_self', '', '0', '', '', '1', '1600936742', '0', '0', 'admin');
INSERT INTO `bw_auth_node` VALUES ('2375', '834', 'manage', 'system', '小程序订阅消息', '/manage/member/wechat/message/template/index', '/manage/member.WechatMessageTemplate/index', 'manage_member_wechat_message_template_index', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2376', '2375', 'manage', 'system', '导出', '/manage/member/wechat/message/template/export', '/manage/member.WechatMessageTemplate/export', 'manage_member_wechat_message_template_export', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2377', '2375', 'manage', 'system', '新增', '/manage/member/wechat/message/template/add', '/manage/member.WechatMessageTemplate/add', 'manage_member_wechat_message_template_add', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2378', '2375', 'manage', 'system', '编辑', '/manage/member/wechat/message/template/edit', '/manage/member.WechatMessageTemplate/edit', 'manage_member_wechat_message_template_edit', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2379', '2375', 'manage', 'system', '修改', '/manage/member/wechat/message/template/modify', '/manage/member.WechatMessageTemplate/modify', 'manage_member_wechat_message_template_modify', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `bw_auth_node` VALUES ('2380', '2375', 'manage', 'system', '删除', '/manage/member/wechat/message/template/del', '/manage/member.WechatMessageTemplate/del', 'manage_member_wechat_message_template_del', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');

-- ----------------------------
-- Table structure for bw_cloud_order
-- ----------------------------
DROP TABLE IF EXISTS `bw_cloud_order`;
CREATE TABLE `bw_cloud_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT '套餐ID',
  `amount` int(10) unsigned NOT NULL COMMENT '次数',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '价格',
  `days` int(10) unsigned NOT NULL COMMENT '有效天数',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `service_id` (`service_id`),
  KEY `packet_id` (`packet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='云市场订单表';

-- ----------------------------
-- Records of bw_cloud_order
-- ----------------------------

-- ----------------------------
-- Table structure for bw_cloud_packet
-- ----------------------------
DROP TABLE IF EXISTS `bw_cloud_packet`;
CREATE TABLE `bw_cloud_packet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `amount` int(10) unsigned NOT NULL COMMENT '次数',
  `price` decimal(12,2) unsigned NOT NULL COMMENT '价格',
  `days` int(10) unsigned NOT NULL COMMENT '有效天数',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='云市场套餐表';

-- ----------------------------
-- Records of bw_cloud_packet
-- ----------------------------
INSERT INTO `bw_cloud_packet` VALUES ('1', '3', '100', '10.00', '365', '1594969942', '1594969942', '0');
INSERT INTO `bw_cloud_packet` VALUES ('2', '2', '1', '0.10', '365', '1595058043', '1595058043', null);
INSERT INTO `bw_cloud_packet` VALUES ('3', '2', '100', '10.00', '365', '1595058069', '1595058069', null);
INSERT INTO `bw_cloud_packet` VALUES ('5', '4', '10', '1.00', '365', '1595208506', '1595208506', null);
INSERT INTO `bw_cloud_packet` VALUES ('6', '4', '1', '0.10', '365', '1595209849', '1595209849', null);

-- ----------------------------
-- Table structure for bw_cloud_service
-- ----------------------------
DROP TABLE IF EXISTS `bw_cloud_service`;
CREATE TABLE `bw_cloud_service` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `file_name` varchar(50) NOT NULL COMMENT '文件名',
  `params` varchar(255) NOT NULL COMMENT '参数',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_name` (`file_name`),
  KEY `id` (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='云市场服务表';

-- ----------------------------
-- Records of bw_cloud_service
-- ----------------------------
INSERT INTO `bw_cloud_service` VALUES ('1', '短信', 'Sms', 'mobile,code', '1594881243', '1594881243');
INSERT INTO `bw_cloud_service` VALUES ('2', 'IP归属地查询', 'Ip', 'ip', '1594881243', '1594881243');
INSERT INTO `bw_cloud_service` VALUES ('3', '身份证实名认证', 'Idcard', 'idcard,realname', '1594881243', '1594881243');
INSERT INTO `bw_cloud_service` VALUES ('4', '全国快递物流查询', 'Kdi', 'no', '1595037496', '1595037496');

-- ----------------------------
-- Table structure for bw_cloud_wallet
-- ----------------------------
DROP TABLE IF EXISTS `bw_cloud_wallet`;
CREATE TABLE `bw_cloud_wallet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `amount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '剩余次数',
  `total` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总次数',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`,`service_id`) USING BTREE,
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='云市场钱包表';

-- ----------------------------
-- Records of bw_cloud_wallet
-- ----------------------------

-- ----------------------------
-- Table structure for bw_cloud_wallet_bill
-- ----------------------------
DROP TABLE IF EXISTS `bw_cloud_wallet_bill`;
CREATE TABLE `bw_cloud_wallet_bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `type` enum('buy','use') DEFAULT 'buy' COMMENT '类型:buy=购买套餐,use=使用服务',
  `value` int(10) NOT NULL COMMENT '变更数量',
  `before` int(10) unsigned NOT NULL COMMENT '变更前',
  `after` int(10) unsigned NOT NULL COMMENT '变更后',
  `memo` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `service_id` (`service_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COMMENT='云市场钱包记录表';

-- ----------------------------
-- Records of bw_cloud_wallet_bill
-- ----------------------------

-- ----------------------------
-- Table structure for bw_config_apis
-- ----------------------------
DROP TABLE IF EXISTS `bw_config_apis`;
CREATE TABLE `bw_config_apis` (
  `id` tinyint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `apikey` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='阿里API市场接口配置';

-- ----------------------------
-- Records of bw_config_apis
-- ----------------------------
INSERT INTO `bw_config_apis` VALUES ('1', 'wechatopen', '{\"app_id\":\"111111111111111111\",\"secret\":\"11111111111111111111111111111111\",\"token\":\"dgqnFLTrIO1Sx7mUI4mn55ur6rJMIesL\",\"aes_key\":\"WW9k2sCjAWIoFK3oXbojQtvUxnhRmnbdvRjl8TcOYIb\"}');

-- ----------------------------
-- Table structure for bw_crud_demo
-- ----------------------------
DROP TABLE IF EXISTS `bw_crud_demo`;
CREATE TABLE `bw_crud_demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) NOT NULL COMMENT '名称',
  `image` varchar(255) NOT NULL COMMENT '单图',
  `images` varchar(500) DEFAULT NULL COMMENT '多图',
  `sales` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '销量',
  `desc` text COMMENT '描述{editor}',
  `switch` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '开关{switch}(0:关闭,1:打开)',
  `pay_time` int(10) unsigned DEFAULT NULL COMMENT '支付时间{date}(range)',
  `status` enum('0','1') DEFAULT '1' COMMENT '状态{switch}(0:下架,1:上架)',
  `create_time` int(10) NOT NULL COMMENT '创建时间{date}(range)',
  `update_time` int(10) DEFAULT '0' COMMENT '更新时间{date}(range)',
  `delete_time` int(10) DEFAULT '0' COMMENT '删除时间{date}(range)',
  `send` date DEFAULT NULL COMMENT '发放时间{date}(date)',
  `fruit` set('apple','orange','banana') DEFAULT 'orange,banana' COMMENT '水果{checkbox}(apple:苹果,orange:橘子,banana:香蕉)',
  `interest` set('basket','run','swim') NOT NULL DEFAULT '' COMMENT '爱好{checkbox}(swim:游泳,run:跑步,basket:篮球)',
  `cate_id` int(11) unsigned DEFAULT NULL COMMENT '分类{select}(1:服装,2:家电)',
  `h_video` varchar(255) DEFAULT NULL COMMENT '单视频{file}(mp4)',
  `s_videos` varchar(255) DEFAULT NULL COMMENT '多视频{files}(mp4)',
  `crt_file` varchar(255) DEFAULT '' COMMENT '证书文件{file}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 COMMENT='crud测试表';

-- ----------------------------
-- Records of bw_crud_demo
-- ----------------------------
INSERT INTO `bw_crud_demo` VALUES ('79', 'test1', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png,/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '121', '&lt;p&gt;&lt;img src=&quot;/uploads/image/20200522/1590116240262164.gif&quot; title=&quot;1590116240262164.gif&quot; alt=&quot;c2afbcd64b9c4acb93bab45f6303f964.gif&quot;/&gt;&lt;/p&gt;', '0', '1590116250', '0', '1590116254', '1596556216', '0', '2020-05-22', 'apple,orange,banana', 'basket,run,swim', '1', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('82', '12345', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '', '0', '&lt;p&gt;&lt;img src=&quot;/uploads/image/20200520/1589954763140415.gif&quot; title=&quot;1589954763140415.gif&quot; alt=&quot;c2afbcd64b9c4acb93bab45f6303f964.gif&quot;/&gt;&lt;/p&gt;', '1', '0', '1', '1589954782', '1589966661', '0', '2020-05-20', '', 'basket,swim', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('95', '1114', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png,/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '0', '', '1', '2020', '0', '1590747710', '1596859462', '0', '2020-05-29', 'apple', 'basket', '2', '', '', '');
INSERT INTO `bw_crud_demo` VALUES ('96', '1111', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '0', '&lt;p&gt;123333333333&lt;/p&gt;', '0', '1590827825', '0', '1590827820', '1597387186', '0', '2020-05-30', 'apple', 'basket', '2', '/uploads/image/20200530/f1f51f51c2d4a44ed1483b41d86c00f0.mp4', '/uploads/image/20200530/41366e1a1b561a823ae7ba907b51aa99.mp4,/uploads/image/20200530/6b2df0b2c65ff9958be5b302884c5e4a.mp4', '');
INSERT INTO `bw_crud_demo` VALUES ('97', '哈哈', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '/uploads/image/20200805/eae1ca409eec1e7ac200d233079dcbc8.png', '2', '&lt;p&gt;1&lt;/p&gt;\n\n&lt;p&gt;1&lt;/p&gt;\n\n&lt;p&gt;1&lt;/p&gt;\n\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;', '0', '2020', '0', '1590828928', '1597032130', '0', '2020-08-10', 'apple', 'swim', '1', '', '', '');
INSERT INTO `bw_crud_demo` VALUES ('102', 'bwsaas', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '&lt;p&gt;wewe&lt;/p&gt;', '1', '2020', '0', '1597214970', '1597373350', '0', '2020-08-12', 'orange,banana', '', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('103', '543545', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '&lt;p&gt;454545&lt;/p&gt;', '1', '2020', '0', '1597217438', '1597373350', '0', '2020-08-12', 'orange,banana', '', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('104', '3', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '2020', '0', '1597223319', '1597373708', '0', '2020-08-13', 'orange,banana', 'run,swim', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('105', '3535', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '2020', '0', '1597224144', '1597373708', '0', '2020-08-13', 'orange,banana', 'run', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('106', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '&lt;p&gt;564564&lt;/p&gt;', '1', '2020', '0', '1597224209', '1597373708', '0', '2020-08-13', 'orange,banana', 'basket,run', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('107', '24324', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '2020', '0', '1597291955', '1597373708', '0', '2020-08-13', 'apple,banana', 'basket,swim', '1', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('108', 'bwsaas', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '0', '0', '0', '1597300331', '1597300331', '0', '2020-08-13', 'orange,banana', 'run', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('109', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '0', '1597301120', '1597373708', '0', '2020-08-13', 'orange,banana', 'run', '0', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('110', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '0', '1597301248', '1597373708', '0', '2020-08-13', '', '', '0', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('111', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '0', '1597301653', '1597373708', '0', '2020-08-13', 'orange,banana', 'run', '2', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('112', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '0', '0', '0', '1597301744', '1597373338', '0', '2020-08-13', 'orange,banana', '', '0', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('113', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '1', '1597301808', '1597373338', '0', '2020-08-13', 'orange,banana', '', '0', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('114', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '0', '0', '0', '1597302049', '1597373338', '0', '2020-08-13', 'orange,banana', '', '0', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('115', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '1', '1597303687', '1597373338', '0', '2020-08-13', 'orange,banana', '', '0', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('116', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '1', '1597303784', '1597303784', '0', '2020-08-13', 'orange,banana', 'basket,run', '1', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('117', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '0', '1597303985', '1597373708', '0', '2020-08-13', 'apple,orange,banana', 'basket,run,swim', '1', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('118', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '1', '1597304533', '1597373708', '0', '2020-08-13', 'orange,banana', 'run', '1', null, null, '');
INSERT INTO `bw_crud_demo` VALUES ('119', '用户接口', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', 'http://buwangyun.oss-accelerate.aliyuncs.com/upload/20200812/e9f0e277b4b5cbbfb63aa145fba400ae.jpg', '0', '', '1', '0', '1', '1597304955', '1597304955', '0', '2020-08-13', 'orange,banana', 'run', '1', null, null, '');

-- ----------------------------
-- Table structure for bw_crud_demo_cate
-- ----------------------------
DROP TABLE IF EXISTS `bw_crud_demo_cate`;
CREATE TABLE `bw_crud_demo_cate` (
  `id` int(11) unsigned NOT NULL COMMENT '序号',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bw_crud_demo_cate
-- ----------------------------
INSERT INTO `bw_crud_demo_cate` VALUES ('1', '111', '111', '111');

-- ----------------------------
-- Table structure for bw_crud_demo_catee
-- ----------------------------
DROP TABLE IF EXISTS `bw_crud_demo_catee`;
CREATE TABLE `bw_crud_demo_catee` (
  `id` int(11) unsigned NOT NULL COMMENT '序号',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `headimage` varchar(255) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bw_crud_demo_catee
-- ----------------------------
INSERT INTO `bw_crud_demo_catee` VALUES ('1', '服装', '0', '0', '/uploads/image/20200522/524a195435b67d9fe8b3ca8fcad5702f.gif');
INSERT INTO `bw_crud_demo_catee` VALUES ('2', '家电', '0', '0', '/uploads/image/20200522/524a195435b67d9fe8b3ca8fcad5702f.gif');

-- ----------------------------
-- Table structure for bw_member
-- ----------------------------
DROP TABLE IF EXISTS `bw_member`;
CREATE TABLE `bw_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `mobile` varchar(15) DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `safe_password` varchar(255) NOT NULL DEFAULT '' COMMENT '安全密码',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '上级id',
  `top_id` bigint(20) DEFAULT '0' COMMENT '顶级租户id',
  `bind_member_miniapp_id` int(11) DEFAULT '0' COMMENT '应用id',
  `ticket` varchar(255) DEFAULT NULL,
  `openid` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=冻结',
  `lock_config` enum('0','1') DEFAULT '1' COMMENT '配置权限:1=开启,0=关闭',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '登录ip',
  `login_time` int(11) DEFAULT NULL COMMENT '登录时间',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `default_miniapp_dir` varchar(20) NOT NULL DEFAULT 'manage' COMMENT '后台菜单默认应用目录',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='租户成员';

-- ----------------------------
-- Records of bw_member
-- ----------------------------
INSERT INTO `bw_member` VALUES ('64', 'bw17777777777', 'bw17777777777', '17777777777', '', '$2y$10$5ijQtNxDnfJgalIkDuDS5uFcP5LIQh7OqFq..MQkSCJD6uwKQIN16', '$2y$10$W/m69OAvSn9AUoSuJNq0F.LEvQB3MR/kUI9m8juL30rb58IbPFUEm', '0', '64', '0', null, null, '1', '0', '127.0.0.1', '1608712976', '1608712951', null, 'manage');

-- ----------------------------
-- Table structure for bw_member_form
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_form`;
CREATE TABLE `bw_member_form` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `form_id` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `is_del` tinyint(2) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小程序模板消息ID';

-- ----------------------------
-- Records of bw_member_form
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_miniapp`;
CREATE TABLE `bw_member_miniapp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `service_id` varchar(200) DEFAULT '' COMMENT '客户端ID',
  `miniapp_order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `member_id` int(11) DEFAULT '0' COMMENT '创建人/购买人',
  `miniapp_id` int(11) DEFAULT '0' COMMENT '应用id',
  `appname` varchar(100) DEFAULT '' COMMENT '应用名称',
  `template_id` int(5) NOT NULL DEFAULT '0' COMMENT '购买时小程序模板id',
  `version` varchar(50) NOT NULL DEFAULT '' COMMENT '购买应用时版本号',
  `head_img` varchar(255) DEFAULT NULL COMMENT 'Logo{image}',
  `qrcode_url` varchar(255) DEFAULT NULL COMMENT '演示二维码',
  `miniapp_appid` char(50) DEFAULT NULL COMMENT '小程序appid',
  `miniapp_open_auth` enum('0','1') DEFAULT '0' COMMENT '小程序开放平台是否授权',
  `miniapp_secret` varchar(255) DEFAULT NULL COMMENT '小程序secret',
  `miniapp_head_img` varchar(255) DEFAULT NULL COMMENT '小程序logo{image}',
  `miniapp_qrcode_url` varchar(255) DEFAULT NULL COMMENT '小程序演示二维码',
  `navbar_color` varchar(50) DEFAULT NULL COMMENT '导航栏颜色',
  `navbar_style` varchar(50) DEFAULT NULL COMMENT '导航栏样式',
  `service_time` int(11) DEFAULT '0' COMMENT '服务时间',
  `mp_appid` varchar(30) DEFAULT NULL COMMENT '公众号appid',
  `mp_secret` varchar(50) DEFAULT NULL COMMENT '公众号secret',
  `mp_head_img` varchar(255) DEFAULT NULL COMMENT '公众号logo{image}',
  `mp_qrcode_url` varchar(255) DEFAULT NULL COMMENT '公众号演示二维码',
  `mp_open_auth` enum('1','0') DEFAULT '0' COMMENT '公众号开放平台是否授权',
  `mp_token` varchar(50) DEFAULT NULL COMMENT '公众号token',
  `mp_aes_key` varchar(50) DEFAULT NULL COMMENT '公众号AesKey',
  `is_psp` enum('0','1') DEFAULT '0' COMMENT '微信服务商:0=独立应用,1=微信服务商',
  `psp_appid` varchar(32) DEFAULT NULL COMMENT '服务商appid',
  `is_open` enum('0','1') DEFAULT '0' COMMENT '开放平台授权情况，0：未授权1：已授权',
  `is_lock` enum('0','1') DEFAULT '0' COMMENT '状态:0=正常,1=锁定',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `expire_time` int(10) DEFAULT '0' COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='客户应用表';

-- ----------------------------
-- Records of bw_member_miniapp
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp_audit
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_miniapp_audit`;
CREATE TABLE `bw_member_miniapp_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `is_commit` tinyint(1) DEFAULT NULL COMMENT '1基础信息设置2上传代码3提交审核4发布小程序',
  `trial_qrcode` varchar(255) DEFAULT NULL COMMENT '二维码',
  `auditid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bw_member_miniapp_audit
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp_order
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_miniapp_order`;
CREATE TABLE `bw_member_miniapp_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(11) NOT NULL COMMENT '租户ID',
  `miniapp_id` int(11) NOT NULL COMMENT '应用ID',
  `miniapp_module_id` int(10) NOT NULL DEFAULT '0' COMMENT '功能id',
  `title` varchar(20) NOT NULL COMMENT '应用名字',
  `update_var` int(11) DEFAULT '0' COMMENT '小程序模板ID',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `valid_days` int(10) NOT NULL DEFAULT '0' COMMENT '有效天数',
  `expire_time` int(10) NOT NULL COMMENT '到期时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:0=关闭,1=正常',
  `create_time` int(10) NOT NULL COMMENT '购买时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `miniapp_module_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '功能类型:1=基础功能,2=附加功能',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `miniapp_id` (`miniapp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='应用购买订单表';

-- ----------------------------
-- Records of bw_member_miniapp_order
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_node
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_node`;
CREATE TABLE `bw_member_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `node_id` int(10) unsigned NOT NULL COMMENT '权限节点ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id` (`member_id`,`node_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5298 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='租户权限表';

-- ----------------------------
-- Records of bw_member_node
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_official_menu
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_official_menu`;
CREATE TABLE `bw_member_official_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_miniapp_id` int(11) DEFAULT '0' COMMENT '用户应用id',
  `appid` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '小程序APPID',
  `types` varchar(20) NOT NULL DEFAULT 'view' COMMENT '链接类型:view=网页类型,miniprogram=小程序类型,click=点击类型',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `url` varchar(255) DEFAULT '' COMMENT '链接',
  `key` varchar(255) DEFAULT '' COMMENT '回复规则id',
  `pagepath` varchar(255) DEFAULT '' COMMENT '小程序地址',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='公众号菜单关联';

-- ----------------------------
-- Records of bw_member_official_menu
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_payment
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_payment`;
CREATE TABLE `bw_member_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiname` char(20) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `config` text,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bw_member_payment
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_plugin_order
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_plugin_order`;
CREATE TABLE `bw_member_plugin_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `plugin_id` int(10) unsigned NOT NULL COMMENT '插件ID',
  `price` decimal(12,2) unsigned NOT NULL COMMENT '价格',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='插件订单表';

-- ----------------------------
-- Records of bw_member_plugin_order
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_wallet
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_wallet`;
CREATE TABLE `bw_member_wallet` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '帐号余额',
  `freeze_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='租户资金表';

-- ----------------------------
-- Records of bw_member_wallet
-- ----------------------------
INSERT INTO `bw_member_wallet` VALUES ('38', '64', '0.00', '0.00', '1608712951', '1608712951');

-- ----------------------------
-- Table structure for bw_member_wallet_bill
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_wallet_bill`;
CREATE TABLE `bw_member_wallet_bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(11) DEFAULT '0' COMMENT '租户ID',
  `money_type` enum('money','freeze_money') NOT NULL DEFAULT 'money' COMMENT '资金类型',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '类型:admin=系统,recharge=充值.miniapp=购买应用,cloud_packet=购买云市场套餐',
  `value` decimal(10,2) DEFAULT '0.00' COMMENT '变更',
  `before` decimal(10,2) DEFAULT '0.00' COMMENT '变更前',
  `after` decimal(10,2) DEFAULT '0.00' COMMENT '变更后',
  `memo` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COMMENT='租户资金变动表';

-- ----------------------------
-- Records of bw_member_wallet_bill
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_wallet_recharge
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_wallet_recharge`;
CREATE TABLE `bw_member_wallet_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `order_sn` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `state` tinyint(1) unsigned DEFAULT '0',
  `remarks` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `type` int(2) DEFAULT '0' COMMENT '0为微信充值 1为支付宝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bw_member_wallet_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_wechat_tpl
-- ----------------------------
DROP TABLE IF EXISTS `bw_member_wechat_tpl`;
CREATE TABLE `bw_member_wechat_tpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `tplmsg_common_app` varchar(255) DEFAULT NULL,
  `tplmsg_common_wechat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of bw_member_wechat_tpl
-- ----------------------------

-- ----------------------------
-- Table structure for bw_message
-- ----------------------------
DROP TABLE IF EXISTS `bw_message`;
CREATE TABLE `bw_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型{radio}(1:平台->租户,2:平台->用户,3:租户->用户,4:租户->用户系统通知)',
  `sender_id` int(10) unsigned NOT NULL COMMENT '发送者ID',
  `receiver_id` int(10) unsigned NOT NULL COMMENT '接收者ID',
  `msg` varchar(255) NOT NULL COMMENT '内容',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态{radio}(0:未读,1:已读)',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`sender_id`,`receiver_id`,`status`,`delete_time`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COMMENT='站内信';

-- ----------------------------
-- Records of bw_message
-- ----------------------------

-- ----------------------------
-- Table structure for bw_miniapp
-- ----------------------------
DROP TABLE IF EXISTS `bw_miniapp`;
CREATE TABLE `bw_miniapp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dir` varchar(50) NOT NULL COMMENT '目录',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `describe` varchar(255) NOT NULL COMMENT '描述内容',
  `logo_image` varchar(255) NOT NULL COMMENT 'Logo',
  `qrcode_image` varchar(255) NOT NULL COMMENT '演示二维码',
  `style_images` varchar(500) NOT NULL COMMENT '应用截图',
  `sell_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '销售价',
  `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '市场价',
  `version` varchar(50) NOT NULL COMMENT '版本',
  `template_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序代码库中的代码模板 ID',
  `expire_day` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '体验天数',
  `content` text COMMENT '详情介绍',
  `types` set('mini_program','app','h5','official','system') NOT NULL DEFAULT 'mini_program' COMMENT '应用类型:mini_program=小程序,app=APP,h5=h5,official=公众号,system=系统',
  `is_manage` enum('0','1') DEFAULT '0' COMMENT '平台管理:0=关闭,1=开启',
  `is_openapp` enum('0','1') DEFAULT '0' COMMENT '接入方式:0=独立应用,1=开放平台',
  `is_wechat_pay` enum('0','1') DEFAULT '0' COMMENT '微信支付:0=关闭,1=开启',
  `is_alipay_pay` enum('0','1') DEFAULT '0' COMMENT '支付宝支付:0=关闭,1=开启',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '排序',
  `status` enum('-1','0','1') DEFAULT '0' COMMENT '状态:-1=未安装,0=关闭,1=开启',
  `create_time` int(11) unsigned NOT NULL COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL COMMENT '更新时间',
  `is_diy` tinyint(1) unsigned DEFAULT '0' COMMENT '是否定制应用:0=否,1=是',
  `diy_member_ids` varchar(255) DEFAULT '' COMMENT '定制租户ID,使用,分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='应用表';

-- ----------------------------
-- Records of bw_miniapp
-- ----------------------------

-- ----------------------------
-- Table structure for bw_miniapp_module
-- ----------------------------
DROP TABLE IF EXISTS `bw_miniapp_module`;
CREATE TABLE `bw_miniapp_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `miniapp_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `group_id` int(11) NOT NULL COMMENT '组id',
  `name` varchar(100) NOT NULL COMMENT '功能名',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型{radio}(1:基础功能,2:附加功能)',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `valid_days` int(10) unsigned DEFAULT '365' COMMENT '有效天数',
  `desc` text COMMENT '描述',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`) USING BTREE,
  KEY `miniapp_id` (`miniapp_id`),
  KEY `type` (`type`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COMMENT='应用功能表';

-- ----------------------------
-- Records of bw_miniapp_module
-- ----------------------------

-- ----------------------------
-- Table structure for bw_plugin
-- ----------------------------
DROP TABLE IF EXISTS `bw_plugin`;
CREATE TABLE `bw_plugin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(40) NOT NULL COMMENT '标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `description` text COMMENT '描述',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `status` enum('1','0','-1') NOT NULL DEFAULT '-1' COMMENT '状态:-1=未安装,0=禁用,1=启用',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `price` decimal(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `type` varchar(20) NOT NULL COMMENT '类型:admin_system平台系统插件,member_system租户系统插件,member_bwmall租户应用插件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='插件表';

-- ----------------------------
-- Records of bw_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sms
-- ----------------------------
DROP TABLE IF EXISTS `bw_sms`;
CREATE TABLE `bw_sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) NOT NULL DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='短信验证码表';

-- ----------------------------
-- Records of bw_sms
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_article
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_article`;
CREATE TABLE `bw_sys_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `title` varchar(255) NOT NULL COMMENT '文章标题',
  `synopsis` varchar(255) DEFAULT NULL COMMENT '文章简介',
  `author` varchar(255) DEFAULT NULL COMMENT '文章作者',
  `image` varchar(255) NOT NULL COMMENT '文章图片',
  `visit` int(10) DEFAULT '0' COMMENT '浏览次数',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '状态:0=隐藏,1=正常',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`category_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of bw_sys_article
-- ----------------------------
INSERT INTO `bw_sys_article` VALUES ('1', '0', '1', '文章1', '文章1简介', '文章1作者', '/uploads/image/20200525/5e1be43310786fdc505d2324299b8560.jpg', '0', '0', '1', '1590403318', '1590403318');

-- ----------------------------
-- Table structure for bw_sys_article_category
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_article_category`;
CREATE TABLE `bw_sys_article_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章分类id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `title` varchar(255) NOT NULL COMMENT '文章分类标题',
  `intr` varchar(255) DEFAULT NULL COMMENT '文章分类简介',
  `image` varchar(255) NOT NULL COMMENT '文章分类图片',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '状态:0=隐藏,1=正常',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) unsigned DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章分类表';

-- ----------------------------
-- Records of bw_sys_article_category
-- ----------------------------
INSERT INTO `bw_sys_article_category` VALUES ('1', '0', '行业新闻', '行业新闻简介', '/uploads/image/20200525/d79a9edc08bcfe0e31fd8a6540ebf954.jpg', '0', '1', '1590403282', '1590403282', '0');

-- ----------------------------
-- Table structure for bw_sys_article_content
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_article_content`;
CREATE TABLE `bw_sys_article_content` (
  `article_id` int(10) unsigned NOT NULL COMMENT '文章id',
  `content` text NOT NULL COMMENT '文章内容',
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章内容表';

-- ----------------------------
-- Records of bw_sys_article_content
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_attachment
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_attachment`;
CREATE TABLE `bw_sys_attachment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '附件名称',
  `att_dir` varchar(200) NOT NULL DEFAULT '' COMMENT '附件路径',
  `satt_dir` varchar(200) DEFAULT NULL COMMENT '压缩图片路径',
  `att_size` char(30) NOT NULL DEFAULT '' COMMENT '附件大小',
  `att_type` char(30) NOT NULL DEFAULT '' COMMENT '附件类型',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '分类ID0编辑器,1商品图片,2拼团图片,3砍价图片,4秒杀图片,5文章图片,6组合数据图',
  `time` int(11) NOT NULL DEFAULT '0' COMMENT '上传时间',
  `image_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '图片上传类型 1本地 2七牛云 3OSS 4COS ',
  `module_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '图片上传模块类型 1 后台上传 2 用户生成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件管理表';

-- ----------------------------
-- Records of bw_sys_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_attachment_category
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_attachment_category`;
CREATE TABLE `bw_sys_attachment_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '父级ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `enname` varchar(50) DEFAULT NULL COMMENT '分类目录',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件分类表';

-- ----------------------------
-- Records of bw_sys_attachment_category
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_city
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_city`;
CREATE TABLE `bw_sys_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '省市级别',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `area_code` varchar(30) NOT NULL DEFAULT '' COMMENT '行政区划代码',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `merger_name` varchar(255) NOT NULL DEFAULT '' COMMENT '合并名称',
  `lng` varchar(50) NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(50) NOT NULL DEFAULT '' COMMENT '纬度',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否展示{radio}(1:是,0:否)',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `city_id` (`city_id`),
  KEY `name` (`name`),
  KEY `parent_id` (`parent_id`),
  KEY `id` (`id`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=3971 DEFAULT CHARSET=utf8 COMMENT='城市表';

-- ----------------------------
-- Records of bw_sys_city
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_config`;
CREATE TABLE `bw_sys_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置id',
  `config_name` varchar(255) NOT NULL COMMENT '字段名称',
  `tab_name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类英文标识',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '类型(文本框,单选按钮...)',
  `input_type` varchar(20) DEFAULT 'input' COMMENT '表单类型',
  `tab_id` int(10) unsigned NOT NULL COMMENT '配置分类id',
  `parameter` varchar(255) DEFAULT NULL COMMENT '单选框和多选框参数',
  `upload_type` tinyint(1) unsigned DEFAULT NULL COMMENT '上传格式1单图2多图3文件',
  `rule` varchar(255) DEFAULT NULL COMMENT '验证规则',
  `message` varchar(255) DEFAULT NULL COMMENT '验证规则错误提示',
  `width` int(10) unsigned DEFAULT NULL COMMENT '多行文本框的宽度',
  `high` int(10) unsigned DEFAULT NULL COMMENT '多行文框的高度',
  `value` varchar(5000) DEFAULT NULL COMMENT '默认值',
  `info` varchar(255) NOT NULL DEFAULT '' COMMENT '配置名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '配置简介',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏0正常1隐藏',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户创建)',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否锁值:1是2否',
  `lazy` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否懒加载:1是2否',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`),
  KEY `tab_id` (`tab_id`),
  KEY `input_type` (`input_type`),
  KEY `member_id` (`member_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `scopes` (`scopes`),
  KEY `disabled` (`disabled`),
  KEY `lazy` (`lazy`)
) ENGINE=InnoDB AUTO_INCREMENT=4534 DEFAULT CHARSET=utf8 COMMENT='配置设置表';

-- ----------------------------
-- Records of bw_sys_config
-- ----------------------------
INSERT INTO `bw_sys_config` VALUES ('261', 'site_upload', 'ceshi', 'upload', 'text', '31', '', '1', '', '', '100', '200', '{\"name\":\"15934162269740.png\",\"src\":\"/uploads/image/20200721/65f57110f8dbc6461f84279ec8680b24.jpg\",\"size\":80468,\"type\":\"image/png\"}', '文件上传1', '文件上传1', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('263', 'qnoss_secret_key', 'qnoss', 'text', 'text', '32', '本地=&gt;public', '1', '', '', '100', '200', '', '私钥信息', '安全密钥', '4', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('266', 'qnoss_access_key', 'qnoss', 'text', 'text', '32', '', '1', 'require', '', '100', '200', '', '访问密钥', '访问密钥', '3', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('267', 'express_secret_key', 'express', 'text', 'text', '34', '', '1', '', '', '100', '200', '', '快递查询秘钥', '请输入快递查询秘钥', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('268', 'asdas_wenjian', 'ceshi', 'upload', 'text', '31', '', '3', '', '', '100', '200', '{\"name\":\"15934162269740.png\",\"size\":95201,\"type\":\"image/png\",\"src\":\"/uploads/image/20200721/dff5399882b699b357add7b50cc59c10.png\"}', '文件', '文件', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('269', 'base_login_mode', 'base', 'radio', 'text', '35', '允许=&gt;1\n不允许=&gt;2', '1', '', '', '100', '200', '1', '同账号多端登录', '是否允许同账号多端登录（只记录最后登录者的登录信息）', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('270', 'app_id', 'wechatopen', 'text', 'text', '36', '', '1', 'require', '', '100', '200', '', 'AppId', 'app_id', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('271', 'secret', 'wechatopen', 'text', 'text', '36', '', '1', 'require', '', '100', '200', '', 'AppSecret', '请输入secret', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('272', 'token', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', '', '消息验证token', '消息验证token', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('273', 'auth_event_url', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', '', '授权事件接收', '授权事件接收', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('274', 'msg_event_url', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', '', '消息与事件接收', '消息与事件接收', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('275', 'aes_key', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', '', '消息加解密key', '消息加解密key', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('276', 'base_login_number', 'base', 'text', 'number', '35', '', '1', '', '', '100', '200', '259200', '刷新token时间', '在还有多少秒即将过期时访问中间件刷新token', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('277', 'web_name', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '布网科技saas', '站点名称', '站点名称', '10', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('278', 'web_title', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '布网科技saas', '站点副标题', '站点副标题', '9', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('279', 'web_url', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', 'https://mall.buwangkeji.com', '域名', '域名', '8', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('280', 'web_logo', 'web_config', 'upload', 'text', '38', '', '1', '', '', '100', '200', '{\"name\":\"企业微信截图_20200312111356.png\",\"size\":1917,\"type\":\"image/png\",\"src\":\"http://mall.buwangkeji.com//uploads/upload/20200812/bc7fbc44508cc7fb24854763fb8f7d1d.png\"}', '站点logo', '站点logo', '7', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('281', 'web_keywords', 'web_config', 'textarea', 'text', '38', '', '1', '', '', '100', '200', '布网科技', 'SEO关键词', 'SEO关键词', '6', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('282', 'web_description', 'web_config', 'textarea', 'text', '38', '', '1', '', '', '100', '200', '布网科技', 'SEO描述', 'SEO描述', '5', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('283', 'web_icp', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '豫ICP备18042703号-1', 'ICP备案号', 'ICP备案号', '4', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('284', 'web_contacts', 'web_config', 'text', 'number', '38', '', '1', '', '', '100', '200', '15098745674', '联系方式', '联系方式', '3', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('285', 'web_address', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '东方经典1', '公司地址', '公司地址', '2', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('286', 'dsfsdf', 'ceshi', 'upload', 'text', '31', '', '2', '', '', '100', '200', '[{\"name\":\"15934162269740.png\",\"src\":\"/uploads/image/20200721/e0cdad5ae6f54104500a81280b92ae9f.png\",\"size\":95201,\"type\":\"image/png\"}]', '手打', 'cvxcv', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('288', 'site_wenben', 'ceshi', 'text', 'text', '31', '', '1', '', '', '100', '200', '300', '测试文本', '测试文本', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('290', 'AppCode', 'aliapi', 'text', 'text', '40', '', '1', '', '', '100', '200', '', 'AppCode', '您的阿里云AppCode', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('291', 'price', 'aliapi', 'text', 'float', '40', '', '1', '', '', '100', '200', '0.1', '价格(RMB)/次', '每次调用的价格', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('416', 'upload_allow_ext', 'base', 'text', 'text', '35', '', '1', '', '', '100', '200', 'doc,gif,ico,icon,jpg,mp3,mp4,p12,pem,png,rar,jpeg,7z,pdf', '允许上传类型', '英文逗号,做分隔符', '7', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('417', 'upload_allow_size', 'base', 'text', 'text', '35', '', '1', '', '', '100', '200', '1024000', '允许上传大小', '允许上传大小', '6', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('418', 'alioss_access_key_id', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', '', '公钥信息', '阿里云oss公钥', '5', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('419', 'alioss_access_key_secret', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', '', '私钥信息', '阿里云oss私钥', '4', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('420', 'alioss_endpoint', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', '', '数据中心', '阿里云oss数据中心', '3', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('421', 'alioss_bucket', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', '', '空间名称', '阿里云oss空间名称', '2', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('422', 'alioss_domain', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', '', '访问域名', '阿里云oss访问域名', '1', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('423', 'upload_type', 'base', 'radio', 'text', '35', '本地存储=&gt;local\n阿里云oss=&gt;alioss\n七牛云oss=&gt;qnoss', '1', '', '', '100', '200', 'local', '上传存储类型', '上传存储类型', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('424', 'qnoss_bucket', 'qnoss', 'text', 'text', '32', '', '1', 'require', '', '100', '200', '', '存储空间', '存储空间', '2', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('425', 'qnoss_domain', 'qnoss', 'text', 'text', '32', '', '1', 'require', '', '100', '200', '', '访问域名', '访问域名', '1', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('427', 'fuxuan_ceshi', 'ceshi', 'checkbox', 'text', '31', '男=>1\n男=>2\n不分=>3', '1', '', '', '100', '200', '1,2', 'fuxuanceshi', 'fuxuanceshi', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('428', 'copyright', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '©版权所有 2015-2020 布网科技', '版权信息', '版权信息', '1', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('437', 'wechat_app_id', 'wechat_pay', 'text', 'text', '41', '', '1', '', '', '100', '200', '', '应用ID(app_id)', '应用ID', '5', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('438', 'wechat_mch_id', 'wechat_pay', 'text', 'text', '41', '', '1', '', '', '100', '200', '', '商户ID(mch_id)', '商户ID(mch_id)', '4', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('439', 'wechat_key', 'wechat_pay', 'text', 'text', '41', '', '1', '', '', '100', '200', '', 'API密钥', 'API密钥', '3', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('440', 'wechat_cert_path', 'wechat_pay', 'upload', 'text', '41', '', '3', '', '', '100', '200', '{\"name\":\"透明.png\",\"size\":172692,\"type\":\"image/png\",\"src\":\"http://www.bwsaas.com/upload/20201103\\\\4848bd71e499aacb95990527c365d45d.png\"}', '支付证书', '支付证书', '2', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('441', 'wechat_key_path', 'wechat_pay', 'upload', 'text', '41', '', '3', '', '', '100', '200', '{\"name\":\"不透明.png\",\"size\":89483,\"type\":\"image/png\",\"src\":\"http://www.bwsaas.com/upload/20201103\\\\7242b6b8e8ebb90f6d39e1123e9a6b0d.png\"}', '证书密钥', '证书密钥:', '1', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('458', 'backstage_top_title', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '控制台', '后台首页标题', '后台首页显示标题', '0', '1', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('459', 'backstage_top_url', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '/manage/admin/dashboard', '后台首页url', '后台首页url', '0', '1', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('460', 'member_top_title', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '首页', '租户后台首页标题', '租户后台首页标题', '0', '1', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('461', 'member_top_url', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '/manage/member/dashboard', '租户后台首页url', '租户后台首页url', '1', '1', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('462', 'auth_url', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', '', '服务器地址(URL)', '服务器地址(URL)', '9', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('463', 'qrcode_login', 'scan', 'radio', 'text', '50', '开启=>1\n关闭=>0', '1', '', '', '100', '200', '0', '开启扫码', '是否开启扫码', '8', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('464', 'mp_id', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', '', 'AppID(公众号)', '把服务器地址(URL)中$APPID$替换为你的AppID', '7', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('465', 'mp_secret', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', '', 'AppSecret(公众号)', 'AppSecret(公众号)', '6', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('466', 'mp_token', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', '', 'Token(公众号)', 'Token必须为英文或数字，长度为3-32字符。如不填写则默认为“TOKEN”', '5', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('467', 'mp_aes_key', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', '', 'EncodingAESKey', '公众号消息加密密钥由43位字符组成，可随机修改，字符范围为A-Z，a-z，0-9', '4', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('468', 'site_danxuan1', 'ceshi', 'radio', 'text', '31', '小明=>0\n小红=>2', '1', '', '', '100', '200', '0', '测试单选', '测试单选', '22', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('469', 'asd', 'ceshi', 'r_textarea', 'text', '31', '', '1', '', '', '100', '200', '<p>友情提示:<br><br>1、请打开微信公众号平台,网址:https://mp.weixin.qq.com,申请公众号必须是服务号。<br>2、登录->开发->基本配置->服务器配置(已启用)<br>3、帐号详情->功能设置->填写相关授权域名：(域名如：mall.buwangkeji.com)<br>4、设置正确后,前台会员支持扫码关注公众号登录管理。</p>', 'asd', 'asd', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3853', 'extract_money_min', 'extract_config', 'text', 'float', '55', '', '1', '', '', '100', '200', '10', '余额最低提现', '余额最低提现金额', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3854', 'extract_money_fee', 'extract_config', 'text', 'number', '55', '', '1', 'number|between:0,100', 'extract_money_fee.between=>数值只能在1-100之间', '100', '200', '0', '余额手续费', '余额提现手续费(百分比)', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3855', 'extract_desc', 'extract_config', 'r_textarea', 'text', '55', '', '1', '', '', '100', '200', '无', '提现说明', '提现说明', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3856', 'recharge_info', 'recharge_config', 'r_textarea', 'text', '54', '', '1', '', '', '100', '200', '<p><span><b>注意事项：</b></span></p><p>充值后帐户的金额不能提现，可用于商城消费使用</p><p>佣金导入账户之后不能再次导出、不可提现</p><p>账户充值出现问题可联系商城客服，也可拨打商城客服热线：4008888888</p>', '充值说明', '充值说明', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3857', 'ceshi_jianjie', 'zuhu_setting', 'textarea', 'text', '43', '', '1', '', '', '100', '200', '11', '测试简介', '测试简介', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3858', 'site_ceshi2', 'zuhu_setting', 'select', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试下拉2', '测试下拉2', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3859', 'site_xiala', 'zuhu_setting', 'select', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试下拉', '测试下拉', '10', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3860', 'site_danxuan', 'zuhu_setting', 'radio', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试单选', '测试单选', '20', '0', '0', 'member', '2', '2', null);
INSERT INTO `bw_sys_config` VALUES ('3861', 'ceshi_yanse', 'zuhu_setting', 'text', 'color', '43', '', '1', '', '', '100', '200', 'rgba(171, 59, 59, 1)', '测试颜色', 'haha', '12', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3862', 'site_fuxuan', 'zuhu_setting', 'checkbox', 'text', '43', '男=&gt;1\n女=&gt;2\n伪娘=&gt;3', '1', '', '', '100', '200', '1,2,3', '测试复选框', '测试复选框', '200', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3863', 'address_home', 'zuhu_setting', 'text', 'text', '43', '', '1', '', '', '100', '200', '家庭地址112233', '家庭地址', '家庭地址', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3864', 'mobile_zuhu', 'zuhu_setting', 'text', 'text', '43', '', '1', '', '', '100', '200', '15236108586', '手机号', '11位手机号', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3865', 'sitedanxuan', 'zu_hu_fen_lei', 'radio', 'text', '42', '甜=&gt;1\n咸=&gt;2', '1', '', '', '100', '200', '2', '测试租户单选框配置', '测试租户单选框配置', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3866', 'afadfsdf', 'zu_hu_fen_lei', 'text', 'text', '42', '', '1', '', '', '100', '200', '1111', '测试文本232', '测试文本232', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('3867', 'asdfadf', 'zu_hu_fen_lei', 'text', 'text', '42', '', '1', '', '', '100', '200', '33', '测试配置2', '测试配置2', '0', '0', '0', 'member', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('4529', 'sys_log_time', 'base', 'text', 'number', '35', '', '1', '', '', '100', '200', '604800', '系统日志清除时限', '系统日志清除时限（单位：秒，0为不清除）', '0', '0', '0', 'common', '2', '1', null);
INSERT INTO `bw_sys_config` VALUES ('4530', 'extract_money_min', 'extract_config', 'text', 'float', '55', '', '1', '', '', '100', '200', '10', '余额最低提现', '余额最低提现金额', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `bw_sys_config` VALUES ('4531', 'extract_money_fee', 'extract_config', 'text', 'number', '55', '', '1', 'number|between:0,100', 'extract_money_fee.between=>数值只能在1-100之间', '100', '200', '0', '余额手续费', '余额提现手续费(百分比)', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `bw_sys_config` VALUES ('4532', 'extract_desc', 'extract_config', 'r_textarea', 'text', '55', '', '1', '', '', '100', '200', '无', '提现说明', '提现说明', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `bw_sys_config` VALUES ('4533', 'recharge_info', 'recharge_config', 'r_textarea', 'text', '54', '', '1', '', '', '100', '200', '<p><span><b>注意事项：</b></span></p><p>充值后帐户的金额不能提现，可用于商城消费使用</p><p>佣金导入账户之后不能再次导出、不可提现</p><p>账户充值出现问题可联系商城客服，也可拨打商城客服热线：4008888888</p>', '充值说明', '充值说明', '0', '0', '64', 'member', '2', '1', '');

-- ----------------------------
-- Table structure for bw_sys_config_group
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_config_group`;
CREATE TABLE `bw_sys_config_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '数据组名称',
  `desc` varchar(256) NOT NULL DEFAULT '' COMMENT '数据说明',
  `config_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据字段',
  `fields` text COMMENT '数据组字段以及类型（json数据）',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COMMENT='分组数据字段表';

-- ----------------------------
-- Records of bw_sys_config_group
-- ----------------------------
INSERT INTO `bw_sys_config_group` VALUES ('85', '充值套餐选择', '充值套餐选择', 'recharge_select', '[{\"id\":\"5f65d41996bc7\",\"info\":\"售价\",\"config_name\":\"price\",\"desc\":\"售价\",\"must\":\"1\",\"input_type\":\"float\",\"rule\":\"\",\"message\":\"\",\"value\":\"1.0\",\"width\":\"100\",\"high\":\"200\",\"parameter\":\"\",\"upload_type\":\"1\",\"type\":\"text\"},{\"id\":\"5f65d4792346e\",\"info\":\"赠送\",\"config_name\":\"give\",\"desc\":\"赠送\",\"must\":\"2\",\"input_type\":\"float\",\"rule\":\"\",\"message\":\"\",\"value\":\"1.0\",\"width\":\"100\",\"high\":\"200\",\"parameter\":\"\",\"upload_type\":\"1\",\"type\":\"text\"}]', 'member', '');

-- ----------------------------
-- Table structure for bw_sys_config_group_data
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_config_group_data`;
CREATE TABLE `bw_sys_config_group_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据详情ID',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '对应的数据组id',
  `value` text NOT NULL COMMENT '数据字段对应的（json）数据值',
  `add_time` int(10) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态（1：开启；2：关闭；）',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户创建)',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `config_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据字段',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `group_id` (`group_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `member_id` (`member_id`),
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=3430 DEFAULT CHARSET=utf8 COMMENT='数据组合键值表';

-- ----------------------------
-- Records of bw_sys_config_group_data
-- ----------------------------
INSERT INTO `bw_sys_config_group_data` VALUES ('3416', '85', '{\"price\":{\"type\":\"float\",\"value\":\"20\"},\"give\":{\"type\":\"float\",\"value\":\"2\"}}', '1600509664', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3417', '85', '{\"price\":{\"type\":\"float\",\"value\":\"30\"},\"give\":{\"type\":\"float\",\"value\":\"3\"}}', '1600509671', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3418', '85', '{\"price\":{\"type\":\"float\",\"value\":\"50\"},\"give\":{\"type\":\"float\",\"value\":\"5\"}}', '1600509678', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3419', '85', '{\"price\":{\"type\":\"float\",\"value\":\"500\"},\"give\":{\"type\":\"float\",\"value\":\"50\"}}', '1600509706', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3420', '85', '{\"price\":{\"type\":\"float\",\"value\":\"800\"},\"give\":{\"type\":\"float\",\"value\":\"100\"}}', '1600509716', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3421', '85', '{\"price\":{\"type\":\"float\",\"value\":\"0.01\"},\"give\":{\"type\":\"float\",\"value\":\"0\"}}', '1600670096', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3422', '85', '{\"price\":{\"type\":\"float\",\"value\":\"1000\"},\"give\":{\"type\":\"float\",\"value\":\"300\"}}', '1601296083', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3423', '85', '{\"price\":{\"type\":\"float\",\"value\":\"20\"},\"give\":{\"type\":\"float\",\"value\":\"2\"}}', '1600509664', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3424', '85', '{\"price\":{\"type\":\"float\",\"value\":\"30\"},\"give\":{\"type\":\"float\",\"value\":\"3\"}}', '1600509671', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3425', '85', '{\"price\":{\"type\":\"float\",\"value\":\"50\"},\"give\":{\"type\":\"float\",\"value\":\"5\"}}', '1600509678', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3426', '85', '{\"price\":{\"type\":\"float\",\"value\":\"500\"},\"give\":{\"type\":\"float\",\"value\":\"50\"}}', '1600509706', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3427', '85', '{\"price\":{\"type\":\"float\",\"value\":\"800\"},\"give\":{\"type\":\"float\",\"value\":\"100\"}}', '1600509716', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3428', '85', '{\"price\":{\"type\":\"float\",\"value\":\"0.01\"},\"give\":{\"type\":\"float\",\"value\":\"0\"}}', '1600670096', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `bw_sys_config_group_data` VALUES ('3429', '85', '{\"price\":{\"type\":\"float\",\"value\":\"1000\"},\"give\":{\"type\":\"float\",\"value\":\"300\"}}', '1601296083', '0', '1', '64', 'member', 'recharge_select', '');

-- ----------------------------
-- Table structure for bw_sys_config_tab
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_config_tab`;
CREATE TABLE `bw_sys_config_tab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置分类id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类名称',
  `tab_name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类英文标识',
  `icon` varchar(30) DEFAULT NULL COMMENT '图标',
  `type` int(2) DEFAULT '0' COMMENT '配置类型',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '配置分类是否显示  0不显示，1显示',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `desc` text COMMENT '配置说明',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`),
  KEY `is_show` (`is_show`),
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='配置分类表';

-- ----------------------------
-- Records of bw_sys_config_tab
-- ----------------------------
INSERT INTO `bw_sys_config_tab` VALUES ('31', '测试配置', 'ceshi', 'fa fa-diamond       ', '1', '0', 'common', '0', '', null);
INSERT INTO `bw_sys_config_tab` VALUES ('32', '七牛云oss', 'qnoss', 'fa fa-folder-open-o', '1', '1', 'common', '10', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('34', '物流配置', 'express', 'fa fa-truck', '1', '1', 'common', '0', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('35', '基础配置', 'base', 'fa fa-cubes  ', '1', '1', 'common', '0', '', null);
INSERT INTO `bw_sys_config_tab` VALUES ('36', '微信开放平台配置', 'wechatopen', 'fa fa-wechat', '1', '1', 'common', '0', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('38', '站点配置', 'web_config', 'fa fa-server', '1', '1', 'common', '0', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('40', '阿里云市场API', 'aliapi', 'fa fa-cubes', '1', '1', 'common', '0', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('41', '微信支付', 'wechat_pay', 'fa fa-edit', '1', '1', 'common', '111', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('42', '测试租户分类', 'zu_hu_fen_lei', 'fa fa-fast-backward', '1', '1', 'member', '0', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('43', '租户信息设置', 'zuhu_setting', 'fa fa-diamond   ', '1', '1', 'member', '333', '', null);
INSERT INTO `bw_sys_config_tab` VALUES ('44', '阿里云oss', 'alioss', 'fa fa-server', '1', '1', 'common', '11', null, null);
INSERT INTO `bw_sys_config_tab` VALUES ('49', '测试分类有说明', 'site_shuoming', '  ', '1', '0', 'common', '0', '<h2>为什么要分页</h2><p>在<a href=\"https://blog.csdn.net/qq_37653144/article/details/82818191\">保护模式</a>中，内存访问使用分段机制——即\"段基址:段内偏移地址\"的方式，为加强段内存的安全性和可管理性还引入了<a href=\"https://blog.csdn.net/qq_37653144/article/details/82821540\">段描述符</a>的概念对段内存加强管理。但仅仅这样还是不够的，如果应用程序过多，或者内存碎片过多，又或者曾经被换出到硬盘的内存段需要再重新装载到内存，可内存中找不到合适大小的区域，要如何解决？12321</p>', null);
INSERT INTO `bw_sys_config_tab` VALUES ('50', '扫码登录', 'scan', 'fa fa-qrcode      ', '1', '1', 'common', '6', '<p>友情提示:<br><br>1、请打开微信公众号平台,网址:https://mp.weixin.qq.com,申请公众号必须是服务号。<br>2、登录-&gt;开发-&gt;基本配置-&gt;服务器配置(已启用)<br>3、帐号详情-&gt;功能设置-&gt;填写相关授权域名：(域名如：mall.buwangkeji.com)<br>4、设置正确后,前台会员支持扫码关注公众号登录管理。</p>', null);
INSERT INTO `bw_sys_config_tab` VALUES ('54', '充值配置', 'recharge_config', 'fa fa-diamond', '3', '1', 'member', '0', '', null);
INSERT INTO `bw_sys_config_tab` VALUES ('55', '提现配置', 'extract_config', 'fa fa-calculator', '1', '1', 'member', '0', '', null);

-- ----------------------------
-- Table structure for bw_sys_crud
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_crud`;
CREATE TABLE `bw_sys_crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `app_name` varchar(50) NOT NULL COMMENT '应用名称',
  `table_name` varchar(50) NOT NULL COMMENT '表名',
  `controller_name_diy` varchar(50) NOT NULL COMMENT '自定义控制器名',
  `model_name_diy` varchar(50) NOT NULL COMMENT '自定义模型名',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '状态:1=成功,0=失败',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4 COMMENT='一键生成表';

-- ----------------------------
-- Records of bw_sys_crud
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_express
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_express`;
CREATE TABLE `bw_sys_express` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '快递公司id',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户ID',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '快递公司编码',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '快递公司全称',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `is_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示{switch}(0:取消,1:展示)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`,`member_id`) USING BTREE,
  KEY `is_show` (`is_show`) USING BTREE,
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=849 DEFAULT CHARSET=utf8 COMMENT='快递公司表';

-- ----------------------------
-- Records of bw_sys_express
-- ----------------------------
INSERT INTO `bw_sys_express` VALUES ('1', '0', 'LIMINWL', '利民物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('2', '0', 'XINTIAN', '鑫天顺物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('3', '0', 'henglu', '恒路物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('4', '0', 'klwl', '康力物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('5', '0', 'meiguo', '美国快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('6', '0', 'a2u', 'A2U速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('7', '0', 'benteng', '奔腾物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('8', '0', 'ahdf', '德方物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('9', '0', 'timedg', '万家通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('10', '0', 'ztong', '智通物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('11', '0', 'xindan', '新蛋物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('12', '0', 'bgpyghx', '挂号信', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('13', '0', 'XFHONG', '鑫飞鸿物流快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('14', '0', 'ALP', '阿里物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('15', '0', 'BFWL', '滨发物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('16', '0', 'SJWL', '宋军物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('17', '0', 'SHUNFAWL', '顺发物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('18', '0', 'TIANHEWL', '天河物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('19', '0', 'YBWL', '邮联物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('20', '0', 'SWHY', '盛旺货运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('21', '0', 'TSWL', '汤氏物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('22', '0', 'YUANYUANWL', '圆圆物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('23', '0', 'BALIANGWL', '八梁物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('24', '0', 'ZGWL', '振刚物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('25', '0', 'JIAYU', '佳宇物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('26', '0', 'SHHX', '昊昕物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('27', '0', 'ande', '安得物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('28', '0', 'ppbyb', '贝邮宝', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('29', '0', 'dida', '递达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('30', '0', 'jppost', '日本邮政', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('31', '0', 'intmail', '中国邮政', '96', '0');
INSERT INTO `bw_sys_express` VALUES ('32', '0', 'HENGCHENGWL', '恒诚物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('33', '0', 'HENGFENGWL', '恒丰物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('34', '0', 'gdems', '广东ems快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('35', '0', 'xlyt', '祥龙运通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('36', '0', 'gjbg', '国际包裹', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('37', '0', 'uex', 'UEX', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('38', '0', 'singpost', '新加坡邮政', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('39', '0', 'guangdongyouzhengwuliu', '广东邮政', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('40', '0', 'bht', 'BHT', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('41', '0', 'cces', 'CCES快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('42', '0', 'cloudexpress', 'CE易欧通国际速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('43', '0', 'dasu', '达速物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('44', '0', 'pfcexpress', '皇家物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('45', '0', 'hjs', '猴急送', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('46', '0', 'huilian', '辉联物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('47', '0', 'huanqiu', '环球速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('48', '0', 'huada', '华达快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('49', '0', 'htwd', '华通务达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('50', '0', 'hipito', '海派通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('51', '0', 'hqtd', '环球通达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('52', '0', 'airgtc', '航空快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('53', '0', 'haoyoukuai', '好又快物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('54', '0', 'hanrun', '韩润物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('55', '0', 'ccd', '河南次晨达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('56', '0', 'hfwuxi', '和丰同城', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('57', '0', 'Sky', '荷兰', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('58', '0', 'hongxun', '鸿讯物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('59', '0', 'hongjie', '宏捷国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('60', '0', 'httx56', '汇通天下物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('61', '0', 'lqht', '恒通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('62', '0', 'jinguangsudikuaijian', '京广速递快件', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('63', '0', 'junfengguoji', '骏丰国际速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('64', '0', 'jiajiatong56', '佳家通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('65', '0', 'jrypex', '吉日优派', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('66', '0', 'jinchengwuliu', '锦程国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('67', '0', 'jgwl', '景光物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('68', '0', 'pzhjst', '急顺通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('69', '0', 'ruexp', '捷网俄全通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('70', '0', 'jmjss', '金马甲', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('71', '0', 'lanhu', '蓝弧快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('72', '0', 'ltexp', '乐天速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('73', '0', 'lutong', '鲁通快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('74', '0', 'ledii', '乐递供应链', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('75', '0', 'lundao', '论道国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('76', '0', 'mailikuaidi', '麦力快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('77', '0', 'mchy', '木春货运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('78', '0', 'meiquick', '美快国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('79', '0', 'valueway', '美通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('80', '0', 'nuoyaao', '偌亚奥国际', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('81', '0', 'euasia', '欧亚专线', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('82', '0', 'pca', '澳大利亚PCA快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('83', '0', 'pingandatengfei', '平安达腾飞', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('84', '0', 'pjbest', '品骏快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('85', '0', 'qbexpress', '秦邦快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('86', '0', 'quanxintong', '全信通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('87', '0', 'quansutong', '全速通国际快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('88', '0', 'qinyuan', '秦远物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('89', '0', 'qichen', '启辰国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('90', '0', 'quansu', '全速快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('91', '0', 'qzx56', '全之鑫物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('92', '0', 'qskdyxgs', '千顺快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('93', '0', 'runhengfeng', '全时速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('94', '0', 'rytsd', '日益通速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('95', '0', 'ruidaex', '瑞达国际速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('96', '0', 'shiyun', '世运快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('97', '0', 'sfift', '十方通物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('98', '0', 'stkd', '顺通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('99', '0', 'bgn', '布谷鸟快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('100', '0', 'jiahuier', '佳惠尔快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('101', '0', 'pingyou', '小包', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('102', '0', 'yumeijie', '誉美捷快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('103', '0', 'meilong', '美龙快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('104', '0', 'guangtong', '广通速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('105', '0', 'STARS', '星晨急便', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('106', '0', 'NANHANG', '中国南方航空股份有限公司', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('107', '0', 'lanbiao', '蓝镖快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('109', '0', 'baotongda', '宝通达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('110', '0', 'dashun', '大顺物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('111', '0', 'dada', '大达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('112', '0', 'fangfangda', '方方达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('113', '0', 'hebeijianhua', '河北建华物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('114', '0', 'haolaiyun', '好来运快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('115', '0', 'jinyue', '晋越快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('116', '0', 'kuaitao', '快淘快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('117', '0', 'peixing', '陪行物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('118', '0', 'hkpost', '香港邮政', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('119', '0', 'ytfh', '一统飞鸿快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('120', '0', 'zhongxinda', '中信达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('121', '0', 'zhongtian', '中天快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('122', '0', 'zuochuan', '佐川急便', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('123', '0', 'chengguang', '程光快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('124', '0', 'cszx', '城市之星', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('125', '0', 'chuanzhi', '传志快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('126', '0', 'feibao', '飞豹快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('127', '0', 'huiqiang', '汇强快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('128', '0', 'lejiedi', '乐捷递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('129', '0', 'lijisong', '成都立即送快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('130', '0', 'minbang', '民邦速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('131', '0', 'ocs', 'OCS国际快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('132', '0', 'santai', '三态速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('133', '0', 'saiaodi', '赛澳递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('134', '0', 'jd', '京东快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('135', '0', 'zengyi', '增益快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('136', '0', 'fanyu', '凡宇速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('137', '0', 'fengda', '丰达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('138', '0', 'coe', '东方快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('139', '0', 'ees', '百福东方快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('140', '0', 'disifang', '递四方速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('141', '0', 'rufeng', '如风达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('142', '0', 'changtong', '长通物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('143', '0', 'chengshi100', '城市100快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('144', '0', 'feibang', '飞邦物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('145', '0', 'haosheng', '昊盛物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('146', '0', 'yinsu', '音速速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('147', '0', 'kuanrong', '宽容物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('148', '0', 'tongcheng', '通成物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('149', '0', 'tonghe', '通和天下物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('150', '0', 'zhima', '芝麻开门', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('151', '0', 'ririshun', '日日顺物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('152', '0', 'anxun', '安迅物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('153', '0', 'baiqian', '百千诚国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('154', '0', 'chukouyi', '出口易', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('155', '0', 'diantong', '店通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('156', '0', 'dajin', '大金物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('157', '0', 'feite', '飞特物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('159', '0', 'gnxb', '国内小包', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('160', '0', 'huacheng', '华诚物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('161', '0', 'huahan', '华翰物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('162', '0', 'hengyu', '恒宇运通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('163', '0', 'huahang', '华航快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('164', '0', 'jiuyi', '久易快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('165', '0', 'jiete', '捷特快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('166', '0', 'jingshi', '京世物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('167', '0', 'kuayue', '跨越快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('168', '0', 'mengsu', '蒙速快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('169', '0', 'nanbei', '南北快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('171', '0', 'pinganda', '平安达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('172', '0', 'ruifeng', '瑞丰速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('173', '0', 'rongqing', '荣庆物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('174', '0', 'suijia', '穗佳物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('175', '0', 'simai', '思迈快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('176', '0', 'suteng', '速腾快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('177', '0', 'shengbang', '晟邦物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('178', '0', 'suchengzhaipei', '速呈宅配', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('179', '0', 'wuhuan', '五环速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('180', '0', 'xingchengzhaipei', '星程宅配', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('181', '0', 'yinjie', '顺捷丰达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('183', '0', 'yanwen', '燕文物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('184', '0', 'zongxing', '纵行物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('185', '0', 'aae', 'AAE快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('186', '0', 'dhl', 'DHL快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('187', '0', 'feihu', '飞狐快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('188', '0', 'shunfeng', '顺丰速运', '92', '1');
INSERT INTO `bw_sys_express` VALUES ('189', '0', 'spring', '春风物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('190', '0', 'yidatong', '易达通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('191', '0', 'PEWKEE', '彪记快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('192', '0', 'PHOENIXEXP', '凤凰快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('193', '0', 'CNGLS', 'GLS快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('194', '0', 'BHTEXP', '华慧快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('195', '0', 'B2B', '卡行天下', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('196', '0', 'PEISI', '配思货运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('197', '0', 'SUNDAPOST', '上大物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('198', '0', 'SUYUE', '苏粤货运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('199', '0', 'F5XM', '伍圆速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('200', '0', 'GZWENJIE', '文捷航空速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('201', '0', 'yuancheng', '远成物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('202', '0', 'dpex', 'DPEX快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('203', '0', 'anjie', '安捷快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('204', '0', 'jldt', '嘉里大通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('205', '0', 'yousu', '优速快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('206', '0', 'wanbo', '万博快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('207', '0', 'sure', '速尔物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('208', '0', 'sutong', '速通物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('209', '0', 'JUNCHUANWL', '骏川物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('210', '0', 'guada', '冠达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('211', '0', 'dsu', 'D速快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('212', '0', 'LONGSHENWL', '龙胜物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('213', '0', 'abc', '爱彼西快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('214', '0', 'eyoubao', 'E邮宝', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('215', '0', 'aol', 'AOL快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('216', '0', 'jixianda', '急先达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('217', '0', 'haihong', '山东海红快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('218', '0', 'feiyang', '飞洋快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('219', '0', 'rpx', 'RPX保时达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('220', '0', 'zhaijisong', '宅急送', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('221', '0', 'tiantian', '天天快递', '99', '0');
INSERT INTO `bw_sys_express` VALUES ('222', '0', 'yunwuliu', '云物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('223', '0', 'jiuye', '九曳供应链', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('224', '0', 'bsky', '百世快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('225', '0', 'higo', '黑狗物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('226', '0', 'arke', '方舟速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('227', '0', 'zwsy', '中外速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('228', '0', 'jxy', '吉祥邮', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('229', '0', 'aramex', 'Aramex', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('230', '0', 'guotong', '国通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('231', '0', 'jiayi', '佳怡物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('232', '0', 'longbang', '龙邦快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('233', '0', 'minhang', '民航快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('234', '0', 'quanyi', '全一快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('235', '0', 'quanchen', '全晨快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('236', '0', 'usps', 'USPS快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('237', '0', 'xinbang', '新邦物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('238', '0', 'yuanzhi', '元智捷诚快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('239', '0', 'zhongyou', '中邮物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('240', '0', 'yuxin', '宇鑫物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('241', '0', 'cnpex', '中环快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('242', '0', 'shengfeng', '盛丰物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('243', '0', 'yuantong', '圆通速递', '97', '1');
INSERT INTO `bw_sys_express` VALUES ('244', '0', 'jiayunmei', '加运美物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('245', '0', 'ywfex', '源伟丰快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('246', '0', 'xinfeng', '信丰物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('247', '0', 'wanxiang', '万象物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('248', '0', 'menduimen', '门对门', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('249', '0', 'mingliang', '明亮物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('250', '0', 'fengxingtianxia', '风行天下', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('251', '0', 'gongsuda', '共速达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('252', '0', 'zhongtong', '中通快递', '100', '1');
INSERT INTO `bw_sys_express` VALUES ('253', '0', 'quanritong', '全日通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('254', '0', 'ems', 'EMS', '1', '1');
INSERT INTO `bw_sys_express` VALUES ('255', '0', 'wanjia', '万家物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('256', '0', 'yuntong', '运通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('257', '0', 'feikuaida', '飞快达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('258', '0', 'haimeng', '海盟速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('259', '0', 'zhongsukuaidi', '中速快件', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('260', '0', 'yuefeng', '越丰快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('261', '0', 'shenghui', '盛辉物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('262', '0', 'datian', '大田物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('263', '0', 'quanjitong', '全际通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('264', '0', 'longlangkuaidi', '隆浪快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('265', '0', 'neweggozzo', '新蛋奥硕物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('266', '0', 'shentong', '申通快递', '95', '1');
INSERT INTO `bw_sys_express` VALUES ('267', '0', 'haiwaihuanqiu', '海外环球', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('268', '0', 'yad', '源安达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('269', '0', 'jindawuliu', '金大物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('270', '0', 'sevendays', '七天连锁', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('271', '0', 'tnt', 'TNT快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('272', '0', 'huayu', '天地华宇物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('273', '0', 'lianhaotong', '联昊通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('274', '0', 'nengda', '港中能达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('275', '0', 'LBWL', '联邦物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('276', '0', 'ontrac', 'onTrac', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('277', '0', 'feihang', '原飞航快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('278', '0', 'bangsongwuliu', '邦送物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('279', '0', 'huaxialong', '华夏龙物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('280', '0', 'ztwy', '中天万运快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('281', '0', 'fkd', '飞康达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('282', '0', 'anxinda', '安信达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('283', '0', 'quanfeng', '全峰快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('284', '0', 'shengan', '圣安物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('285', '0', 'jiaji', '佳吉物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('286', '0', 'yunda', '韵达快运', '94', '0');
INSERT INTO `bw_sys_express` VALUES ('287', '0', 'ups', 'UPS快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('288', '0', 'debang', '德邦物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('289', '0', 'yafeng', '亚风速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('290', '0', 'kuaijie', '快捷速递', '98', '0');
INSERT INTO `bw_sys_express` VALUES ('291', '0', 'huitong', '百世快递', '93', '0');
INSERT INTO `bw_sys_express` VALUES ('293', '0', 'aolau', 'AOL澳通速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('294', '0', 'anneng', '安能物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('295', '0', 'auexpress', '澳邮中国快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('296', '0', 'exfresh', '安鲜达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('297', '0', 'bcwelt', 'BCWELT', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('298', '0', 'youzhengguonei', '挂号信', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('299', '0', 'xiaohongmao', '北青小红帽', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('300', '0', 'lbbk', '宝凯物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('301', '0', 'byht', '博源恒通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('302', '0', 'idada', '百成大达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('303', '0', 'baitengwuliu', '百腾物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('304', '0', 'birdex', '笨鸟海淘', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('305', '0', 'bsht', '百事亨通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('306', '0', 'dayang', '大洋物流快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('307', '0', 'dechuangwuliu', '德创物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('308', '0', 'donghanwl', '东瀚物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('309', '0', 'dfpost', '达方物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('310', '0', 'dongjun', '东骏快捷物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('311', '0', 'dindon', '叮咚澳洲转运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('312', '0', 'dazhong', '大众佐川急便', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('313', '0', 'decnlh', '德中快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('314', '0', 'dekuncn', '德坤供应链', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('315', '0', 'eshunda', '俄顺达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('316', '0', 'ewe', 'EWE全球快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('317', '0', 'fedexuk', 'FedEx英国', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('318', '0', 'fox', 'FOX国际速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('319', '0', 'rufengda', '凡客如风达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('320', '0', 'fandaguoji', '颿达国际快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('321', '0', 'hnfy', '飞鹰物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('322', '0', 'flysman', '飞力士物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('323', '0', 'sccod', '丰程物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('324', '0', 'farlogistis', '泛远国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('325', '0', 'gsm', 'GSM', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('326', '0', 'gaticn', 'GATI快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('327', '0', 'gts', 'GTS快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('328', '0', 'gangkuai', '港快速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('329', '0', 'gtsd', '高铁速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('330', '0', 'tiandihuayu', '华宇物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('331', '0', 'huangmajia', '黄马甲快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('332', '0', 'ucs', '合众速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('333', '0', 'huoban', '伙伴物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('334', '0', 'nedahm', '红马速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('335', '0', 'huiwen', '汇文配送', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('336', '0', 'nmhuahe', '华赫物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('337', '0', 'hangyu', '航宇快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('338', '0', 'minsheng', '闽盛物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('339', '0', 'riyu', '日昱物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('340', '0', 'sxhongmajia', '山西红马甲', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('341', '0', 'syjiahuier', '沈阳佳惠尔', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('342', '0', 'shlindao', '上海林道货运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('343', '0', 'shunjiefengda', '顺捷丰达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('344', '0', 'subida', '速必达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('345', '0', 'bphchina', '速方国际物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('346', '0', 'sendtochina', '速递中国', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('347', '0', 'suning', '苏宁快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('348', '0', 'sihaiet', '四海快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('349', '0', 'tianzong', '天纵物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('350', '0', 'chinatzx', '同舟行物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('351', '0', 'nntengda', '腾达速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('352', '0', 'sd138', '泰国138', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('353', '0', 'tongdaxing', '通达兴物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('354', '0', 'tlky', '天联快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('355', '0', 'youshuwuliu', 'UC优速快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('356', '0', 'ueq', 'UEQ快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('357', '0', 'weitepai', '微特派快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('358', '0', 'wtdchina', '威时沛运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('359', '0', 'wzhaunyun', '微转运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('360', '0', 'gswtkd', '万通快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('361', '0', 'wotu', '渥途国际速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('362', '0', 'xiyoute', '希优特快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('363', '0', 'xilaikd', '喜来快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('364', '0', 'xsrd', '鑫世锐达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('365', '0', 'xtb', '鑫通宝物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('366', '0', 'xintianjie', '信天捷快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('367', '0', 'xaetc', '西安胜峰', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('368', '0', 'xianfeng', '先锋快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('369', '0', 'sunspeedy', '新速航', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('370', '0', 'xipost', '西邮寄', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('371', '0', 'sinatone', '信联通', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('372', '0', 'sunjex', '新杰物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('373', '0', 'yundaexus', '韵达美国件', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('374', '0', 'yxwl', '宇鑫物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('375', '0', 'yitongda', '易通达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('376', '0', 'yiqiguojiwuliu', '一柒物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('377', '0', 'yilingsuyun', '亿领速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('378', '0', 'yujiawuliu', '煜嘉物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('379', '0', 'gml', '英脉物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('380', '0', 'leopard', '云豹国际货运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('381', '0', 'czwlyn', '云南中诚', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('382', '0', 'sdyoupei', '优配速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('383', '0', 'yongchang', '永昌物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('384', '0', 'yufeng', '御风速运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('385', '0', 'yamaxunwuliu', '亚马逊物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('386', '0', 'yousutongda', '优速通达', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('387', '0', 'yishunhang', '亿顺航', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('388', '0', 'yongwangda', '永旺达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('389', '0', 'ecmscn', '易满客', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('390', '0', 'yingchao', '英超物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('391', '0', 'edlogistics', '益递物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('392', '0', 'yyexpress', '远洋国际', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('393', '0', 'onehcang', '一号仓', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('394', '0', 'ycgky', '远成快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('395', '0', 'lineone', '一号线', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('396', '0', 'ypsd', '壹品速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('397', '0', 'vipexpress', '鹰运国际速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('398', '0', 'el56', '易联通达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('399', '0', 'yyqc56', '一运全成物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('400', '0', 'zhongtie', '中铁快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('401', '0', 'ZTKY', '中铁物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('402', '0', 'zzjh', '郑州建华快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('403', '0', 'zhongruisudi', '中睿速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('404', '0', 'zhongwaiyun', '中外运速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('405', '0', 'zengyisudi', '增益速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('406', '0', 'sujievip', '郑州速捷', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('407', '0', 'zhichengtongda', '至诚通达快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('408', '0', 'zhdwl', '众辉达物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('409', '0', 'kuachangwuliu', '直邮易', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('410', '0', 'topspeedex', '中运全速', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('411', '0', 'otobv', '中欧快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('412', '0', 'zsky123', '准实快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('413', '0', 'donghong', '东红物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('414', '0', 'kuaiyouda', '快优达速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('415', '0', 'balunzhi', '巴伦支快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('416', '0', 'hutongwuliu', '户通物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('417', '0', 'xianchenglian', '西安城联速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('418', '0', 'youbijia', '邮必佳', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('419', '0', 'feiyuan', '飞远物流', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('420', '0', 'chengji', '城际速递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('421', '0', 'huaqi', '华企快运', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('422', '0', 'yibang', '一邦快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('423', '0', 'citylink', 'CityLink快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('424', '0', 'meixi', '美西快递', '1', '0');
INSERT INTO `bw_sys_express` VALUES ('425', '0', 'acs', 'ACS', '1', '0');

-- ----------------------------
-- Table structure for bw_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_log`;
CREATE TABLE `bw_sys_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员操作记录ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '访问者',
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT '账号',
  `path` varchar(1000) NOT NULL DEFAULT '' COMMENT '链接',
  `menu_path` varchar(1000) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '路由地址',
  `method` varchar(255) NOT NULL DEFAULT '' COMMENT '访问类型',
  `param` text COMMENT '参数',
  `ip` varchar(16) NOT NULL DEFAULT '' COMMENT '登录IP',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间{date}',
  `scopes` varchar(30) NOT NULL DEFAULT 'admin' COMMENT '范围{radio}(admin:总后台,member:租户后台,mini_program:小程序,h5:h5,app:app,official:公众号)',
  `city_name` varchar(255) NOT NULL DEFAULT '' COMMENT '城市名',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE,
  KEY `add_time` (`add_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21165 DEFAULT CHARSET=utf8 COMMENT='管理员操作记录表';

-- ----------------------------
-- Records of bw_sys_log
-- ----------------------------
INSERT INTO `bw_sys_log` VALUES ('21109', '1', '0', '0', 'Admin', 'admin', '/manage/Config/showconfig', '/manage/config/showconfig', '配置设置', '[]', '127.0.0.1', '1604390985', 'admin', '');
INSERT INTO `bw_sys_log` VALUES ('21110', '1', '0', '0', 'Admin', 'admin', '/manage/Config/showconfig', '/manage/config/showconfig', '配置设置', '[]', '127.0.0.1', '1604391904', 'admin', '');
INSERT INTO `bw_sys_log` VALUES ('21111', '1', '0', '0', 'Admin', 'admin', '/manage/publicCommon/upload', '/manage/publicCommon/upload.html?rule=fileExt%3Ajpg%2Cjpeg%2Cpng%2Cgif%2Cpem%2Csql%2Ctxt%2Chtml%2Cjs', '', '{\"rule\":\"fileExt:jpg,jpeg,png,gif,pem,sql,txt,html,js,css,php\"}', '127.0.0.1', '1604391927', 'admin', '');
INSERT INTO `bw_sys_log` VALUES ('21112', '1', '0', '0', 'Admin', 'admin', '/manage/publicCommon/upload', '/manage/publicCommon/upload.html?rule=fileExt%3Ajpg%2Cjpeg%2Cpng%2Cgif%2Cpem%2Csql%2Ctxt%2Chtml%2Cjs', '', '{\"rule\":\"fileExt:jpg,jpeg,png,gif,pem,sql,txt,html,js,css,php\"}', '127.0.0.1', '1604391927', 'admin', '');
INSERT INTO `bw_sys_log` VALUES ('21113', '1', '0', '0', 'Admin', 'admin', '/manage/Config/setValues', '/manage/Config/setValues.html', '', '{\"wechat_app_id\":\"\",\"wechat_mch_id\":\"\",\"wechat_key\":\"\",\"wechat_cert_path\":\"{\\\"name\\\":\\\"\\u900f\\u660e.png\\\",\\\"size\\\":172692,\\\"type\\\":\\\"image\\/png\\\",\\\"src\\\":\\\"http:\\/\\/www.bwsaas.com\\/upload\\/20201103\\\\\\\\4848bd71e499aacb95990527c365d45d.png\\\"}\",\"file\":\"\",\"wechat_key_path\":\"{\\\"name\\\":\\\"\\u4e0d\\u900f\\u660e.png\\\",\\\"size\\\":89483,\\\"type\\\":\\\"image\\/png\\\",\\\"src\\\":\\\"http:\\/\\/www.bwsaas.com\\/upload\\/20201103\\\\\\\\7242b6b8e8ebb90f6d39e1123e9a6b0d.png\\\"}\",\"alioss_access_key_id\":\"\",\"alioss_access_key_secret\":\"\",\"alioss_endpoint\":\"\",\"alioss_bucket\":\"\",\"alioss_domain\":\"\",\"qnoss_secret_key\":\"\",\"qnoss_access_key\":\"\",\"qnoss_bucket\":\"\",\"qnoss_domain\":\"\",\"auth_url\":\"\",\"qrcode_login\":\"0\",\"mp_id\":\"\",\"mp_secret\":\"\",\"mp_token\":\"\",\"mp_aes_key\":\"\",\"price\":\"0.1\",\"AppCode\":\"\",\"web_name\":\"\\u5e03\\u7f51\\u79d1\\u6280saas\",\"web_title\":\"\\u5e03\\u7f51\\u79d1\\u6280saas\",\"web_url\":\"https:\\/\\/mall.buwangkeji.com\",\"web_logo\":\"{\\\"name\\\":\\\"\\u4f01\\u4e1a\\u5fae\\u4fe1\\u622a\\u56fe_20200312111356.png\\\",\\\"size\\\":1917,\\\"type\\\":\\\"image\\/png\\\",\\\"src\\\":\\\"http:\\/\\/mall.buwangkeji.com\\/\\/uploads\\/upload\\/20200812\\/bc7fbc44508cc7fb24854763fb8f7d1d.png\\\"}\",\"web_keywords\":\"\\u5e03\\u7f51\\u79d1\\u6280\",\"web_description\":\"\\u5e03\\u7f51\\u79d1\\u6280\",\"web_icp\":\"\\u8c6bICP\\u590718042703\\u53f7-1\",\"web_contacts\":\"15098745674\",\"web_address\":\"\\u4e1c\\u65b9\\u7ecf\\u51781\",\"copyright\":\"\\u00a9\\u7248\\u6743\\u6240\\u6709 2015-2020 \\u5e03\\u7f51\\u79d1\\u6280\",\"aes_key\":\"\",\"msg_event_url\":\"\",\"auth_event_url\":\"\",\"token\":\"\",\"secret\":\"\",\"app_id\":\"\",\"upload_allow_ext\":\"doc,gif,ico,icon,jpg,mp3,mp4,p12,pem,png,rar,jpeg,7z,pdf\",\"upload_allow_size\":\"1024000\",\"sys_log_time\":\"604800\",\"upload_type\":\"local\",\"base_login_number\":\"259200\",\"base_login_mode\":\"1\",\"express_secret_key\":\"\"}', '127.0.0.1', '1604391958', 'admin', '');

-- ----------------------------
-- Table structure for bw_sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_notice`;
CREATE TABLE `bw_sys_notice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content` text NOT NULL COMMENT '内容{editor}',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态{switch}(0:关闭1:开启)',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间{date}',
  `title` varchar(255) NOT NULL COMMENT '标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='系统公告';

-- ----------------------------
-- Records of bw_sys_notice
-- ----------------------------
INSERT INTO `bw_sys_notice` VALUES ('4', '<p>欢迎使用我们的sass系统，已新上线商城应用，分为小程序端和公众号端，有需要的租户可以去安装使用！！！</p>\n', '1', '1601013652', '已新上线商城应用，分为小程序端和公众号端，有需要的租户可以去安装使用');
INSERT INTO `bw_sys_notice` VALUES ('5', '<p>布网云开源sass系统，欢迎大家使用</p>\n', '1', '1600963200', '布网云开源sass系统，欢迎大家使用');
INSERT INTO `bw_sys_notice` VALUES ('8', '<p>欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！欢迎新老租户使用本系统！！！</p>\n', '1', '1600963200', '欢迎新老租户使用本系统！！！');
INSERT INTO `bw_sys_notice` VALUES ('9', '<p>新版本v1.1.0上线了</p>\n', '1', '1603872244', '新版本v1.1.0上线了');

-- ----------------------------
-- Table structure for bw_sys_region
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_region`;
CREATE TABLE `bw_sys_region` (
  `id` int(6) NOT NULL COMMENT '序号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `pid` int(6) NOT NULL COMMENT '上级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='城市列表';

-- ----------------------------
-- Records of bw_sys_region
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `bw_sys_uploadfile`;
CREATE TABLE `bw_sys_uploadfile` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `upload_type` varchar(20) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `original_name` varchar(255) DEFAULT NULL COMMENT '文件原名',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `image_width` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `image_height` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `image_type` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `image_frames` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `mime_type` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_ext` varchar(100) DEFAULT NULL,
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(10) DEFAULT NULL COMMENT '创建日期',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  `upload_time` int(10) DEFAULT NULL COMMENT '上传时间',
  `member_id` int(10) NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `upload_type` (`upload_type`),
  KEY `original_name` (`original_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1476 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='上传文件表';

-- ----------------------------
-- Records of bw_sys_uploadfile
-- ----------------------------

-- ----------------------------
-- Table structure for bw_system_config
-- ----------------------------
DROP TABLE IF EXISTS `bw_system_config`;
CREATE TABLE `bw_system_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(20) DEFAULT '' COMMENT '分类',
  `name` varchar(100) DEFAULT '' COMMENT '配置名',
  `value` varchar(500) DEFAULT '' COMMENT '配置值',
  PRIMARY KEY (`id`),
  KEY `idx_system_config_type` (`type`) USING BTREE,
  KEY `idx_system_config_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COMMENT='系统-配置';

-- ----------------------------
-- Records of bw_system_config
-- ----------------------------
INSERT INTO `bw_system_config` VALUES ('1', 'base', 'site_copy', '©版权所有 2019-2020 楚才科技');
INSERT INTO `bw_system_config` VALUES ('2', 'base', 'site_name', 'ThinkAdmin');
INSERT INTO `bw_system_config` VALUES ('3', 'base', 'site_icon', 'https://v6.thinkadmin.top/upload/f47b8fe06e38ae99/08e8398da45583b9.png');
INSERT INTO `bw_system_config` VALUES ('4', 'base', 'site_copy', '©版权所有 2019-2020 楚才科技');
INSERT INTO `bw_system_config` VALUES ('5', 'base', 'app_name', 'ThinkAdmin');
INSERT INTO `bw_system_config` VALUES ('6', 'base', 'app_version', 'v6.0');
INSERT INTO `bw_system_config` VALUES ('7', 'base', 'miitbeian', '粤ICP备16006642号-2');
INSERT INTO `bw_system_config` VALUES ('8', 'storage', 'qiniu_http_protocol', 'http');
INSERT INTO `bw_system_config` VALUES ('9', 'storage', 'type', 'local');
INSERT INTO `bw_system_config` VALUES ('10', 'storage', 'allow_exts', 'doc,gif,icon,jpg,mp3,mp4,p12,pem,png,rar,xls,xlsx');
INSERT INTO `bw_system_config` VALUES ('11', 'storage', 'qiniu_region', '华东');
INSERT INTO `bw_system_config` VALUES ('12', 'storage', 'qiniu_bucket', '');
INSERT INTO `bw_system_config` VALUES ('13', 'storage', 'qiniu_http_domain', '');
INSERT INTO `bw_system_config` VALUES ('14', 'storage', 'qiniu_access_key', '');
INSERT INTO `bw_system_config` VALUES ('15', 'storage', 'qiniu_secret_key', '');
INSERT INTO `bw_system_config` VALUES ('16', 'wechat', 'type', 'api');
INSERT INTO `bw_system_config` VALUES ('17', 'wechat', 'token', '');
INSERT INTO `bw_system_config` VALUES ('18', 'wechat', 'appid', '');
INSERT INTO `bw_system_config` VALUES ('19', 'wechat', 'appsecret', '');
INSERT INTO `bw_system_config` VALUES ('20', 'wechat', 'encodingaeskey', '');
INSERT INTO `bw_system_config` VALUES ('21', 'wechat', 'thr_appid', '');
INSERT INTO `bw_system_config` VALUES ('22', 'wechat', 'thr_appkey', '');
INSERT INTO `bw_system_config` VALUES ('23', 'wechat', 'mch_id', '');
INSERT INTO `bw_system_config` VALUES ('24', 'wechat', 'mch_key', '');
INSERT INTO `bw_system_config` VALUES ('25', 'wechat', 'mch_ssl_type', 'pem');
INSERT INTO `bw_system_config` VALUES ('26', 'wechat', 'mch_ssl_p12', '');
INSERT INTO `bw_system_config` VALUES ('27', 'wechat', 'mch_ssl_key', '');
INSERT INTO `bw_system_config` VALUES ('28', 'wechat', 'mch_ssl_cer', '');
INSERT INTO `bw_system_config` VALUES ('29', 'storage', 'alioss_http_protocol', 'http');
INSERT INTO `bw_system_config` VALUES ('30', 'storage', 'alioss_point', 'oss-cn-hangzhou.aliyuncs.com');
INSERT INTO `bw_system_config` VALUES ('31', 'storage', 'alioss_bucket', '');
INSERT INTO `bw_system_config` VALUES ('32', 'storage', 'alioss_http_domain', '');
INSERT INTO `bw_system_config` VALUES ('33', 'storage', 'alioss_access_key', '');
INSERT INTO `bw_system_config` VALUES ('34', 'storage', 'alioss_secret_key', '');

-- ----------------------------
-- Table structure for bw_token
-- ----------------------------
DROP TABLE IF EXISTS `bw_token`;
CREATE TABLE `bw_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text NOT NULL,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `expires_time` int(11) NOT NULL COMMENT '到期时间',
  `login_ip` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT '登录ip',
  `scopes` varchar(100) DEFAULT NULL COMMENT '登录类型',
  PRIMARY KEY (`id`),
  KEY `uid` (`user_id`),
  KEY `login_ip` (`login_ip`),
  KEY `scopes` (`scopes`),
  KEY `create_time` (`create_time`),
  KEY `expires_time` (`expires_time`)
) ENGINE=InnoDB AUTO_INCREMENT=374 DEFAULT CHARSET=utf8mb4 COMMENT='token表';

-- ----------------------------
-- Records of bw_token
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user
-- ----------------------------
DROP TABLE IF EXISTS `bw_user`;
CREATE TABLE `bw_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `invite_code` varchar(50) DEFAULT '0',
  `member_miniapp_id` int(11) DEFAULT '0' COMMENT '应用',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `mobile` varchar(20) DEFAULT '' COMMENT '手机号',
  `unionid` varchar(32) DEFAULT '' COMMENT '开放平台unionid',
  `miniapp_uid` varchar(32) DEFAULT '' COMMENT 'openid',
  `official_uid` varchar(32) DEFAULT '' COMMENT '公众号用户openid',
  `session_key` varchar(255) DEFAULT '',
  `safe_password` varchar(255) DEFAULT '' COMMENT '支付密码',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像{image}',
  `login_ip` varchar(20) DEFAULT '' COMMENT '登录IP',
  `login_time` int(11) unsigned zerofill DEFAULT '00000000000' COMMENT '登录时间{date}',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态{switch}(0:锁定,1:正常)',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除{switch}(0:正常,1:删除)',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间{date}',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间{date}',
  `integral` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '积分',
  `money` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '用户余额',
  `brokerage` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '佣金',
  `username` varchar(50) DEFAULT '' COMMENT '用户名',
  PRIMARY KEY (`id`),
  KEY `CODE` (`invite_code`),
  KEY `MINIAPP_ID` (`member_miniapp_id`),
  KEY `WECHAT_ID` (`miniapp_uid`),
  KEY `OPEN_ID` (`official_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COMMENT='APP用户表';

-- ----------------------------
-- Records of bw_user
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_address
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_address`;
CREATE TABLE `bw_user_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户地址id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `real_name` varchar(32) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `mobile` varchar(16) NOT NULL DEFAULT '' COMMENT '收货人电话',
  `province` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在省',
  `city` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在市',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `city_code` int(11) NOT NULL DEFAULT '0' COMMENT '城市表city_id',
  `district` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在区',
  `detail` varchar(256) NOT NULL DEFAULT '' COMMENT '收货人详细地址',
  `post_code` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '邮编',
  `longitude` varchar(16) NOT NULL DEFAULT '0' COMMENT '经度',
  `latitude` varchar(16) NOT NULL DEFAULT '0' COMMENT '纬度',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `member_miniapp_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`user_id`) USING BTREE,
  KEY `is_default` (`is_default`) USING BTREE,
  KEY `is_del` (`is_del`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='用户地址表';

-- ----------------------------
-- Records of bw_user_address
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_bill
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_bill`;
CREATE TABLE `bw_user_bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户账单id',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `link_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '关联id',
  `pm` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '收支{radio}(0:支出,1:获得)',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '账单标题',
  `category` varchar(64) NOT NULL DEFAULT '' COMMENT '明细币种{radio}{radio}(money:余额,integral:积分,brokerage:佣金)',
  `type` varchar(64) NOT NULL DEFAULT '' COMMENT '明细类型{radio}(money:余额,integral:积分,brokerage:佣金)',
  `number` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '明细数字',
  `balance` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '剩余',
  `mark` varchar(512) NOT NULL DEFAULT '' COMMENT '备注',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间{date}',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:待确认,1:有效,-1:无效)',
  `take` tinyint(1) NOT NULL DEFAULT '0' COMMENT '收货状态{radio}(0:未收货,1:已收货)',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `openid` (`uid`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `add_time` (`add_time`) USING BTREE,
  KEY `pm` (`pm`) USING BTREE,
  KEY `type` (`category`,`type`,`link_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=392 DEFAULT CHARSET=utf8 COMMENT='用户账单表';

-- ----------------------------
-- Records of bw_user_bill
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_extract
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_extract`;
CREATE TABLE `bw_user_extract` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` text NOT NULL COMMENT '用户ID',
  `bank_name` varchar(60) DEFAULT NULL COMMENT '银行名称',
  `bank_username` varchar(60) DEFAULT NULL COMMENT '银行收款人姓名',
  `bank_zone` varchar(100) DEFAULT NULL COMMENT '开户地址',
  `bank_detail` varchar(100) DEFAULT NULL COMMENT '详细开户地址',
  `bank_card` varchar(255) DEFAULT '' COMMENT '银行卡号',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `wx_name` varchar(50) DEFAULT NULL COMMENT '微信账户人姓名',
  `wxImag` varchar(300) DEFAULT NULL COMMENT '微信收款码{image}',
  `ali_name` varchar(50) DEFAULT NULL COMMENT '阿里账户人姓名',
  `ali_account` varchar(100) DEFAULT NULL COMMENT '支付宝账号',
  `aliImag` varchar(300) DEFAULT NULL COMMENT '支付宝收款码{image}',
  `add_time` int(11) NOT NULL COMMENT '添加时间{date}',
  `deletetime` int(11) DEFAULT '0' COMMENT '删除时间{date}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户提现表';

-- ----------------------------
-- Records of bw_user_extract
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_extract_log
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_extract_log`;
CREATE TABLE `bw_user_extract_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `real_name` varchar(64) DEFAULT NULL COMMENT '提现人姓名',
  `bank_name` varchar(60) DEFAULT NULL COMMENT '银行名称',
  `bank_zone` varchar(100) DEFAULT NULL COMMENT '开户地址',
  `bank_detail` varchar(100) DEFAULT NULL COMMENT '详细开户地址',
  `bank_card` varchar(255) DEFAULT '' COMMENT '银行卡号',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `wxImag` varchar(300) DEFAULT NULL COMMENT '微信收款码{image}',
  `aliImag` varchar(300) DEFAULT NULL COMMENT '支付宝收款码{image}',
  `ali_account` varchar(100) DEFAULT NULL COMMENT '支付宝账号',
  `extract_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现金额',
  `fee` decimal(8,2) unsigned DEFAULT NULL COMMENT '手续费',
  `before` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现前数额',
  `after` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现后数额',
  `mark` varchar(512) DEFAULT NULL COMMENT '备注',
  `extract_type` enum('bank','alipay','wx') DEFAULT 'bank' COMMENT '提现方式{radio}(bank:银行卡,alipa:支付宝,wx:微信)',
  `type` enum('money') DEFAULT 'money' COMMENT '提现类型{radio}(money:余额)',
  `status` enum('-1','0','1') DEFAULT '0' COMMENT '提现状态{radio}(-1:未通过,0:审核中,1:已提现)',
  `fail_msg` varchar(128) DEFAULT NULL COMMENT '提现失败原因',
  `updatetime` int(10) unsigned DEFAULT NULL COMMENT '更新时间{date}',
  `createtime` int(10) unsigned DEFAULT NULL COMMENT '添加时间{date}',
  `deletetime` int(10) DEFAULT NULL COMMENT '删除时间{date}',
  `return_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '退款金额',
  PRIMARY KEY (`id`),
  KEY `extract_type` (`extract_type`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `add_time` (`createtime`) USING BTREE,
  KEY `openid` (`user_id`) USING BTREE,
  KEY `fail_time` (`updatetime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1628 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户提现记录表';

-- ----------------------------
-- Records of bw_user_extract_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_group
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_group`;
CREATE TABLE `bw_user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父角色ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态0:禁用1:正常',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `status` (`status`),
  KEY `pid` (`pid`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of bw_user_group
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_group_access
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_group_access`;
CREATE TABLE `bw_user_group_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL COMMENT '管理员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='权限分组表';

-- ----------------------------
-- Records of bw_user_group_access
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_group_node
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_group_node`;
CREATE TABLE `bw_user_group_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL COMMENT '角色组ID',
  `node_id` int(10) unsigned NOT NULL COMMENT '权限节点ID',
  `node_name` varchar(100) NOT NULL DEFAULT '' COMMENT '节点规则唯一英文标识,全小写',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类,请加应用前缀,如admin_',
  PRIMARY KEY (`id`),
  KEY `role_id` (`group_id`),
  KEY `rule_name` (`node_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COMMENT='权限授权表';

-- ----------------------------
-- Records of bw_user_group_node
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_node
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_node`;
CREATE TABLE `bw_user_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `app_name` varchar(40) NOT NULL DEFAULT '' COMMENT '规则所属应用名字或插件名字，如manage, user ,api,home',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如system,plugin,app',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) NOT NULL DEFAULT '' COMMENT '额外url参数',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述标题',
  `condition` varchar(200) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否菜单',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父ID',
  `path` varchar(100) NOT NULL DEFAULT '' COMMENT '对应的前端路由菜单',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `module` (`app_name`,`status`,`type`),
  KEY `ismenu` (`ismenu`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8mb4 COMMENT='权限规则表';

-- ----------------------------
-- Records of bw_user_node
-- ----------------------------
INSERT INTO `bw_user_node` VALUES ('1', '1', '', 'system', '/user', '', '会员管理', '', '1', '', '', '0', '', '0', '0');
INSERT INTO `bw_user_node` VALUES ('2', '1', '', 'system', '/user/index', '', '会员管理', '', '1', '', '', '1', '', '0', '0');

-- ----------------------------
-- Table structure for bw_user_recharge
-- ----------------------------
DROP TABLE IF EXISTS `bw_user_recharge`;
CREATE TABLE `bw_user_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '充值用户ID',
  `order_id` varchar(32) DEFAULT NULL COMMENT '订单号',
  `price` decimal(8,2) DEFAULT NULL COMMENT '充值金额',
  `give_price` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '购买赠送金额',
  `recharge_type` varchar(32) DEFAULT NULL COMMENT '充值类型{radio}(mini_program:微信充值)',
  `paid` tinyint(1) DEFAULT '0' COMMENT '是否充值{switch}(0:未充值,1:已充值)',
  `pay_time` int(10) DEFAULT NULL COMMENT '充值支付时间{date}',
  `add_time` int(12) DEFAULT NULL COMMENT '充值时间{date}',
  `refund_price` decimal(10,2) DEFAULT '0.00' COMMENT '退款金额',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `order_id` (`order_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `recharge_type` (`recharge_type`) USING BTREE,
  KEY `paid` (`paid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='用户充值表';

-- ----------------------------
-- Records of bw_user_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_fans
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_fans`;
CREATE TABLE `bw_wechat_fans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(50) DEFAULT '' COMMENT '公众号APPID',
  `unionid` varchar(100) DEFAULT '' COMMENT '粉丝unionid',
  `openid` varchar(100) DEFAULT '' COMMENT '粉丝openid',
  `tagid_list` varchar(100) DEFAULT '' COMMENT '粉丝标签id',
  `is_black` tinyint(1) unsigned DEFAULT '0' COMMENT '是否为黑名单状态',
  `subscribe` tinyint(1) unsigned DEFAULT '0' COMMENT '关注状态(0未关注,1已关注)',
  `nickname` varchar(200) DEFAULT '' COMMENT '用户昵称',
  `sex` tinyint(1) unsigned DEFAULT '0' COMMENT '用户性别(1男性,2女性,0未知)',
  `country` varchar(50) DEFAULT '' COMMENT '用户所在国家',
  `province` varchar(50) DEFAULT '' COMMENT '用户所在省份',
  `city` varchar(50) DEFAULT '' COMMENT '用户所在城市',
  `language` varchar(50) DEFAULT '' COMMENT '用户的语言(zh_CN)',
  `headimgurl` varchar(500) DEFAULT '' COMMENT '用户头像',
  `subscribe_time` bigint(20) unsigned DEFAULT '0' COMMENT '关注时间',
  `subscribe_at` datetime DEFAULT NULL COMMENT '关注时间',
  `remark` varchar(50) DEFAULT '' COMMENT '备注',
  `subscribe_scene` varchar(200) DEFAULT '' COMMENT '扫码关注场景',
  `qr_scene` varchar(100) DEFAULT '' COMMENT '二维码场景值',
  `qr_scene_str` varchar(200) DEFAULT '' COMMENT '二维码场景内容',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_fans_openid` (`openid`) USING BTREE,
  KEY `index_wechat_fans_unionid` (`unionid`) USING BTREE,
  KEY `index_wechat_fans_is_back` (`is_black`) USING BTREE,
  KEY `index_wechat_fans_subscribe` (`subscribe`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信-粉丝';

-- ----------------------------
-- Records of bw_wechat_fans
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_fans_tags
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_fans_tags`;
CREATE TABLE `bw_wechat_fans_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '标签ID',
  `appid` varchar(50) DEFAULT '' COMMENT '公众号APPID',
  `name` varchar(35) DEFAULT NULL COMMENT '标签名称',
  `count` bigint(20) unsigned DEFAULT '0' COMMENT '总数',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  KEY `index_wechat_fans_tags_id` (`id`) USING BTREE,
  KEY `index_wechat_fans_tags_appid` (`appid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信-标签';

-- ----------------------------
-- Records of bw_wechat_fans_tags
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_keys
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_keys`;
CREATE TABLE `bw_wechat_keys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appid` char(100) DEFAULT '' COMMENT '公众号APPID',
  `type` varchar(20) DEFAULT '' COMMENT '类型(text,image,news)',
  `keys` varchar(100) DEFAULT '' COMMENT '关键字',
  `content` text COMMENT '文本内容',
  `image_url` varchar(255) DEFAULT '' COMMENT '图片链接',
  `voice_url` varchar(255) DEFAULT '' COMMENT '语音链接',
  `music_title` varchar(100) DEFAULT '' COMMENT '音乐标题',
  `music_url` varchar(255) DEFAULT '' COMMENT '音乐链接',
  `music_image` varchar(255) DEFAULT '' COMMENT '缩略图片',
  `music_desc` varchar(255) DEFAULT '' COMMENT '音乐描述',
  `video_title` varchar(100) DEFAULT '' COMMENT '视频标题',
  `video_url` varchar(255) DEFAULT '' COMMENT '视频URL',
  `video_desc` varchar(255) DEFAULT '' COMMENT '视频描述',
  `news_id` bigint(20) unsigned DEFAULT '0' COMMENT '图文ID',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序字段',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0禁用,1启用)',
  `create_by` bigint(20) unsigned DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `member_miniapp_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属租户应用的id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_keys_appid` (`appid`) USING BTREE,
  KEY `index_wechat_keys_type` (`type`) USING BTREE,
  KEY `index_wechat_keys_keys` (`keys`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COMMENT='微信-关键字';

-- ----------------------------
-- Records of bw_wechat_keys
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_keyword
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_keyword`;
CREATE TABLE `bw_wechat_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_miniapp_id` int(11) NOT NULL COMMENT '租户应用id',
  `is_miniapp` enum('0','1') NOT NULL DEFAULT '0' COMMENT '小程序{switch}(0:否,1:是)',
  `type` enum('text','image') NOT NULL DEFAULT 'text' COMMENT '类型{select}(text:文本,image:图片)',
  `keyword` varchar(100) NOT NULL DEFAULT '' COMMENT '关键字',
  `title` varchar(255) DEFAULT '' COMMENT '标题',
  `url` varchar(255) DEFAULT '' COMMENT '网址',
  `image` varchar(255) DEFAULT '' COMMENT '图片',
  `content` text COMMENT '文本{editor}',
  `media_id` varchar(255) DEFAULT NULL COMMENT '资源id',
  `media` text COMMENT '资源',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_miniapp_id` (`member_miniapp_id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='微信公众号关键字服务';

-- ----------------------------
-- Records of bw_wechat_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_media
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_media`;
CREATE TABLE `bw_wechat_media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(100) DEFAULT '' COMMENT '公众号ID',
  `md5` varchar(32) DEFAULT '' COMMENT '文件md5',
  `type` varchar(20) DEFAULT '' COMMENT '媒体类型',
  `media_id` varchar(100) DEFAULT '' COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT '' COMMENT '本地文件链接',
  `media` varchar(255) DEFAULT '' COMMENT '素材media数据',
  `media_url` varchar(300) DEFAULT '' COMMENT '远程图片链接',
  `create_at` int(12) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_media_appid` (`appid`) USING BTREE,
  KEY `index_wechat_media_md5` (`md5`) USING BTREE,
  KEY `index_wechat_media_type` (`type`) USING BTREE,
  KEY `index_wechat_media_media_id` (`media_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='微信-素材';

-- ----------------------------
-- Records of bw_wechat_media
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_message_log
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_message_log`;
CREATE TABLE `bw_wechat_message_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型{radio}(0:小程序订阅消息,1:公众号模板消息)',
  `member_miniapp_id` int(10) unsigned NOT NULL COMMENT '租户应用ID',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `key` varchar(20) NOT NULL DEFAULT '' COMMENT '模板标识',
  `param` text NOT NULL COMMENT '参数',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态{radio}(0:失败,1:成功)',
  `fail_msg` varchar(255) NOT NULL DEFAULT '' COMMENT '失败原因',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COMMENT='微信消息记录';

-- ----------------------------
-- Records of bw_wechat_message_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_message_template
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_message_template`;
CREATE TABLE `bw_wechat_message_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_miniapp_id` int(10) unsigned NOT NULL COMMENT '租户应用ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型{radio}(0:小程序订阅消息,1:公众号模板消息)',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '模板名',
  `key` varchar(20) NOT NULL DEFAULT '' COMMENT '模板标识',
  `template_id` varchar(50) NOT NULL DEFAULT '' COMMENT '模板ID',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '回复内容',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:关闭,1:开启)',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `member_miniapp_id` (`member_miniapp_id`,`type`,`key`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='微信消息模板';

-- ----------------------------
-- Records of bw_wechat_message_template
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_news
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_news`;
CREATE TABLE `bw_wechat_news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `media_id` varchar(100) DEFAULT '' COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT '' COMMENT '永久素材外网URL',
  `article_id` varchar(60) DEFAULT '' COMMENT '关联图文ID(用英文逗号做分割)',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除状态(0未删除,1已删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `member_miniapp_id` int(11) DEFAULT '0' COMMENT '所属租户应用的id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_news_artcle_id` (`article_id`) USING BTREE,
  KEY `index_wechat_news_media_id` (`media_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='微信-图文';

-- ----------------------------
-- Records of bw_wechat_news
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_news_article
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_news_article`;
CREATE TABLE `bw_wechat_news_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '' COMMENT '素材标题',
  `local_url` varchar(300) DEFAULT '' COMMENT '永久素材显示URL',
  `show_cover_pic` tinyint(4) unsigned DEFAULT '0' COMMENT '显示封面(0不显示,1显示)',
  `author` varchar(20) DEFAULT '' COMMENT '文章作者',
  `digest` varchar(300) DEFAULT '' COMMENT '摘要内容',
  `content` longtext COMMENT '图文内容',
  `content_source_url` varchar(200) DEFAULT '' COMMENT '原文地址',
  `read_num` bigint(20) unsigned DEFAULT '0' COMMENT '阅读数量',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='微信-文章';

-- ----------------------------
-- Records of bw_wechat_news_article
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_notify_queue
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_notify_queue`;
CREATE TABLE `bw_wechat_notify_queue` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_send` tinyint(2) DEFAULT NULL,
  `param` text,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ISSEND` (`is_send`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站内信';

-- ----------------------------
-- Records of bw_wechat_notify_queue
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_open_token
-- ----------------------------
DROP TABLE IF EXISTS `bw_wechat_open_token`;
CREATE TABLE `bw_wechat_open_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `authorizer_appid` varchar(255) DEFAULT NULL,
  `authorizer_access_token` varchar(255) DEFAULT NULL,
  `authorizer_refresh_token` varchar(255) DEFAULT NULL,
  `expires_in` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bw_wechat_open_token
-- ----------------------------
