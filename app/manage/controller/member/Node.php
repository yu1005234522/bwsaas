<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use buwang\base\MemberBaseController;
use buwang\traits\JwtTrait;
use app\manage\model\AuthNode;
use app\manage\model\AuthGroupNode;
use app\manage\model\MemberNode;

class Node extends MemberBaseController
{
    /*protected $middleware = [
        'login' => ['except' => []]
    ];*/

    public function index()
    {
        //得到登录的租户ID
        $member_id = $this->request->uid;

        //查询租户拥有的权限

        $list = MemberNode::getlist($member_id);

        $list = AuthNode::tree($list, 'title', 'id');
        // var_dump($list);die;
        return view('', ['list' => $list])->filter(function ($content) {
            return str_replace("&amp;emsp;", '&emsp;', $content);
        });
    }

    /**
     * 编辑节点
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        //得到登录的租户ID
        $member_id = $this->request->uid;
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //父级不能是自己
            if (isset($param['pid'])) {
                if ($param['pid'] == $param['id']) {
                    return $this->error('父ID不能是自己');
                }
            }

            if (isset($param['auth_name'])) {
                //判断是否存在唯一标识
                $count = AuthNode::where('auth_name', $param['auth_name'])->where('id', 'not in', $param['id'])->count();
                if ($count) return $this->error('已存在该标识');
            }

            $result = AuthNode::update($param);
            //编辑中间表
            $AuthGroupNodes = AuthGroupNode::where('node_id', $param['id'])->select()->toArray();
            if ($AuthGroupNodes) {
                $AuthGroup_data = array();
                foreach ($AuthGroupNodes as $AuthGroupNode) {
                    $GroupNode = array();
                    $GroupNode ['id'] = $AuthGroupNode['id'];
                    if (isset($param['type'])) {
                        $GroupNode['type'] = $param['type'];
                    }
                    if (isset($param['name'])) {
                        $GroupNode['node_name'] = $param['name'];
                    }
                    if (isset($param['auth_name'])) {
                        $GroupNode['auth_name'] = $param['auth_name'];
                    }
                    $AuthGroup_data[] = $GroupNode;
                }
                $AuthGroupNode = new AuthGroupNode;
                $AuthGroupNode->saveAll($AuthGroup_data);
            }

            if ($result) {
                //xn_add_admin_log('编辑权限节点');
                return $this->success('操作成功');
            } else {
                return $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $data = AuthNode::where('id', $id)->find();

        //查询租户拥有的权限
        $list = MemberNode::getlist($member_id);
        $list = AuthNode::tree($list, 'title', 'id');
        return view('form', ['data' => $data, 'list' => $list, 'pid' => $data['pid']])->filter(function ($content) {
            return str_replace("&amp;emsp;", '&emsp;', $content);
        });
    }

    /**
     * 添加节点
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        //得到登录的租户ID
        $member_id = $this->request->uid;
        if ($this->request->isPost()) {
            $param = $this->request->param();
            if (!$param['auth_name']) return $this->error('权限标识必填');
            //判断是否存在唯一标识
            $count = AuthNode::where('auth_name', $param['auth_name'])->count();
            if ($count) return $this->error('已存在该标识');
            $result = AuthNode::create($param);
            if ($result) {
                //xn_add_admin_log('添加权限节点');
                return $this->success('操作成功');
            } else {
                return $this->error('操作失败');
            }
        }


        //查询租户拥有的权限

        $list = MemberNode::getlist($member_id);
        $list = AuthNode::tree($list, 'title', 'id');
        return view('form', ['list' => $list, 'pid' => $this->request->get('pid')])->filter(function ($content) {
            return str_replace("&amp;emsp;", '&emsp;', $content);
        });
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = intval($this->request->get('id'));
        !($id > 0) && return $this->error('参数错误');
        $child_count = AuthNode::where('pid', $id)->count();
        $child_count && return $this->error('请先删除子节点');
        AuthNode::destroy($id);
        //删除中间表
        $ids = AuthGroupNode::where('node_id', $id)->column('id');
        if ($ids) {
            AuthGroupNode::destroy($ids);
        }
        //xn_add_admin_log('删除权限节点');
        return $this->success('删除成功');
    }

    /**
     * 排序
     */
    public function sort()
    {
        $param = $this->request->post();
        foreach ($param as $k => $v) {
            $v = empty($v) ? null : $v;
            if (!$v) $v = 0;
            AuthNode::where('id', $k)->save(['sort' => $v]);
        }
        return redirect('/manage/node/index');
    }
}
