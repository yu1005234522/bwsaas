<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\util\crud;

use buwang\util\crud\exceptions\TableException;
use buwang\util\CommonTool;
use think\exception\FileException;
use think\facade\Db;

/**
 * 快速构建系统CRUD
 */
class BuildCrud
{

    /**
     * 当前目录
     * @var string
     */
    protected $dir;

    /**
     * 应用目录
     * @var string
     */
    protected $rootDir;

    /**
     * 分隔符
     * @var string
     */
    protected $DS = DIRECTORY_SEPARATOR;

    /**
     * 数据库名
     * @var string
     */
    protected $dbName;

    /**
     *  表前缀
     * @var string
     */
    protected $tablePrefix = 'ea';

    /**
     * 主表
     * @var string
     */
    protected $table;

    /**
     * 表注释名
     * @var string
     */
    protected $tableComment;

    /**
     * 主表列信息
     * @var array
     */
    protected $tableColumns;

    /**
     * 数据列表可见字段
     * @var string
     */
    protected $fields;

    /**
     * 是否软删除模式
     * @var bool
     */
    protected $delete = false;

    /**
     * 是否强制覆盖
     * @var bool
     */
    protected $force = false;

    /**
     * 关联模型
     * @var array
     */
    protected $relationArray = [];

    /**
     * 控制器对应的URL
     * @var string
     */
    protected $controllerUrl;

    /**
     * 生成的控制器名
     * @var string
     */
    protected $controllerFilename;


    /**
     * 控制器命名
     * @var string
     */
    protected $controllerName;

    /**
     * 控制器命名空间
     * @var string
     */
    protected $controllerNamespace;

    /**
     * 视图名
     * @var string
     */
    protected $viewFilename;

    /**
     * js文件名
     * @var string
     */
    protected $jsFilename;

    /**
     * 生成的模型文件名
     * @var string
     */
    protected $modelFilename;

    /**
     * 主表模型命名
     * @var string
     */
    protected $modelName;

    /**
     * 复选框字段后缀
     * @var array
     */
    protected $checkboxFieldSuffix = [];

    /**
     * 单选框字段后缀
     * @var array
     */
    protected $radioFieldSuffix = [];

    /**
     * 单图片字段后缀
     * @var array
     */
    protected $imageFieldSuffix = ['image', 'logo', 'photo', 'icon'];

    /**
     * 多图片字段后缀
     * @var array
     */
    protected $imagesFieldSuffix = ['images', 'photos', 'icons'];

    /**
     * 单文件字段后缀
     * @var array
     */
    protected $fileFieldSuffix = ['file'];

    /**
     * 多文件字段后缀
     * @var array
     */
    protected $filesFieldSuffix = ['files'];

    /**
     * 时间字段后缀
     * @var array
     */
    protected $dateFieldSuffix = ['time', 'date'];

    /**
     * 开关组件字段
     * @var array
     */
    protected $switchFields = ['status'];

    /**
     * 下拉选择字段
     * @var array
     */
    protected $selectFileds = [];

    /**
     * 富文本字段
     * @var array
     */
    protected $editorFields = [];

    /**
     * 排序字段
     * @var array
     */
    protected $sortFields = [];

    /**
     * 忽略字段
     * @var array
     */
    protected $ignoreFields = ['update_time', 'delete_time'];

    /**
     * 外键字段
     * @var array
     */
    protected $foreignKeyFields = [];

    /**
     * 相关生成文件
     * @var array
     */
    protected $fileList = [];

    /**
     * 表单类型
     * @var array
     */
    protected $formTypeArray = ['text', 'image', 'images', 'file', 'files', 'select', 'switch', 'date', 'editor', 'textarea', 'checkbox', 'radio'];

    /**
     * 初始化
     * BuildCurd constructor.
     */
    public function __construct()
    {
        $this->tablePrefix = config('database.connections.mysql.prefix');
        $this->dbName = config('database.connections.mysql.database');
        $this->dir = __DIR__;
        $this->rootDir = root_path();
        return $this;
    }

    /**
     * 设置主表
     * @param $table
     * @return $this
     * @throws TableException
     */
    public function setTable($table)
    {
        $this->table = $table;
        try {

            // 获取表列相关
            $colums = Db::query("SHOW FULL COLUMNS FROM {$this->tablePrefix}{$this->table}");
            foreach ($colums as $vo) {

                $colum = [
                    'type' => $vo['Type'],//字段类型
                    'comment' => !empty($vo['Comment']) ? $vo['Comment'] : $vo['Field'],//默认注释为字段名
                    'required' => $vo['Null'] == "NO" ? true : false,//生成Html代码的required用到
                    'default' => $vo['Default'],//获取字段的默认值
                ];

                // 格式化列数据
                $this->buildColum($colum);//可能会增加字段formType和define并修改了comment字段
                $this->tableColumns[$vo['Field']] = $colum;//保存处理过的字段数据

                if ($vo['Field'] == 'delete_time') {//启动软删除
                    $this->delete = true;
                }

            }

            // 获取表名注释
            $table_schema = env('database.database', 'bwsaas');
            $tableSchema = Db::query("SELECT table_name,table_comment FROM information_schema.TABLES WHERE table_schema = '{$table_schema}' AND table_name = '{$this->tablePrefix}{$this->table}'");
            $this->tableComment = (isset($tableSchema[0]['table_comment']) && !empty($tableSchema[0]['table_comment'])) ? $tableSchema[0]['table_comment'] : $this->table;

        } catch (\Exception $e) {
            throw new TableException($e->getMessage());
        }

        // 初始化默认控制器名 不带PHP后缀 crud/Demo
        $nodeArray = explode('_', $this->table);
        if (count($nodeArray) == 1) {
            $this->controllerFilename = ucfirst($nodeArray[0]);
        } else {
            foreach ($nodeArray as $k => $v) {
                if ($k == 0) {
                    $this->controllerFilename = "{$v}{$this->DS}";
                } else {
                    $this->controllerFilename .= ucfirst($v);
                }
            }
        }

        // 初始化默认模型名 CrudDemo
        $this->modelFilename = ucfirst(CommonTool::lineToHump($this->table));

        $this->buildViewJsUrl();//构建视图和js的url，控制器 模型 等命名空间和文件名

        // 构建数据
        $this->buildStructure();//处理字段名为sort为排序 describe detail content为编辑器

        return $this;
    }

    /**
     * 设置关联表
     * @param $relationTable
     * @param $foreignKey
     * @param null $primaryKey
     * @param null $modelFilename
     * @param array $onlyShowFileds
     * @param null $bindSelectField
     * @return $this
     * @throws TableException
     */
    public function setRelation($relationTable, $foreignKey, $primaryKey = null, $modelFilename = null, $onlyShowFileds = [], $bindSelectField = null)
    {
        if (!isset($this->tableColumns[$foreignKey])) {
            throw new TableException("主表不存在外键字段：{$foreignKey}");
        }
        if (!empty($modelFilename)) {
            $modelFilename = str_replace('/', $this->DS, $modelFilename);//如果为Null 转化为 ""
        }
        try {
            $colums = Db::query("SHOW FULL COLUMNS FROM {$this->tablePrefix}{$relationTable}");
            $formatColums = [];
            $delete = false;
            //判断下拉选择的字段是否存在
            if (!empty($bindSelectField) && !in_array($bindSelectField, array_column($colums, 'Field'))) {
                throw new TableException("关联表{$relationTable}不存在该字段: {$bindSelectField}");
            }
            foreach ($colums as $vo) {
                //关联表的主键如果为空，就自动查询赋值
                if (empty($primaryKey) && $vo['Key'] == 'PRI') {
                    $primaryKey = $vo['Field'];
                }
                //只处理$onlyShowFileds定义的字段
                if (!empty($onlyShowFileds) && !in_array($vo['Field'], $onlyShowFileds)) {
                    continue;
                }
                $colum = [
                    'type' => $vo['Type'],
                    'comment' => $vo['Comment'],
                    'default' => $vo['Default'],
                ];

                $this->buildColum($colum);

                $formatColums[$vo['Field']] = $colum;
                //是否启用软删除
                if ($vo['Field'] == 'delete_time') {
                    $delete = true;
                }
            }
            //关联模型名
            $modelFilename = empty($modelFilename) ? ucfirst(CommonTool::lineToHump($relationTable)) : $modelFilename;
            $modelArray = explode($this->DS, $modelFilename);
            $modelName = array_pop($modelArray);

            $relation = [
                'modelFilename' => $modelFilename,//带 / 如 relation/CrudDemoCate
                'modelName' => $modelName,//不带 / 如 CrudDemoCate
                'foreignKey' => $foreignKey,
                'primaryKey' => $primaryKey,
                'bindSelectField' => $bindSelectField,//生成select用到的显示字段
                'delete' => $delete,//是否启用软删除
                'tableColumns' => $formatColums,
            ];

            if (!empty($bindSelectField)) {
                //修复$'modelFilename为$modelName
                $relationArray = explode('\\', $modelName);
                //为主表增加字段 以$foreignKey为key 存储关联模型相关数据
                $this->tableColumns[$foreignKey]['bindSelectField'] = $bindSelectField;
                $this->tableColumns[$foreignKey]['bindRelation'] = end($relationArray);
            }
            $this->relationArray[$relationTable] = $relation;//关联模型数组存储数据
            $this->selectFileds[] = $foreignKey;//自动设置的下拉字段，也可以命令行直接传入更多filed
        } catch (\Exception $e) {
            throw new TableException($e->getMessage());
        }
        return $this;
    }

    /**
     * 设置控制器名
     * @param $controllerFilename
     * @return $this
     */
    public function setControllerFilename($controllerFilename)
    {
        $this->controllerFilename = str_replace('/', $this->DS, $controllerFilename);
        $this->buildViewJsUrl();
        return $this;
    }

    /**
     * 设置模型名
     * @param $modelFilename
     * @return $this
     */
    public function setModelFilename($modelFilename)
    {
        $this->modelFilename = str_replace('/', $this->DS, $modelFilename);
        $this->buildViewJsUrl();
        return $this;
    }

    /**
     * 设置显示字段
     * @param $fields
     * @return $this
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * 设置删除模式
     * @param $delete
     * @return $this
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;
        return $this;
    }

    /**
     * 设置是否强制替换
     * @param $force
     * @return $this
     */
    public function setForce($force)
    {
        $this->force = $force;
        return $this;
    }

    /**
     * 设置复选框字段后缀
     * @param $array
     * @return $this
     */
    public function setCheckboxFieldSuffix($array)
    {
        $this->checkboxFieldSuffix = array_merge($this->checkboxFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置单选框字段后缀
     * @param $array
     * @return $this
     */
    public function setRadioFieldSuffix($array)
    {
        $this->radioFieldSuffix = array_merge($this->radioFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置单图片字段后缀
     * @param $array
     * @return $this
     */
    public function setImageFieldSuffix($array)
    {
        $this->imageFieldSuffix = array_merge($this->imageFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置多图片字段后缀
     * @param $array
     * @return $this
     */
    public function setImagesFieldSuffix($array)
    {
        $this->imagesFieldSuffix = array_merge($this->imagesFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置单文件字段后缀
     * @param $array
     * @return $this
     */
    public function setFileFieldSuffix($array)
    {
        $this->fileFieldSuffix = array_merge($this->fileFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置多文件字段后缀
     * @param $array
     * @return $this
     */
    public function setFilesFieldSuffix($array)
    {
        $this->filesFieldSuffix = array_merge($this->filesFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置时间字段后缀
     * @param $array
     * @return $this
     */
    public function setDateFieldSuffix($array)
    {
        $this->dateFieldSuffix = array_merge($this->dateFieldSuffix, $array);
        return $this;
    }

    /**
     * 设置开关字段
     * @param $array
     * @return $this
     */
    public function setSwitchFields($array)
    {
        $this->switchFields = array_merge($this->switchFields, $array);
        return $this;
    }

    /**
     * 设置下拉选择字段
     * @param $array
     * @return $this
     */
    public function setSelectFileds($array)
    {
        $this->selectFileds = array_merge($this->selectFileds, $array);
        return $this;
    }

    /**
     * 设置排序字段
     * @param $array
     * @return $this
     */
    public function setSortFields($array)
    {
        $this->sortFields = array_merge($this->sortFields, $array);
        return $this;
    }

    /**
     * 设置忽略字段
     * @param $array
     * @return $this
     */
    public function setIgnoreFields($array)
    {
        $this->ignoreFields = array_merge($this->ignoreFields, $array);
        return $this;
    }

    /**
     * 获取相关的文件
     * @return array
     */
    public function getFileList()
    {
        return $this->fileList;
    }

    /**
     * 构建基础视图、JS、URL
     * @return $this
     */
    protected function buildViewJsUrl()
    {
        //$this->controllerFilename crud/Demo
        $nodeArray = explode($this->DS, $this->controllerFilename);
        $formatArray = [];
        foreach ($nodeArray as $vo) {
            $formatArray[] = CommonTool::humpToLine(lcfirst($vo));
        }

        $this->viewFilename = implode($this->DS, $formatArray);
        //处理数组的最后一个元素
        $formatArray[count($formatArray) - 1] = ucfirst($formatArray[count($formatArray) - 1]);
        $this->controllerUrl = implode('.', $formatArray); //crud.Demo
        $this->jsFilename = $this->viewFilename;//crud/demo

        // 控制器命名空间
        $namespaceArray = $nodeArray;
        $this->controllerName = array_pop($namespaceArray); // Demo
        $namespaceSuffix = implode('\\', $namespaceArray);
        $this->controllerNamespace = empty($namespaceSuffix) ? "app\admin\controller" : "app\admin\controller\\{$namespaceSuffix}";

        // 主表模型命名 CrudDemo
        $modelArray = explode($this->DS, $this->modelFilename);

        $this->modelName = array_pop($modelArray);

        return $this;
    }

    /**
     * 构建字段
     * @return $this
     */
    protected function buildStructure()
    {
        foreach ($this->tableColumns as $key => $val) {

            // 排序
            if (in_array($key, ['sort'])) {
                $this->sortFields[] = $key;
            }

            // 富文本
            if (in_array($key, ['describe', 'content', 'details'])) {
                $this->editorFields[] = $key;
            }

        }
        return $this;
    }

    /**
     * 构建必填
     * @param $require
     * @return string
     */
    protected function buildRequiredHtml($require)
    {
        return $require ? 'lay-verify="required"' : "";
    }

    /**
     * 构建初始化字段信息
     * @param $colum
     * @return mixed
     */
    protected function buildColum(&$colum)
    {

        $string = $colum['comment'];
        //性别 {radio} (1:男, 2:女, 0:未知)
        // 处理字段注释里面的自定义类型 符合規則的會附加字段formType，如 radio
        preg_match('/{[\s\S]*?}/i', $string, $formTypeMatch);//{radio}

        if (!empty($formTypeMatch) && isset($formTypeMatch[0])) {
            $colum['comment'] = str_replace($formTypeMatch[0], '', $colum['comment']);
            $formType = trim(str_replace('}', '', str_replace('{', '', $formTypeMatch[0])));
            if (in_array($formType, $this->formTypeArray)) {
                $colum['formType'] = $formType;
            }
        }

        // 处理字段类型的數據集如 ['1'=>'男','2'=>'女','0'=>'未知']
        preg_match('/\([\s\S]*?\)/i', $string, $defineMatch);
        if (!empty($formTypeMatch) && isset($defineMatch[0])) {
            $colum['comment'] = str_replace($defineMatch[0], '', $colum['comment']);
            if (isset($colum['formType']) && in_array($colum['formType'], ['images', 'files', 'select', 'switch', 'radio', 'checkbox', 'date'])) {
                $define = str_replace(')', '', str_replace('(', '', $defineMatch[0]));
                if (in_array($colum['formType'], ['select', 'switch', 'radio', 'checkbox'])) {
                    $formatDefine = [];
                    $explodeArray = explode(',', $define);
                    foreach ($explodeArray as $vo) {
                        $voExplodeArray = explode(':', $vo);
                        if (count($voExplodeArray) == 2) {
                            $formatDefine[trim($voExplodeArray[0])] = trim($voExplodeArray[1]);
                        }
                    }
                    !empty($formatDefine) && $colum['define'] = $formatDefine;//处理完成的数据集合 ['1'=>'男','2'=>'女','0'=>'未知']
                } else {
                    $colum['define'] = $define;//除了'select', 'switch', 'radio', 'checkbox'其它的保留原来数据
                }
            }
        }

        $colum['comment'] = trim($colum['comment']);

        return $colum;
    }

    /**
     * 构建下拉控制器
     * @param $field
     * @return mixed
     */
    protected function buildSelectController($field)
    {
        $field = CommonTool::lineToHump(ucfirst($field));
        $name = "get{$field}List";
        $selectCode = CommonTool::replaceTemplate(
            $this->getTemplate("controller{$this->DS}select"),
            [
                'name' => $name,
            ]);
        return $selectCode;
    }

    /**
     * 构架下拉模型
     * @param $field
     * @param $array
     * @return mixed
     */
    protected function buildSelectModel($field, $array)
    {
        $field = CommonTool::lineToHump(ucfirst($field));
        $name = "get{$field}List";
        $values = '[';
        foreach ($array as $k => $v) {
            $values .= "'{$k}'=>'{$v}',";
        }
        $values .= ']';
        $selectCode = CommonTool::replaceTemplate(
            $this->getTemplate("model{$this->DS}select"),
            [
                'name' => $name,
                'values' => $values,
            ]);
        return $selectCode;
    }

    /**
     * 构架关联下拉模型
     * @param $relation
     * @param $filed
     * @return mixed
     */
    protected function buildRelationSelectModel($relation, $filed)
    {
        $relationArray = explode('/', $relation);
        $name = end($relationArray);
        $name = "get{$name}List";
        $relation = str_replace($this->DS, '\\', $relation);
        $selectCode = CommonTool::replaceTemplate(
            $this->getTemplate("model{$this->DS}relationSelect"),
            [
                'name' => $name,
                'relation' => "\app\admin\model\\{$relation}",
                'values' => $filed,
            ]);
        return $selectCode;
    }

    /**
     * 构建下拉框视图
     * @param $field
     * @param string $select
     * @return mixed
     */
    protected function buildOptionView($field, $select = '')
    {
        $field = CommonTool::lineToHump(ucfirst($field));
        $name = "get{$field}List";
        $optionCode = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}module{$this->DS}option"),
            [
                'name' => $name,
                'select' => $select,
            ]);
        return $optionCode;
    }

    /**
     * 构建switch视图
     * @param $field
     * @param $val
     * @param string $select
     * @return mixed
     */
    protected function buildSwitchView($field, $val, $select = '', $edit = false)
    {
        $optionCode = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}module{$this->DS}switchInput"),
            [
                'field' => $field,
                'select' => $select,
                'value' => $edit ? '{$row.' . $field . '|default=\'\'}' : $val['default'],
                'text' => implode("|", array_values(array_reverse($val['define'])))
            ]);
        return $optionCode;
    }

    /**
     * 构建单选框视图
     * @param $field
     * @param string $select
     * @return mixed
     */
    protected function buildRadioView($field, $select = '')
    {
        $formatField = CommonTool::lineToHump(ucfirst($field));
        $name = "get{$formatField}List";
        $optionCode = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}module{$this->DS}radioInput"),
            [
                'field' => $field,
                'name' => $name,
                'select' => $select,
            ]);
        return $optionCode;
    }

    /**
     * 构建多选框视图
     * @param $field
     * @param string $select
     * @return mixed
     */
    protected function buildCheckboxView($field, $select = '')
    {
        $formatField = CommonTool::lineToHump(ucfirst($field));
        $name = "get{$formatField}List";
        $optionCode = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}module{$this->DS}checkboxInput"),
            [
                'field' => $field,
                'name' => $name,
                'select' => $select,
            ]);
        return $optionCode;
    }

    /**
     * 初始化
     * @return $this
     */
    public function render()
    {

        //初始化主表和关联表字段formType数据
        $this->renderData();

        // 控制器
        $this->renderController();

        // 模型
        $this->renderModel();

        // 视图
        $this->renderView();

        // JS
        $this->renderJs();

        return $this;
    }

    /**
     * 初始化主表和关联表字段formType数据
     * text    普通文本框
     * image    单图片
     * images    多图片
     * file    单文件
     * files    多文件
     * date    时间组件    需配合数据集使用,时间控件类型选择 [year年选择器,month年月选择,date日期选择器,time时间选择器,datetime日期时间选择器]
     * editor    富文本
     * textarea    普通文本
     * select    下拉选择    需配合数据集使用
     * switch    开关组件    需配合数据集使用
     * checkbox    多选框    需配合数据集使用
     * radio    单选框    需配合数据集使用
     * @return $this
     */
    protected function renderData()
    {

        // 主表
        foreach ($this->tableColumns as $field => $val) {

            // 过滤主表被忽略不显示的字段
            if (in_array($field, $this->ignoreFields)) {
                unset($this->tableColumns[$field]);
                continue;
            }

            // 判断是否已初始化（已经通过字段注释来
            //$this->buildColum($colum);
            //初始化的字段跳过）
            if (isset($this->tableColumns[$field]['formType'])) {
                continue;
            }

            // 判断图片
            if ($this->checkContain($field, $this->imageFieldSuffix)) {
                $this->tableColumns[$field]['formType'] = 'image';
                continue;
            }
            if ($this->checkContain($field, $this->imagesFieldSuffix)) {
                $this->tableColumns[$field]['formType'] = 'images';
                continue;
            }

            // 判断文件
            if ($this->checkContain($field, $this->fileFieldSuffix)) {
                $this->tableColumns[$field]['formType'] = 'file';
                continue;
            }
            if ($this->checkContain($field, $this->filesFieldSuffix)) {
                $this->tableColumns[$field]['formType'] = 'files';
                continue;
            }

            // 判断时间
            if ($this->checkContain($field, $this->dateFieldSuffix)) {
                $this->tableColumns[$field]['formType'] = 'date';
                continue;
            }

            // 判断开关
            if (in_array($field, $this->switchFields)) {
                $this->tableColumns[$field]['formType'] = 'switch';
                continue;
            }

            // 判断富文本
            if (in_array($field, $this->editorFields)) {
                $this->tableColumns[$field]['formType'] = 'editor';
                continue;
            }

            // 判断排序
            if (in_array($field, $this->sortFields)) {
                $this->tableColumns[$field]['formType'] = 'sort';
                continue;
            }

            // 判断下拉选择
            if (in_array($field, $this->selectFileds)) {
                $this->tableColumns[$field]['formType'] = 'select';
                continue;
            }

            $this->tableColumns[$field]['formType'] = 'text';
        }

        // 关联表
        foreach ($this->relationArray as $table => $tableVal) {
            foreach ($tableVal['tableColumns'] as $field => $val) {

                // 过滤字段
                if (in_array($field, $this->ignoreFields)) {
                    unset($this->relationArray[$table]['tableColumns'][$field]);
                    continue;
                }

                // 判断是否已初始化
                if (isset($this->relationArray[$table]['tableColumns'][$field]['formType'])) {
                    continue;
                }

                // 判断图片
                if ($this->checkContain($field, $this->imageFieldSuffix)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'image';
                    continue;
                }
                if ($this->checkContain($field, $this->imagesFieldSuffix)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'images';
                    continue;
                }

                // 判断文件
                if ($this->checkContain($field, $this->fileFieldSuffix)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'file';
                    continue;
                }
                if ($this->checkContain($field, $this->filesFieldSuffix)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'files';
                    continue;
                }

                // 判断时间
                if ($this->checkContain($field, $this->dateFieldSuffix)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'date';
                    continue;
                }

                // 判断开关
                if (in_array($field, $this->switchFields)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'switch';
                    continue;
                }

                // 判断富文本
                if (in_array($field, $this->editorFields)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'editor';
                    continue;
                }

                // 判断排序
                if (in_array($field, $this->sortFields)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'sort';
                    continue;
                }

                // 判断下拉选择
                if (in_array($field, $this->selectFileds)) {
                    $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'select';
                    continue;
                }

                $this->relationArray[$table]['tableColumns'][$field]['formType'] = 'text';
            }
        }

        return $this;

    }

    /**
     * 初始化控制器
     * @return $this
     */
    protected function renderController()
    {
        $controllerFile = "{$this->rootDir}app{$this->DS}admin{$this->DS}controller{$this->DS}{$this->controllerFilename}.php";
        //如果有关联模型就重写控制器的index方法，用关联模型获取关联数据
        $relationSearch = '';
        if (empty($this->relationArray)) {
            $controllerIndexMethod = '';
        } else {
            $relationCode = '';
            $relationSearch = 'protected $relationSearch = true;';
            foreach ($this->relationArray as $key => $val) {
                $relation = CommonTool::lineToHump($key);
                $relationCode .= "->withJoin('{$relation}', 'LEFT')\r";
            }
            $controllerIndexMethod = CommonTool::replaceTemplate(
                $this->getTemplate("controller{$this->DS}indexMethod"),
                [
                    'relationIndexMethod' => $relationCode,
                ]);
        }
        $selectList = '';
        //$this->assign('{{name}}', $this->model->{{name}}()); 关联模型模板赋值变量
        foreach ($this->relationArray as $relation) {
            if (!empty($relation['bindSelectField'])) {
                //修复$relation['modelFilename']为$relation['modelName']
                $relationArray = explode('\\', $relation['modelName']);
                $selectList .= $this->buildSelectController(end($relationArray));
            }
        }
        //主表的select生成
        foreach ($this->tableColumns as $field => $val) {
            if (isset($val['formType']) && in_array($val['formType'], ['select', 'switch', 'radio', 'checkbox']) && isset($val['define'])) {
                $selectList .= $this->buildSelectController($field);
            }
        }
        $modelFilenameExtend = str_replace($this->DS, '\\', $this->modelFilename);
        $controllerValue = CommonTool::replaceTemplate(
            $this->getTemplate("controller{$this->DS}controller"),
            [
                'controllerName' => $this->controllerName,
                'controllerNamespace' => $this->controllerNamespace,
                'controllerAnnotation' => $this->tableComment,
                'modelFilename' => "\app\admin\model\\{$modelFilenameExtend}",
                'indexMethod' => $controllerIndexMethod,
                'selectList' => $selectList,
                'relationSearch' => $relationSearch,
            ]);
        $this->fileList[$controllerFile] = $controllerValue;
        return $this;
    }

    /**
     * 初始化模型
     * @return $this
     */
    protected function renderModel()
    {
        // 主表模型
        $modelFile = "{$this->rootDir}app{$this->DS}admin{$this->DS}model{$this->DS}{$this->modelFilename}.php";
        if (empty($this->relationArray)) {
            $relationList = '';
        } else {
            $relationList = '';
            foreach ($this->relationArray as $key => $val) {
                $relation = CommonTool::lineToHump($key);
                $val['modelFilename'] = str_replace($this->DS, '\\', $val['modelFilename']);
                $relationCode = CommonTool::replaceTemplate(
                    $this->getTemplate("model{$this->DS}relation"),
                    [
                        'relationMethod' => $relation,
                        'relationModel' => "\app\admin\model\\{$val['modelFilename']}",
                        'foreignKey' => $val['foreignKey'],
                        'primaryKey' => $val['primaryKey'],
                    ]);
                $relationList .= $relationCode;
            }
        }

        $selectList = '';
        foreach ($this->relationArray as $relation) {
            if (!empty($relation['bindSelectField'])) {
                //修复$relation['modelFilename']为$relation['modelName']
                $selectList .= $this->buildRelationSelectModel($relation['modelFilename'], $relation['bindSelectField']);
            }
        }
        foreach ($this->tableColumns as $field => $val) {
            if (isset($val['formType']) && in_array($val['formType'], ['select', 'switch', 'radio', 'checkbox']) && isset($val['define'])) {
                $selectList .= $this->buildSelectModel($field, $val['define']);
            }
        }

        $extendNamespaceArray = explode($this->DS, $this->modelFilename);
        $extendNamespace = null;
        if (count($extendNamespaceArray) > 1) {
            array_pop($extendNamespaceArray);
            $extendNamespace = '\\' . implode('\\', $extendNamespaceArray);
        }

        $modelValue = CommonTool::replaceTemplate(
            $this->getTemplate("model{$this->DS}model"),
            [
                'modelName' => $this->modelName,
                'modelNamespace' => "app\admin\model{$extendNamespace}",
                'table' => $this->table,
                'deleteTime' => $this->delete ? '"delete_time"' : 'false',
                'relationList' => $relationList,
                'selectList' => $selectList,
            ]);
        $this->fileList[$modelFile] = $modelValue;

        // 关联模型
        foreach ($this->relationArray as $key => $val) {
            $relationModelFile = "{$this->rootDir}app{$this->DS}admin{$this->DS}model{$this->DS}{$val['modelFilename']}.php";

            // todo 判断关联模型文件是否存在, 存在就不重新生成文件, 防止关联模型文件被覆盖
            $relationModelClass = "\\app\\admin\\model\\{$val['modelFilename']}";
            if (class_exists($relationModelClass) && method_exists(new $relationModelClass, 'getName')) {
                $tableName = (new $relationModelClass)->getName();
                if (CommonTool::humpToLine(lcfirst($tableName)) == CommonTool::humpToLine(lcfirst($key))) {
                    continue;
                }
            }

            $extendNamespaceArray = explode($this->DS, $val['modelFilename']);
            $extendNamespace = null;
            if (count($extendNamespaceArray) > 1) {
                array_pop($extendNamespaceArray);
                $extendNamespace = '\\' . implode('\\', $extendNamespaceArray);
            }

            $relationModelValue = CommonTool::replaceTemplate(
                $this->getTemplate("model{$this->DS}model"),
                [
                    'modelName' => $val['modelName'],
                    'modelNamespace' => "app\admin\model{$extendNamespace}",
                    'table' => $key,
                    'deleteTime' => $val['delete'] ? '"delete_time"' : 'false',
                    'relationList' => '',
                    'selectList' => '',
                ]);
            $this->fileList[$relationModelFile] = $relationModelValue;
        }
        return $this;
    }

    /**
     * 初始化视图
     * @return $this
     */
    protected function renderView()
    {
        // 列表页面
        $viewIndexFile = "{$this->rootDir}app{$this->DS}admin{$this->DS}view{$this->DS}{$this->viewFilename}{$this->DS}index.html";
        $viewIndexValue = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}index"),
            [
                'controllerUrl' => $this->controllerUrl,
            ]);
        $this->fileList[$viewIndexFile] = $viewIndexValue;

        // 添加页面
        $viewAddFile = "{$this->rootDir}app{$this->DS}admin{$this->DS}view{$this->DS}{$this->viewFilename}{$this->DS}add.html";
        $addFormList = '';
        foreach ($this->tableColumns as $field => $val) {

            if (in_array($field, ['id', 'create_time'])) {
                continue;
            }

            $templateFile = "view{$this->DS}module{$this->DS}input";
            $define = '';

            // 根据formType去获取具体模板
            if ($val['formType'] == 'image') {
                $templateFile = "view{$this->DS}module{$this->DS}image";
            } elseif ($val['formType'] == 'images') {
                $templateFile = "view{$this->DS}module{$this->DS}images";
                $define = isset($val['define']) ? $val['define'] : ',';
            } elseif ($val['formType'] == 'file') {
                $templateFile = "view{$this->DS}module{$this->DS}file";
            } elseif ($val['formType'] == 'files') {
                $templateFile = "view{$this->DS}module{$this->DS}files";
                $define = isset($val['define']) ? $val['define'] : ',';
            } elseif ($val['formType'] == 'editor') {
                $templateFile = "view{$this->DS}module{$this->DS}editor";
            } elseif ($val['formType'] == 'date') {
                $templateFile = "view{$this->DS}module{$this->DS}date";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $val['define'];
                } else {
                    $define = 'datetime';
                }
                if (!in_array($define, ['year', 'month', 'date', 'time', 'datetime'])) {
                    $define = 'datetime';
                }
            } elseif ($val['formType'] == 'switch') {
                $templateFile = "view{$this->DS}module{$this->DS}switch";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildSwitchView($field, $val, '{if ' . $val['default'] . ' == "1"}checked{/if}');
                }
            } elseif ($val['formType'] == 'radio') {
                $templateFile = "view{$this->DS}module{$this->DS}radio";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildRadioView($field, '{in name="k" value="' . $val['default'] . '"}checked=""{/in}');
                }
            } elseif ($val['formType'] == 'checkbox') {
                $templateFile = "view{$this->DS}module{$this->DS}checkbox";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildCheckboxView($field, '{in name="k" value="' . $val['default'] . '"}checked=""{/in}');
                }
            } elseif ($val['formType'] == 'select') {
                $templateFile = "view{$this->DS}module{$this->DS}select";
                if (isset($val['bindRelation'])) {
                    $define = $this->buildOptionView($val['bindRelation']);
                } elseif (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildOptionView($field);
                }
            } elseif (in_array($field, ['remark']) || $val['formType'] == 'textarea') {
                $templateFile = "view{$this->DS}module{$this->DS}textarea";
            }

            $addFormList .= CommonTool::replaceTemplate(
                $this->getTemplate($templateFile),
                [
                    'comment' => $val['comment'],
                    'field' => $field,
                    'required' => $this->buildRequiredHtml($val['required']),
                    'value' => $val['default'],
                    'define' => $define,
                ]);
        }
        $viewAddValue = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}form"),
            [
                'formList' => $addFormList,
            ]);
        $this->fileList[$viewAddFile] = $viewAddValue;


        // 编辑页面
        $viewEditFile = "{$this->rootDir}app{$this->DS}admin{$this->DS}view{$this->DS}{$this->viewFilename}{$this->DS}edit.html";
        $editFormList = '';
        foreach ($this->tableColumns as $field => $val) {

            if (in_array($field, ['id', 'create_time'])) {
                continue;
            }

            $templateFile = "view{$this->DS}module{$this->DS}input";

            $define = '';
            $value = '{$row.' . $field . '|default=\'\'}';

            // 根据formType去获取具体模板
            if ($val['formType'] == 'image') {
                $templateFile = "view{$this->DS}module{$this->DS}image";
            } elseif ($val['formType'] == 'images') {
                $templateFile = "view{$this->DS}module{$this->DS}images";
            } elseif ($val['formType'] == 'file') {
                $templateFile = "view{$this->DS}module{$this->DS}file";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $val['define'];
                } else {
                    $define = 'mp4';
                }
            } elseif ($val['formType'] == 'files') {
                $templateFile = "view{$this->DS}module{$this->DS}files";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $val['define'];
                } else {
                    $define = 'mp4';
                }
            } elseif ($val['formType'] == 'editor') {
                $templateFile = "view{$this->DS}module{$this->DS}editor";
                $value = '{$row.' . $field . '|raw|default=\'\'}';
            } elseif ($val['formType'] == 'date') {
                $templateFile = "view{$this->DS}module{$this->DS}date";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $val['define'];
                } else {
                    $define = 'datetime';
                    $value = '{$row.' . $field . '|date=\'Y-m-d H:i:s\'|default=\'\'}';
                }
                if (!in_array($define, ['year', 'month', 'date', 'time', 'datetime'])) {
                    $define = 'datetime';
                    $value = '{$row.' . $field . '|date=\'Y-m-d H:i:s\'|default=\'\'}';
                }
            } elseif ($val['formType'] == 'switch') {
                $templateFile = "view{$this->DS}module{$this->DS}switch";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildSwitchView($field, $val, '{if $row.' . $field . ' == "1"}checked{/if}', true);
                }
            } elseif ($val['formType'] == 'radio') {
                $templateFile = "view{$this->DS}module{$this->DS}radio";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildRadioView($field, '{in name="k" value="$row.' . $field . '"}checked=""{/in}');
                }
            } elseif ($val['formType'] == 'checkbox') {
                $templateFile = "view{$this->DS}module{$this->DS}checkbox";
                if (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildCheckboxView($field, '{in name="k" value="$row.' . $field . '"}checked=""{/in}');
                }
            } elseif ($val['formType'] == 'select') {
                $templateFile = "view{$this->DS}module{$this->DS}select";
                if (isset($val['bindRelation'])) {
                    $define = $this->buildOptionView($val['bindRelation'], '{in name="k" value="$row.' . $field . '"}selected=""{/in}');
                } elseif (isset($val['define']) && !empty($val['define'])) {
                    $define = $this->buildOptionView($field, '{in name="k" value="$row.' . $field . '"}selected=""{/in}');
                }
            } elseif (in_array($field, ['remark'])) {//remark字段自动渲染为textarea  add.html
                $templateFile = "view{$this->DS}module{$this->DS}textarea";
                $value = '{$row.' . $field . '|raw|default=\'\'}';
            }

            $editFormList .= CommonTool::replaceTemplate(
                $this->getTemplate($templateFile),
                [
                    'comment' => $val['comment'],
                    'field' => $field,
                    'required' => $this->buildRequiredHtml($val['required']),
                    'value' => $value,
                    'define' => $define,
                ]);
        }
        $viewEditValue = CommonTool::replaceTemplate(
            $this->getTemplate("view{$this->DS}form"),
            [
                'formList' => $editFormList,
            ]);
        $this->fileList[$viewEditFile] = $viewEditValue;

        return $this;
    }

    /**
     * 初始化JS
     * @return $this
     */
    protected function renderJs()
    {
        $jsFile = "{$this->rootDir}public{$this->DS}static{$this->DS}admin{$this->DS}js{$this->DS}{$this->jsFilename}.js";

        $indexCols = "    {type: 'checkbox'},\r";
        // 主表字段
        foreach ($this->tableColumns as $field => $val) {

            if ($val['formType'] == 'image') {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}', templet: ea.table.image}";
            } elseif ($val['formType'] == 'images') {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}', templet: ea.table.image}";
                //continue;
            } elseif ($val['formType'] == 'file') {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}', templet: ea.table.url}";
            } elseif ($val['formType'] == 'files') {
                continue;
            } elseif ($val['formType'] == 'editor') {
                continue;
            } elseif ($val['formType'] == 'switch' || in_array($field, $this->switchFields)) {
                if (isset($val['define']) && !empty($val['define'])) {
                    $tips = implode("|", array_values(array_reverse($val['define'])));
                    $values = json_encode($val['define'], JSON_UNESCAPED_UNICODE);
                    $templateValue = "{field: '{$field}', search: 'select', selectList: {$values}, tips: '{$tips}', title: '{$val['comment']}', templet: ea.table.switch}";
                } else {
                    $templateValue = "{field: '{$field}', title: '{$val['comment']}', templet: ea.table.switch}";
                }
            } elseif (in_array($val['formType'], ['select', 'checkbox', 'radio'])) {
                if (isset($val['define']) && !empty($val['define'])) {
                    $values = json_encode($val['define'], JSON_UNESCAPED_UNICODE);
                    if ($val['formType'] == 'checkbox') $templateValue = "{field: '{$field}', search: 'select', selectList: {$values}, title: '{$val['comment']}', templet: ea.table.text}";
                    else $templateValue = "{field: '{$field}', search: 'select', selectList: {$values}, title: '{$val['comment']}'}";
                } else {
                    $templateValue = "{field: '{$field}', title: '{$val['comment']}'}";
                }
            } elseif (in_array($val['formType'], ['badge'])) {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}', templet: ea.table.text}";
            } elseif (in_array($field, $this->sortFields)) {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}', edit: 'text'}";
                //增加时间戳字段的index渲染
            } elseif ($val['formType'] == 'date' && isset($val['define']) && in_array($val['define'], ['range'])) {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}', search: 'range', searchTip: ' - ', templet: ea.table.date}";
            } else {
                $templateValue = "{field: '{$field}', title: '{$val['comment']}'}";
            }

            $indexCols .= $this->formatColsRow("{$templateValue},\r");
        }

        // 关联表
        foreach ($this->relationArray as $table => $tableVal) {
            $table = CommonTool::lineToHump($table);
            foreach ($tableVal['tableColumns'] as $field => $val) {
                if ($val['formType'] == 'image') {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}', templet: ea.table.image}";
                } elseif ($val['formType'] == 'images') {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}', templet: ea.table.image}";
                    //continue;
                } elseif ($val['formType'] == 'file') {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}', templet: ea.table.url}";
                } elseif ($val['formType'] == 'files') {
                    continue;
                } elseif ($val['formType'] == 'editor') {
                    continue;
                } elseif ($val['formType'] == 'select') {
                    if (isset($val['define']) && !empty($val['define'])) {
                        $values = json_encode($val['define'], JSON_UNESCAPED_UNICODE);
                        $templateValue = "{field: '{$table}.{$field}', search: 'select', selectList: {$values}, title: '{$val['comment']}'}";
                    } else {
                        $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}'}";
                    }
                    //$templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}'}";
                } elseif (in_array($val['formType'], ['badge'])) {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}', templet: ea.table.text}";
                } elseif ($val['formType'] == 'switch' || in_array($field, $this->switchFields)) {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}', templet: ea.table.switch}";
                } elseif (in_array($field, $this->sortFields)) {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}', edit: 'text'}";
                    //增加时间戳字段的index渲染
                } elseif ($val['formType'] == 'date' && isset($val['define']) && in_array($val['define'], ['range'])) {
                    $templateValue = "{field: '{$field}', title: '{$val['comment']}', search: 'range', searchTip: ' - ', templet: ea.table.date}";
                } else {
                    $templateValue = "{field: '{$table}.{$field}', title: '{$val['comment']}'}";
                }

                $indexCols .= $this->formatColsRow("{$templateValue},\r");
            }
        }

        $indexCols .= $this->formatColsRow("{width: 250, title: '操作', templet: ea.table.tool},\r");

        $jsValue = CommonTool::replaceTemplate(
            $this->getTemplate("static{$this->DS}js"),
            [
                'controllerUrl' => $this->controllerUrl,
                'indexCols' => $indexCols,
            ]);
        $this->fileList[$jsFile] = $jsValue;
        return $this;
    }

    /**
     * 检测文件
     * @return $this
     */
    protected function check()
    {
        // 是否强制性
        if ($this->force) {
            return $this;
        }
        foreach ($this->fileList as $key => $val) {
            if (is_file($key)) {
                throw new FileException("文件已存在：{$key}");
            }
        }
        return $this;
    }

    /**
     * 开始生成
     * @return array
     */
    public function create()
    {
        $this->check();
        foreach ($this->fileList as $key => $val) {

            // 判断文件夹是否存在,不存在就创建
            $fileArray = explode($this->DS, $key);
            array_pop($fileArray);
            $fileDir = implode($this->DS, $fileArray);
            if (!is_dir($fileDir)) {
                mkdir($fileDir, 0775, true);
            }

            // 写入
            file_put_contents($key, $val);
        }
        return array_keys($this->fileList);
    }

    /**
     * 开始删除
     * @return array
     */
    public function delete()
    {
        $deleteFile = [];
        foreach ($this->fileList as $key => $val) {
            if (is_file($key)) {
                unlink($key);
                $deleteFile[] = $key;
            }
        }
        return $deleteFile;
    }

    /**
     * 检测字段后缀
     * @param $string
     * @param $array
     * @return bool
     */
    protected function checkContain($string, $array)
    {
        foreach ($array as $vo) {
            if (strrchr($string, $vo) == $vo) {
                return true;
            }
        }
        return false;
    }

    /**
     * 格式化表单行
     * @param $value
     * @return string
     */
    protected function formatColsRow($value)
    {
        return "                    {$value}";
    }

    /**
     * 获取对应的模板信息
     * @param $name
     * @return false|string
     */
    protected function getTemplate($name)
    {
        return file_get_contents("{$this->dir}{$this->DS}templates{$this->DS}{$name}.code");
    }

}