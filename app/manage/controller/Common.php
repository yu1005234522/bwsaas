<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller;

use buwang\base\AdminBaseController;
use buwang\util\File;
use buwang\util\Util;
use app\manage\model\City;

class Common extends AdminBaseController
{
    /**
     * 刷新验证码
     * @return string
     */
    public function captcha()
    {
        return $this->success(['src' => captcha_src()]);
    }

    /**
     * 清除缓存
     * @return \think\response\Json
     */
    public function clearCache()
    {
        Util::clear_sys_cache();

        $root = app()->getRootPath() . 'runtime' . '/';
        $cache = [];

        if (is_dir($root)) $cache[$root] = scandir($root);
        foreach ($cache as $p => $list) {
            foreach ($list as $file) {
                if (!in_array($file, ['.', '..', 'log', 'schema', 'route.php'])) {
                    $path = $p . '/' . $file;
                    if (is_file($path)) @unlink($path);
                    else File::del_dir($path . '/');
                }
            }
        }
        return json(['code' => 1, 'msg' => '服务端清理缓存成功']);
    }


    /**
     * @NodeAnotation(title="添加区域页面")
     */
    public function map()
    {
        $this->layout && $this->app->view->engine()->layout($this->layout);
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            return $this->success('获取成功');
        }
        $get = $this->request->get();
        return view('', ['data' => $get]);
    }

    /**
     * @NodeAnotation(title="百度地图定位")
     */
    public function baiduMap(){
        $this->layout && $this->app->view->engine()->layout($this->layout);
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            return $this->success('获取成功');
        }
        $get = $this->request->get();
        return view('', ['data' => $get]);
    }

    /**
     * 城市信息
     */
    public function cityInfo(){
        $lon = $this->request->get('lon');
        $lat = $this->request->get('lat');
        $ak  = $this->request->get('ak');
        $location = $lon.','.$lat;
        $pois = 1;
        $output='json';
        $baidu_api='http://api.map.baidu.com/geocoder/v2/?ak=jhPGtfGrTzyoMNBRi1PVtkODgfelXofF&output=json&pois=1&location=34.247986,108.716148';
        $res = \buwang\util\Http::getRequest($baidu_api, compact('ak', 'location','pois','output'));
        $result = json_decode($res, true) ?: false;
        if(!$result)return $this->error('查询失败');
       return $this->success('查询成功', $result);
    }


    /**
     * @NodeAnotation(title="得到地区数据")
     */
    public function getMapData()
    {
        return $this->success('查询成功', City::getCityData());
    }


}
