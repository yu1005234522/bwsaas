<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use app\common\model\member\Wallet;
use app\common\model\member\WalletRecharge;
use buwang\base\MemberBaseController;
use buwang\facade\WechatPay;
use dh2y\qrcode\QRcode;
use think\facade\View;

/**
 * @ControllerAnnotation(title="租户充值")
 */
class Recharge extends MemberBaseController
{
    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
    }

    public function index()
    {
        $money = Wallet::where('member_id', $this->uid)->value('money') ?: 0;
        View::assign('money', $money);
        return view();
    }

    public function pay()
    {
        if ($this->request->isPost()) {
            $pay_type = $this->request->post('pay_type/s');
            $money = $this->request->post('money/f');

            if (!in_array($pay_type, ['wechat', 'alipay'])) return $this->error('支付方式有误');
            if($money < 10) return $this->error('充值金额最低为10元');

            $member = $this->user;

            $member_id = $member['id'];
            $order_sn = get_uuid();
            $type = $pay_type == 'wechat' ? 0 : 1;

            $res = WalletRecharge::create(compact('member_id', 'order_sn', 'money', 'type'));
            if (!$res) return $this->error('充值记录创建失败');

            if ($pay_type == 'wechat') {
                $order = [
                    'trade_type' => 'NATIVE',
                    'body' => '充值',
                    'out_trade_no' => $order_sn,
                    'total_fee' => $money * 100,//分
                    'notify_url' => (string)url('/manage/recharge/notify', [], false, true),
                ];
                $wechat = WechatPay::doPay();
                //判断错误返回
                if (is_array($wechat)) {
                    return $this->error($wechat['msg']);
                }
                $result = $wechat->order->unify($order);
                //订单创建失败
                if ($result['return_code'] != 'SUCCESS' || $result['result_code'] != 'SUCCESS') return $this->error('微信订单创建失败');

                $code = new QRcode();
                $code_url = $code->png($result['code_url'], '')->getPath(); //获取二维码生成的地址
            } else return $this->error('不支持的支付方式');
            $data = [
                'code_url' => $code_url,
                'out_trade_no'  =>  $order_sn,
                'total_fee' =>  $money,
            ];
            return $this->success('提交成功', $data);
        }
    }
}