<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\service\cloud;

use buwang\traits\ErrorTrait;
use GuzzleHttp\Client;

abstract class Base
{
    use ErrorTrait;

    //阿里云市场调用接口所需的appCode
    protected $appCode;

    //服务请求网址
    protected $base_uri;
    //请求类型
    protected $method = 'GET';
    //请求超时时间
    protected $timeout = 10;
    //请求配置
    protected $options;

    //请求客户端
    protected $client;

    public function __construct()
    {
        $this->appCode = bw_config('aliapi.AppCode');

        $this->options = [
            'headers' => [
                'Authorization' => "APPCODE {$this->appCode}",
                'Accept' => 'application/json',
            ]
        ];
        $this->client = new Client([
            'base_uri' => $this->base_uri,
            'timeout' => $this->timeout,
        ]);
    }

    abstract public function run(array $param);
}