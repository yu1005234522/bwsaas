<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use buwang\base\AdminBaseController;
use think\facade\Filesystem;
use app\manage\model\ConfigGroup;
use app\manage\model\ConfigGroupData;
use app\manage\model\Token;
use buwang\base\MemberBaseController;
use buwang\service\UserService;

class GroupData extends MemberBaseController
{
    /*protected $middleware = [
        'login' => ['except' => []]
    ];*/

    public function index($type = '',$app = null)
    {

        $id =  $type;
        //得到登录用户身份
        $scopes = 'member';
        $user = $this->user;
        $member_id = $user['top_id'];
        $id = $id ?: $this->request->get('group_id');
        $app = $app ?: $this->request->get('app');
        $configGroup = ConfigGroup::where('config_name', $id)->where('scopes', $scopes);
        if($app)$configGroup = $configGroup->where('dir', $app);
        $ConfigGroup = $configGroup->find();
        if ($ConfigGroup) $whereId = $ConfigGroup['id']; else $whereId = $id;

        $data = ConfigGroupData::getShowData($whereId, null, $member_id, $scopes,$app);
        if (!$data) return '只能查看自己的数据';
        return view('', ['show' => $data, 'group_id' => $id,'app'=>$app]);
    }


    /**
     * 列表
     *
     * @return \think\Response
     */
    public function getConfigList()
    {

        //得到登录用户身份
        $scopes = 'member';
        $user = $this->user;
        $member_id = $user['top_id'];

//        var_dump(111111);die;
        //搜索条件
        $map = array();
        $map[] = ['a.member_id', '=', $member_id];
        $map[] = ['a.scopes', '=', $scopes];
        $sort = 'a.sort desc,a.id desc';
        //页数
        $page = input('page/d');
        //条数
        $limit = input('limit/d');
        //时间开始
        $time_start = input('beginTime/s');
        //时间结束
        $time_end = input('endTime/s');
        //订单状态
        $id = input('id/d') ?: null;
        $group_id = input('group_id/s');
        $app = input('app/s');
        //手机号
        $status = input('status/d') ?: null;


        //订单状态
        if ($id !== null) {
            $map[] = ['a.id', '=', $id];
        }


        if ($status !== null) {
            $map[] = ['a.status', '=', $status];
        }


        //时间区间
        if ($time_start != null) {
            $time_start = strtotime($time_start);
            $map[] = ['a.add_time', '>=', $time_start];
            if ($time_end != null) {
                $time_end = strtotime($time_end);
                $map[] = ['a.add_time', '<=', $time_end];
            }
        } elseif ($time_end != null) {
            $time_end = strtotime($time_end);
            $map[] = ['a.add_time', '<=', $time_end];
            if ($time_start != null) {
                $time_start = strtotime($time_start);
                $map[] = ['a.add_time', '>=', $time_start];
            }
        }
        $configGroup = ConfigGroup::where('config_name', $group_id)->where('scopes', $scopes);
        if($app)$configGroup = $configGroup->where('dir',$app);
        //取出组合数据字段值
        $ConfigGroup = $configGroup->find();
        if ($ConfigGroup) {
            $map[] = ['a.group_id', '=', $ConfigGroup['id']];
        } else {
            $map[] = ['a.group_id', '=', $group_id];
            $ConfigGroup = ConfigGroup::where('scopes', $scopes)->find($group_id);
        }
        if (!$ConfigGroup) return json([
            'code' => 1,
            'msg' => '不存在该数据组',
            'count' => 0,
            'data' => [],
        ]);

        $fields = $ConfigGroup['fields'];
        $configs = json_decode($fields, true);//json串传数组
        //门店列表
        $list = ConfigGroupData::alias('a')
            ->field(['a.*'])
            ->where($map)
            ->where('member_id',$member_id)
            ->page($page, $limit)
            ->order($sort)
            ->select();


        foreach ($list as &$obj) {
            $value = $obj['value'];
            unset($obj['value']);
            $value = json_decode(htmlspecialchars_decode($value), true);

            foreach ($configs as $config) {
                if ($value) {
                    if (isset($value[$config['config_name']])) {
                        if ($config['type'] == 'upload') {

                            $r_textarea = base64_decode($value[$config['config_name']]['value']);
                            //var_dump($r_textarea);die;
                            $value[$config['config_name']]['value'] = json_decode(htmlspecialchars_decode(urldecode($r_textarea)), true);
                        }


                        $obj[$config['config_name']] = $value[$config['config_name']]['value'];
                    } else {
                        $obj[$config['config_name']] = null;
                    }
                } else {
                    $obj[$config['config_name']] = null;
                }
            }

        }
        //echo Db::name('offline_order')->getLastSql();die;//打印最后一条
        //echo Db::table('contract')->getLastSql();die;//打印最后一条sql
        $count = ConfigGroupData::alias('a')
            ->where($map)
            ->count();

        //总数
        //列表数据
        $data = array();
        $data['code'] = 0;
        $data['msg'] = '查询成功';
        $data['count'] = $count;
        $data['data'] = $list;
        return json($data);
    }

    public function add()
    {

        //得到登录用户身份
        $scopes = 'member';
        $user = $this->user;
        $member_id = $user['top_id'];


        $group_id = $this->request->get('group_id');
        $app = $this->request->get('app');
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $res = ConfigGroupData::setValues($param, $member_id, $scopes, true);
            if (!$res) return $this->error(ConfigGroupData::getError('设置失败'));
            return $this->success('添加成功');
        }
        //var_dump($id);die;
        $ConfigGroup = ConfigGroup::where('config_name', $group_id)->where('scopes', $scopes)->find();
        if ($ConfigGroup) $data = ConfigGroupData::getShowData($ConfigGroup['id'], null, $member_id, $scopes); else $data = ConfigGroupData::getShowData($group_id, null, $member_id, $scopes);

        return view('edit', ['entity' => null, 'show' => $data, 'group_id' => $group_id, 'app' => $app]);
    }


    public function edit()
    {
        //得到登录用户身份
        $scopes = 'member';
        $user = $this->user;
        $member_id = $user['top_id'];
        $group_id = $this->request->get('group_id');

        $app = $this->request->get('app');
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $res = ConfigGroupData::setValues($param, $member_id, $scopes, true);
            if (!$res) return $this->error(ConfigGroupData::getError('设置失败'));
            return $this->success('更新成功');
        }
        $id = $this->request->get('id');

        $data = ConfigGroupData::find($id);
        $ConfigGroup = ConfigGroup::where('config_name', $group_id)->where('scopes', $scopes)->find();
        if ($ConfigGroup) $ShowData = ConfigGroupData::getShowData($ConfigGroup['id'], $data, $member_id, $scopes); else $ShowData = ConfigGroupData::getShowData($group_id, $data, $member_id, $scopes);

        return view('', ['entity' => $data, 'show' => $ShowData, 'group_id' => $group_id, 'app' => $app]);
    }


    /**
     * 删除/批量删除
     *
     * @return \think\Response
     */
    public function softdleting()
    {

        //门店ids
        $ids = input('ids/s');
        $ids_array = explode(',', $ids);
        $list = array();
        foreach ($ids_array as $key) {
            //如果存在子配置则不允许删除
//            if(ConfigTab::haveConfig($key)){
//                return 'paramFail';
//            }


            ConfigGroupData::destroy($key);
        }

        //Offline::destroy($ids_array);
        return 'success';
    }


    /**
     * 更改开启状态
     *
     * @return \think\Response
     */
    public function setconfigshow()
    {
        $id = input('id/d');
        $data = ConfigGroupData::find($id);
        if ($data['status'] === 1) {
            $data['status'] = 2;
        } elseif ($data['status'] === 2) {
            $data['status'] = 1;
        };
        $data->save();
        return 'success';
    }


}