define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'CrudDemo/index',
        add_url: 'crudDemo/add',
        edit_url: 'mall.goods/edit',
        delete_url: 'mall.goods/delete',
        export_url: 'mall.goods/export',
        modify_url: 'mall.goods/modify',
        stock_url: 'mall.goods/stock',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['refresh',
                    [{
                        text: '添加记录',
                        url: init.add_url,
                        method: 'open',
                        auth: 'add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus ',
                        extend: 'data-full="true"',
                    }],
                    'delete', 'export'],
                cols: [[
                    {type:'checkbox', fixed: 'left'},
                    {field: 'id', title: 'ID', align: 'center', width: 80, fixed: 'left'},
                    {field: 'name', title: '名称', align: 'center', minWidth: 150},
                    {field: 'image', title: '单图', align: 'center', width: 60, search: false, templet: ea.table.image},
                    {field: 'images', title: '多图', align: 'center', width: 150, search: false, templet: ea.table.image},
                    {field: 'sales', title: '销量', align: 'center', width: 80, search: 'between',searchOp:'='},
                    {field: 'switch', title: '开关', align: 'center', width: 100, search: false, templet: ea.table.switch},
                    {field: 'pay_time', title: '支付时间', align: 'center', width: 160, search: false, templet: ea.table.date},
                    {field: 'status', title: '状态', align: 'center', search: 'select', selectList: {0: '禁用', 1: '启用'}, tips:'启用|禁用', width: 100, templet: ea.table.switch},
                    {field: 'create_time', title: '创建时间', align: 'center', width: 160, search: false, templet: ea.table.date},
                    {field: 'update_time', title: '更新时间', align: 'center', width: 160, search: false, templet: ea.table.date},
                    {field: 'send', title: '发放时间', align: 'center', width: 105, search: 'range', searchTip: ' - '},
                    {field: 'fruit', title: '水果', align: 'center', width: 190, templet: ea.table.text},
                    {field: 'interest', title: '爱好', align: 'center', width: 190, templet: ea.table.text},
                    {field: 'cate_id', title: '分类', align: 'center', width: 80},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '编辑',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                            }, {
                                text: '入库',
                                url: init.stock_url,
                                method: 'open',
                                auth: 'stock',
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                            }],
                            'delete']
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        stock: function () {
            ea.listen();
        },
    };
    return Controller;
});