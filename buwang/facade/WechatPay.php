<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\facade;

use think\Facade;

/**
 * @method static bwUnifiedOrder(array $data, bool $official = false, string $sub_mch_id = NULL)
 * @method static doPay(int $bw_member_app_id = 0, bool $official = false, string $sub_mch_id = NULL, array $extendConfig = [])
 */
class WechatPay extends Facade
{

    protected static function getFacadeClass()
    {
        return 'buwang\util\wechat\WechatPay';
    }

}