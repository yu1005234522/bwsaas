<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app;

use buwang\service\WechatNotifyService;
use think\Service;
use buwang\util\apiReturn;
use buwang\service\UserService;
use buwang\service\SystemService;
use buwang\service\CacheService;

/**
 * 应用服务类
 */
class AppService extends Service
{

    // 服务注册
    public $bind = [
        'json' => apiReturn::class,
        'userService' => UserService::class,
        'systemService' => SystemService::class,
        'wechatNotify' => WechatNotifyService::class,
        'cacheService' => CacheService::class,
    ];


    public function boot()
    {
        // 服务启动
    }
}
