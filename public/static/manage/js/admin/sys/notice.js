define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/sys/notice/index',
        add_url: 'admin/sys/notice/add',
        edit_url: 'admin/sys/notice/edit',
        delete_url: 'admin/sys/notice/del',
        export_url: 'admin/sys/notice/export',
        modify_url: 'admin/sys/notice/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'title', title: '标题'},
                    {field: 'status', title: '状态', templet: ea.table.switch},
                    {field: 'add_time', title: '发布时间',templet: ea.table.date},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});