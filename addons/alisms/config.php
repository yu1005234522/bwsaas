<?php

return array (
    'tips'=>[
        'name'=>'tips',
        'title'=>'温馨提示:',
        'type'=>'string',
        'value'=>'该提示将出现的插件配置头部，通常用于提示和说明'
    ],
  'key' =>
  array (
    'name' => 'key',
    'title' => '应用key',
    'type' => 'string',
    'content' =>
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'secret' =>
  array (
    'name' => 'secret',
    'title' => '密钥secret',
    'type' => 'string',
    'content' =>
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'sign' =>
  array (
    'name' => 'sign',
    'title' => '签名',
    'type' => 'string',
    'content' =>
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'template' =>
  array (
    'name' => 'template',
    'title' => '短信模板',
    'type' => 'string',
    'content' =>
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
