define(["jquery", "easy-admin"], function ($, ea, Vue) {

    var form = layui.form;

    var Controller = {
        index: function () {
            // var app = new Vue({
            //     el: '#app',
            //     data: {
            //         upload_type: 'upload_type'
            //     }
            // });
            //
            // form.on("radio(upload_type)", function (data) {
            //     app.upload_type = this.value;
            // });

            ea.listen(
                function (data) {
                    return data;
                },
                function (res) {
                    layer.open({
                        type: 1
                        ,title: '充值余额' //不显示标题栏
                        ,closeBtn: false
                        ,area: '300px;'
                        ,shade: 0.2
                        ,id: 'pay' //设定一个id，防止重复弹出
                        ,btn: ['支付成功', '支付失败']
                        ,btnAlign: 'c'
                        ,moveType: 1 //拖拽模式，0或者1
                        ,content: '<div style="display: flex;justify-content: center;"><img src="' + res.data.data.code_url + '"></div>'
                        ,yes: function(index, layero){
                            layer.close(index);
                            setTimeout(function () {
                                location.reload();
                            }, 1000)
                        }
                    });
                }
            );
        }
    };
    return Controller;
});