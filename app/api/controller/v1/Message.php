<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\api\controller\Basic;

/**
 * 站内信
 * Class Message
 * @package app\api\controller\v1
 */
class Message extends Basic
{

    protected function initialize()
    {
        parent::initialize();
        $this->isUserAuth();
    }

    /**
     * 消息列表
     */
    public function index()
    {
        $type = request()->get('type/d', 0);//type 3=租户-》用户
        $page = request()->get('page/d', 1);
        $limit = request()->get('limit/d', 10);

        $user = $this->user;//登录用户

        $list = \app\common\model\Message::list($type, $user['id'], $page, $limit);
        return $this->success('success', compact('list'));
    }

    /**
     * 消息已读
     * @param int $id 消息id
     * @return \think\Response
     */
    public function read($id)
    {
        \app\common\model\Message::read($id);
        return $this->success('success');
    }

    /**
     * 全部消息已读
     * @return \think\Response
     */
    public function readAll()
    {
        if ($this->request->isPost()) {
            $type = $this->request->post('type/d', 2);
            \app\common\model\Message::readAll($type, $this->user->id);
            return $this->success('success');
        }
    }
}