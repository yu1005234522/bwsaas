<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use buwang\base\MemberBaseController;
use think\facade\View;
use app\manage\model\ConfigTab;
use app\manage\model\Config as ConfigModel;
use app\manage\model\Member;
use app\manage\model\AuthNode;

class Config extends MemberBaseController
{
    /*protected $middleware = [
        'login' => ['except' => []]
    ];*/
    protected $saveDataUid = 0;


    public function showMemberConfig($tab_name = null, $type = null, $config_ids = null,$app =null)
    {
        $tab_name = $tab_name ?: ($this->request->get('tab_name') ?: null);//配置分类
        $type = $type ?: ($this->request->get('type') ?: null);//配置类型
        $config_ids = $config_ids ?: ($this->request->get('config_ids') ?: null);//配置id
        $app = $app ?: ($this->request->get('app') ?: null);//app标识
        $data = ConfigModel::getShowData($tab_name, $type, null, 'member', $config_ids,$app);
        //  var_dump($data);die;
        return view('', ['show' => $data,'app'=>$app]);
    }


    public function setMemberValues()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $res = ConfigModel::setValues($param, null, 'member', true);
            if (!$res) return $this->error(ConfigModel::getError('设置失败'));
            return $this->success('更新成功');
        }

    }


}
