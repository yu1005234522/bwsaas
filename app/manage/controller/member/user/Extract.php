<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member\user;

use buwang\base\MemberBaseController;

//use EasyAdmin\annotation\ControllerAnnotation;
//use EasyAdmin\annotation\NodeAnotation;
//use think\App;
use app\common\model\MemberMiniapp;

/**
 * @ControllerAnnotation(title="用户提现表")
 */
class Extract extends MemberBaseController
{
    use \buwang\traits\Crud;

    protected $relationSearch = true;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\UserExtract();

        $this->assign('getUserList', $this->model->getUserList());

    }


    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            //查询该租户的所有应用
            $ids = MemberMiniapp::where('member_id', $this->user->top_id)->column('id');
            if (!$ids) return $this->success('success', ['total' => 0, 'list' => []]);

            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->withJoin('user', 'LEFT')
                ->where($where)
                ->where('user.member_miniapp_id', 'in', $ids)
                ->count();
            $list = $this->model
                ->withJoin('user', 'LEFT')
                ->where($where)
                ->where('user.member_miniapp_id', 'in', $ids)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        return $this->fetch();
    }
}