<?php
$dir = ADDONS_DIR;
return [
    [
        //角色名称/功能名称
        'name' => $dir . '门店功能',
        //角色唯一标识,不可重复
        'group_name' => 'shop',
        //角色备注/功能描述
        'remark' => '',
        'parent' => '',//父节点唯一标识
        //角色拥有的节点
        'nodes' => [
            [
                "title" => '首页',
                //菜单url
                "menu_path" => '/addons/' . $dir . '/index/index',
                //实际
                "name" => '/addons/' . $dir . '/Index/index',
                //权限标识,必填 唯一
                "auth_name" => '',
                //附加参数 ?id=1&name=demo
                "param" => '',
                //打开方式
                "target" => '_self',
                //是否菜单 1=是,0=否
                "ismenu" => '1',
                //图标
                "icon" => 'fa fa-file',
                //备注
                "remark" => '',
                //子节点
                'children' => []
            ],
            [
                "title" => '登录页',
                //菜单url
                "menu_path" => '/addons/' . $dir . '/index/login',
                //实际
                "name" => '/addons/' . $dir . '/Index/login',
                //权限标识,必填 唯一
                "auth_name" => '',
                //附加参数 ?id=1&name=demo
                "param" => '',
                //打开方式
                "target" => '_self',
                //是否菜单 1=是,0=否
                "ismenu" => '1',
                //图标
                "icon" => 'fa fa-file',
                //备注
                "remark" => '',
                //子节点
                'children' => []
            ],
        ]
    ]
];