
namespace <?php echo $model_namespace?>;

use buwang\base\BaseModel;

class <?php echo $model_class_name?> extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = '<?php echo $pk?>';

    /**
     * 模型名称
     * @var string
     */
    protected $name = '<?php echo $model_table_name?>';

<?php echo $relation_function?>
}