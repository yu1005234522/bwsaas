define(["easy-admin", "miniTab"], function (ea, miniTab) {
    var Controller = {
        userSetting: function () {
            $('#recharge').on('click',function(event){
                // 打开新的窗口
                miniTab.openNewTabByIframe({
                    href:"/manage/member/recharge",
                    title:"账户充值",
                });
                return false;
            });
            $('#wallet_bill').on('click',function(event){
                layer.open({
                    type: 2,
                    content: '/manage/member/walletBill',
                    area: ['90%', '90%']
                });

                return false;
            });
            ea.listen();
        },
    };
    return Controller;
});