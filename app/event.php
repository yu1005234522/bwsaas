<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

// 事件定义文件
return [
    'bind' => [
    ],

    'listen' => [
        'AppInit' => [
            'buwang\event\InitConfig',
        ],
        'HttpRun' => [],
        'HttpEnd' => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'RouteLoaded' => [
            'buwang\event\Miniapp',//设置应用表前缀
        ],
    ],

    'subscribe' => [
        buwang\subscribe\UserSubscribe::class,//用户事件订阅
        buwang\subscribe\SystemSubscribe::class,//系统事件订阅
        buwang\subscribe\WechatSubscribe::class,//微信系统事件订阅
    ],
];
