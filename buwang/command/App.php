<?php
declare (strict_types = 1);

namespace buwang\command;

use buwang\util\console\CliEcho;
use buwang\util\crud\BuildCrud;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use buwang\service\MiniappService;

class App extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('build')
            ->addOption('build', 'b', Option::VALUE_REQUIRED, '应用目录标识', null)
            ->setDescription('应用安装文件生成指令');
    }

    protected function execute(Input $input, Output $output)
    {
        $dir = $input->getOption('build');
        if(!$dir){
            CliEcho::error('缺少生成安装文件的应用目录标识参数 请使用命令：-b [目录标识] 或 --build [目录标识]');
            return false;
        }
        //判断是否存在该目录
        $group_php_path = base_path($dir);
        if (!file_exists($group_php_path)){
            CliEcho::error($group_php_path.'应用目录不存在');
            return false;
        }
       $array = MiniappService::exportPath($dir);
        $output->info("生成应用安装文件：");
        $output->info(">>>>>>>>>>>>>>>");
        foreach ($array as $key => $val) {
            $output->info($val);
        }
        $output->info(">>>>>>>>>>>>>>>");
        $output->info("确定生成上方所有文件? 如果以上文件存在会直接覆盖。 请输入 'y' 按回车键继续操作: ");
        $line = fgets(defined('STDIN') ? STDIN : fopen('php://stdin', 'r'));
        if (trim($line) != 'y') {
            CliEcho::error('取消生成应用安装文件');
            return false;
        }
        try{
            MiniappService::exportInstallDir($dir);
        }catch (\Exception $s){
            CliEcho::error($s->getMessage());
            return false;
        }

        CliEcho::success("{$dir}应用安装文件生成完毕！");
    }
}
