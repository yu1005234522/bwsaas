define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'uploadfile/index',
    };

    var Controller = {

        config: function () {
            ea.listen();
        },
    };
    return Controller;
});