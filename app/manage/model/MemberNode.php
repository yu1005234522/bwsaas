<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\model;

use buwang\base\BaseModel;

/**
 * 租户权限表
 * Class MemberNode
 * @package app\manage\model
 */
class MemberNode extends BaseModel
{
    protected $pk = 'id';
    // 表名
    protected $name = 'MemberNode';

    public static function getlist($member_id)
    {
        //查询租户拥有的权限
        $list = self::field(['b.*'])
            ->alias('a')
            ->join('bw_auth_node b', 'b.id = a.node_id')
            ->where('a.member_id', $member_id)
            ->order('b.sort asc, b.id asc')
            ->select()->toArray();
        //树枝节点成为顶级节点
        foreach ($list as &$value) {
            $rs = MemberNode::where('member_id', $member_id)->where('node_id', $value['pid'])->find();
            if (!$rs) $value['pid'] = 0;
        }
        return $list;
    }

}