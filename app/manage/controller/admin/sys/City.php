<?php

namespace app\manage\controller\admin\sys;

use buwang\base\AdminBaseController;
//use EasyAdmin\annotation\ControllerAnnotation;
//use EasyAdmin\annotation\NodeAnotation;
//use think\App;

/**
 * @ControllerAnnotation(title="城市表")
 */
class City extends AdminBaseController
{
    use \buwang\traits\Crud;
    
    
    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\system\SysCity();
        
        $this->assign('getIsShowList', $this->model->getIsShowList());

    }

    /**下载基础sql
     * @return \think\response\File
     */
    public function download()
    {
        $template_image = root_path().DS.'app'.DS.'manage'.DS.'source'.DS.'city'.DS.'datebase.sql';
        return download($template_image, '城市数据.sql');
    }

    
}