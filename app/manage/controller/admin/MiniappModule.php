<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\admin;

use app\manage\model\AuthGroup;
use buwang\base\AdminBaseController;
use buwang\util\Caches;
use think\facade\View;

/**
 * @ControllerAnnotation(title="应用功能表")
 */
class MiniappModule extends AdminBaseController
{
    use \buwang\traits\Crud;


    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\manage\model\admin\MiniappModule();

        $this->assign('getTypeList', $this->model->getTypeList());

    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();

            $module_miniapp_id = Caches::get('module_miniapp_id');
            !$module_miniapp_id && $module_miniapp_id = 0;
            $where[] = ['miniapp_id', '=', $module_miniapp_id];

            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }

        $miniapp_id = request()->param('miniapp_id/d', 0);
        Caches::set('module_miniapp_id', $miniapp_id);

        return view();
    }

    /**
     * @NodeAnotation(title="添加")
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [];
            $this->validate($post, $rule);
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败:' . $e->getMessage());
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }

        //附加应用ID
        $miniapp_id = Caches::get('module_miniapp_id');
        !$miniapp_id && $miniapp_id = 0;
        View::assign('miniapp_id', $miniapp_id);

        return view();
    }

    /**
     * 角色组
     */
    public function group(){
        $miniapp_id = $this->request->get('miniapp_id/d', 0);

        $fields = input('selectFieds');
        $data = AuthGroup::where($this->selectWhere)
            ->alias('group')
            ->join('miniapp','group.app_name = miniapp.dir')
            ->where('miniapp.id', $miniapp_id)
            ->field($fields)->select();
        foreach ($data as &$item) {
            $item['group.id'] = $item['id'];
            $item['group.name'] = $item['name'];
            unset($item['id']);
            unset($item['name']);
        }
        return $this->success('ok', $data);
    }
}