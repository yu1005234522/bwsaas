define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/bill/index',
        add_url: 'member/user/bill/add',
        edit_url: 'member/user/bill/edit',
        delete_url: 'member/user/bill/del',
        export_url: 'member/user/bill/export',
        modify_url: 'member/user/bill/modify',
    };

    function getgetMemberMiniappList() {
        var miniappList = {};
        var select_name = $('#select_name').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }

    function getTypes() {
        var miniappList = {};
        var select_name = $('#select_type').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }

    function getCategorys() {
        var miniappList = {};
        var select_name = $('#select_category').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }




    var Controller = {
        index: function () {
            var memberMiniappList = getgetMemberMiniappList();//应用搜索列表
            var types = getTypes();//类型
            var categorys = getCategorys()//币种
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: '用户账单id'},
                    {field: 'uid', title: '用户ID',minWidth:150},
                    {field: 'user.mobile', title: '手机号',hide:true,minWidth:150},
                    {field: 'user.nickname', title: '昵称',hide:true,minWidth:150},
                    {field: 'user.avatar', title: '头像', templet: ea.table.image,minWidth:150},
                    // {field: 'link_id', title: '关联id',minWidth:150},
                    {field: 'pm', search: 'select', selectList: ["支出","获得"], title: '收支', templet: '#pm_templet' ,minWidth:150},
                    {field: 'title', title: '账单标题',minWidth:150},
                    {field: 'category', search: 'select', selectList: categorys, title: '明细币种',minWidth:150},
                    {field: 'type', title: '明细类型',search: 'select', selectList: types,minWidth:150},
                    {field: 'number', title: '明细数字',templet: '#number_templet',minWidth:150},
                    {field: 'balance', title: '剩余',minWidth:150},
                    {field: 'mark', title: '备注',minWidth:150},
                    {field: 'status', search: 'select', selectList: {"0":"待确认","1":"有效","-1":"无效"}, title: '状态',minWidth:150},
                    {field: 'appname', search: 'select',fieldAlias:'memberMiniapp.id', selectList: memberMiniappList, title: '应用名称',minWidth:150},
                    {field: 'head_img', title: 'Logo',fieldAlias:'memberMiniapp.head_img', templet: ea.table.image,hide:true,minWidth:150},
                    {field: 'miniapp_head_img', title: '小程序logo',fieldAlias:'memberMiniapp.miniapp_head_img', templet: ea.table.image,minWidth:150},
                    {field: 'mp_head_img', title: '公众号logo',fieldAlias:'memberMiniapp.mp_head_img', templet: ea.table.image,minWidth:150},
                    {field: 'add_time', title: '添加时间',minWidth:150,templet: ea.table.date},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});