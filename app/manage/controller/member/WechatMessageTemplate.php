<?php

namespace app\manage\controller\member;

use buwang\base\MemberBaseController;
use think\facade\View;

/**
 * 微信消息模板
 * Class WechatMessageTemplate
 * @package app\manage\controller\member
 */
class WechatMessageTemplate extends MemberBaseController
{
    use \buwang\traits\Crud;

    /**
     * 允许修改的字段
     * @var array
     */
    protected $allowModifyFileds = [
        'status',
    ];

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\WechatMessageTemplate();
        $this->assign('getTypeList', $this->model->getTypeList());
        $this->assign('getStatusList', $this->model->getStatusList());
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->where($where)
                ->where('member_miniapp_id', $this->member_miniapp_id)
                ->count();
            $list = $this->model
                ->where($where)
                ->where('member_miniapp_id', $this->member_miniapp_id)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        return view();
    }



    /**
     * @NodeAnotation(title="添加")
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [];
            $this->validate($post, $rule);
            $this->isValidMember($post['member_miniapp_id']);
            //同一租户应用内，模板标识不可重复
            if($this->model->where('member_miniapp_id', $post['member_miniapp_id'])->where('type', $post['type'])->where('key', $post['key'])->find()) return $this->error('模板标识已存在');
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败:' . $e->getMessage());
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }
        return view();
    }

    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit($id)
    {
        $row = $this->model->find($id);
        if ($row->isEmpty()) return $this->error('数据不存在');
        $this->isValidMember($row['member_miniapp_id']);
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [];
            $this->validate($post, $rule);

            try {
                $save = $row->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败');
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }
        View::assign('row', $row);
        return view();
    }

    /**
     * @NodeAnotation(title="删除")
     */
    public function del($id)
    {
        $row = $this->model->whereIn('id', $id)->select();
        if ($row->isEmpty()) return $this->error('数据不存在');
        foreach ($row as $v){
            $this->isValidMember($v['member_miniapp_id']);
        }
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            return $this->error('删除失败');
        }
        if ($save) return $this->success('删除成功');
        else return $this->error('删除失败');
    }

    /**
     * @NodeAnotation(title="属性修改")
     */
    public function modify()
    {
        $post = $this->request->post();
        $rule = [
            'id|ID' => 'require',
            'field|字段' => 'require',
            'value|值' => 'require',
        ];
        $this->validate($post, $rule);
        $row = $this->model->find($post['id']);
        if (!$row) {
            return $this->error('数据不存在');
        }
        $this->isValidMember($row['member_miniapp_id']);
        if (!in_array($post['field'], $this->allowModifyFileds)) {
            return $this->error('该字段不允许修改：' . $post['field']);
        }
        try {
            $row->save([
                $post['field'] => $post['value'],
            ]);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success('保存成功');
    }
}