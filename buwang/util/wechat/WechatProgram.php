<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\util\wechat;

use app\common\model\MemberMiniapp;
use app\common\model\WechatOpenToken;
use EasyWeChat\Factory;

/**
 * 小程序独立配置和开放平台授权操作实例
 */
class WechatProgram
{
    /**
     * 获取小程序单独/开放平台操作实例
     * @param int $bw_member_app_id
     * @return value|\EasyWeChat\OpenPlatform\Authorizer\MiniProgram\Application|false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getWechatObj(int $bw_member_app_id)
    {
        try{
            $program = MemberMiniapp::where(['id' => $bw_member_app_id])->find();
            return empty($program->miniapp->is_openapp) ? self::MiniProgram($bw_member_app_id, $program) : self::openPlatformMiniProgram($bw_member_app_id, $program);
        } catch (\Throwable $e) {
            return false;
        }
    }

    /**
     * 微信小程序配置(独立应用)
     * @param integer $bw_member_app_id 来自用户应用ID
     * @param array $bw_member_app
     * @return value|\EasyWeChat\MiniProgram\Application
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function miniProgram(int $bw_member_app_id, $bw_member_app = [])
    {
        if (empty($bw_member_app)) {
            $bw_member_app = MemberMiniapp::where(['id' => $bw_member_app_id])->field('miniapp_appid,miniapp_secret')->find();
        }
        $config = [
            'app_id' => $bw_member_app['miniapp_appid'],
            'secret' => $bw_member_app['miniapp_secret'],
        ];
        return Factory::miniProgram($config);
    }

    /**
     * 获取微信小程序开放平台操作实例
     * @param integer $bw_member_app_id 租户应用ID
     * @param array $bw_member_app //当前租户小程序配置
     * @return \EasyWeChat\OpenPlatform\Authorizer\MiniProgram\Application|false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function openPlatformMiniProgram(int $bw_member_app_id, $bw_member_app = [])
    {
        if (empty($bw_member_app)) {
            $bw_member_app = MemberMiniapp::where(['id' => $bw_member_app_id])->field('miniapp_appid')->find();
            if (!$bw_member_app || empty($bw_member_app['miniapp_appid'])) {
                return false;
            }
        }
        if (empty($bw_member_app['miniapp_appid'])) {
            return false;
        }
        $refreshToken = WechatOpenToken::refreshToken($bw_member_app_id, $bw_member_app['miniapp_appid']);
        if (!$refreshToken) {
            return false;
        }
        //代小程序实现业务
        return (new WechatMp())->getOpenPlatform()->miniProgram($bw_member_app['miniapp_appid'], $refreshToken['refreshToken']);
    }
}