<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use buwang\base\MemberBaseController;
use app\manage\model\AuthGroupNode;
use app\manage\model\AuthGroup;
use app\manage\model\AuthNode;
use app\manage\model\Member;
use app\manage\model\AuthGroupAccess;

class Role extends MemberBaseController
{
    /*protected $middleware = [
        'login' => ['except' => []]
    ];*/


    /**
     * 查看
     * @menu true
     */
    public function index()
    {
        if (request()->isPost() || request()->isAjax()) {
            $member_id = $this->uid;

            $Member = Member::getTop($member_id);

            $list = AuthGroupNode::getMemberlist([['scopes', '=', 'member'], ['member_id', '=', $Member['id']]], $Member['id']);

            $count = count($list);
            //var_dump($list);die;
            $list = array_merge(AuthNode::tree($list, 'name', 'id'));

            return json([
                'code' => 0,
                'msg' => '查询成功',
                'count' => $count,
                'data' => $list,
            ]);

        }
        return view();
    }


    /**
     * 编辑管理组
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {

        $member_id = $this->uid;

        $Member = Member::getTop($member_id);
        if ($this->request->isPost()) {
            if ($member_id != $Member['id']) return $this->error('子租户无权操作');
            $param = $this->request->param();
            //父级不能是自己
            if (isset($param['pid'])) {
                if ($param['pid'] == $param['id']) {
                    return $this->error('父ID不能是自己');
                }
            }
            if (isset($param['group_name'])) {
                //判断是否存在唯一标识
                $count = AuthGroup::where('group_name', $param['group_name'])->where('id', 'not in', $param['id'])->count();
                if ($count) return $this->error('请换一个标识，该标识有人在使用');
            }
            if (isset($param['name'])) {
                //判断是否存在唯一标识
                $count = AuthGroup::where('name', $param['name'])->where('scopes', 'member')->where('member_id', '<>', 0)->where('id', 'not in', $param['id'])->count();
                if ($count) return $this->error('已存在该用户组名');
            }
            $res = AuthGroupNode::updateRule($param['id'], explode(",", $param['rule_ids']), true);
            if (!$res) return $this->error(AuthGroupNode::getError('操作失败'));

            $result = AuthGroup::update($param);
            //编辑中间表
            $AuthGroupNodes = AuthGroupAccess::where('group_id', $param['id'])->select()->toArray();
            if ($AuthGroupNodes) {
                $AuthGroup_data = array();
                foreach ($AuthGroupNodes as $AuthGroupNode) {
                    $GroupNode = array();
                    $GroupNode ['id'] = $AuthGroupNode['id'];
                    if (isset($param['name'])) {
                        $GroupNode['name'] = $param['name'];
                    }


                    $AuthGroup_data[] = $GroupNode;
                }
                $AuthGroupNode = new AuthGroupAccess;
                $AuthGroupNode->saveAll($AuthGroup_data);
            }
            if ($result) {
                //xn_add_admin_log('修改管理组名称');
                return $this->success('操作成功');
            } else {
                return $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $data = AuthGroup::find($id);
        $data['rule_ids'] = null;
        //查询所有权限，以逗号拼接
        $rules = AuthGroupNode::valiWhere()->where('group_id', $id)->column('node_id');
        if ($rules) $data['rule_ids'] = implode(",", $rules);

        // var_dump($list);die;
        return view('form', ['data' => $data, 'pid' => $data['pid']]);
    }


    /**
     * 添加管理组
     * @return \think\response\View
     */
    public function add()
    {
        $member_id = $this->uid;

        $Member = Member::getTop($member_id);

        if ($this->request->isPost()) {
            if ($member_id != $Member['id']) return $this->error('子租户无权操作');
            $param = $this->request->param();
            $param['group_name'] = md5(time());
            //判断是否存在唯一标识
            $count = AuthGroup::where('group_name', $param['group_name'])->count();
            if ($count) return $this->error('请换一个标识，该标识有人在使用');
            $count = AuthGroup::where('name', $param['name'])->where('scopes', 'member')->where('member_id', '<>', 0)->count();
            if ($count) return $this->error('已存在该用户组名');
            $result = new AuthGroup;
            $result['group_name'] = $param['group_name'];
            $result['name'] = $param['name'];
            $result['pid'] = $param['pid'];
            $result['status'] = $param['status'];
            $result['scopes'] = 'member';
            $result['member_id'] = $Member['id'];
            $result->save();
//            $result = AuthGroup::create($param);
            if (!$result) return $this->error('操作失败');
            $res = AuthGroupNode::updateRule($result['id'], explode(",", $param['rule_ids']), false);

            if (!$res) return (AuthGroupNode::getError('操作失败'));
            // xn_add_admin_log('添加管理组');
            return $this->success('操作成功');

        }
        $list = AuthGroup::where('scopes', 'member')->where('member_id', '<>', 0)->select()->toArray();
        $list = AuthNode::tree($list, 'name', 'id');
        return view('form', ['list' => $list, 'pid' => $this->request->get('pid')])->filter(function ($content) {
            return str_replace("&amp;emsp;", '&emsp;', $content);
        });
    }


    /**
     * 删除用户组
     */
    public function delete($ids = 0)
    {

        $member_id = $this->uid;

        $Member = Member::getTop($member_id);

        if (request()->isPost()) {
            if (!$ids) return $this->error('参数有误');
            $list = AuthGroup::where('id', 'in', $ids)->where('scopes', 'member')->select();
            if (!$list) return $this->error('记录不存在');
            if (AuthGroup::where('id', 'in', $ids)->where('member_id', '=', 0)->count()) $this->error('存在系统角色无法删除');
            if ($member_id != $Member['id']) return $this->error('子租户无权操作');

            try {
                $data = array();
                foreach ($list as $item) {

                    $child_count = AuthGroup::where('pid', $item['id'])->count();
                    if ($child_count) return $this->error('请先删除子节点');

                    AuthGroup::destroy($item['id']);
                    //删除角色权限中间表
                    $ids = AuthGroupNode::where('group_id', $item['id'])->column('id');
                    if ($ids) {
                        AuthGroupNode::destroy($ids);
                    }
                    //删除用户角色中间表
                    $ids = AuthGroupAccess::where('group_id', $item['id'])->column('id');
                    if ($ids) {
                        AuthGroupAccess::destroy($ids);
                    }
                }
            } catch (\Exception $e) {
                return $this->error('删除失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success('删除成功');
        }
    }


    /**
     * 得到用户组数据
     */
    public function getRoleTree()
    {
        $member_id = $this->uid;

        $Member = Member::getTop($member_id);

        $checked_ids = $this->request->param('checked_ids') ?: [];//初始化商品分类
        $disabled = $this->request->param('disabled') !== null ? $this->request->param('disabled') : true;//节点禁选
        $is_child = $this->request->param('is_child') ?: false;//是否只初始选中子节点
        $is_all = $this->request->param('is_all') ?: false;//是否展示全部用户组
        if ($disabled === 'false') $disabled = false;
        if ($disabled === 'true') $disabled = true;
        if ($is_child === 'false') $is_child = false;
        if ($is_child === 'true') $is_child = true;
        if ($is_all === 'false') $is_all = false;
        if ($is_all === 'true') $is_all = true;
        if ($is_all) {
            $where = [['scopes', '=', 'member']];
        } else {
            $where = [['scopes', '=', 'member'], ['member_id', '=', $Member['id']]];
        }
        return json(AuthGroup::getTree($where, 0, $checked_ids, $disabled, $is_child));
    }

    /**
     * 得到顶级租户拥有的全部节点数据
     */
    public function getNodeTree()
    {

        $auth_ids = config('auth.hidden_node_ids', []);//屏蔽的节点
        //得到屏蔽的所有子孙节点
        $auth_ids = AuthNode::getNodeChildIds($auth_ids);


        $member_id = $this->uid;
        $Member = Member::getTop($member_id);
        $pid = $this->request->param('pid') ?: 0;//初始化商品分类
        $checked_ids = $this->request->param('checked_ids') ?: [];//初始化商品分类
        $disabled = $this->request->param('disabled') !== null ? $this->request->param('disabled') : true;//节点禁选
        $is_child = $this->request->param('is_child') ?: false;//是否只初始选中子节点

        if ($disabled === 'false') $disabled = false;
        if ($disabled === 'true') $disabled = true;
        if (!$pid) {
            //得到拥有的系统角色
            //查询中间表得到拥有的全部角色
            $SystemGroupIds = AuthGroupAccess::valiWhere()->where('uid', $member_id)->where('scopes', 'member')->column('group_id');

            //过滤得到系统角色 （可注释掉不过滤）
            //if($SystemGroupIds)$SystemGroupIds= AuthGroup::valiWhere()->where('id','in',$SystemGroupIds)->where('status',1)->where('member_id',0)->where('scopes','member')->column('id');
            //查询角色拥有的所有权限
            $NodeIds = AuthGroupNode::where('group_id', 'in', $SystemGroupIds)->where('node_id', 'not in', $auth_ids)->column('node_id');
            $tree = AuthNode::getTree([['id', 'in', $NodeIds]], 0, $checked_ids, $disabled, $is_child);
            return json($tree);
        } else {
            //得到父角色权限
            $NodeIds = AuthGroupNode::where('group_id', 'in', (string)$pid)->where('node_id', 'not in', $auth_ids)->column('node_id');
            $tree = AuthNode::getTree([['id', 'in', $NodeIds]], 0, $checked_ids, $disabled, $is_child);
            return json($tree);
        }

    }


    /**
     * 更改开启状态
     *
     * @return \think\Response
     */
    public function setStatus()
    {
        $member_id = $this->uid;
        $Member = Member::getTop($member_id);
        if ($member_id != $Member['id']) return $this->error('子租户无权操作');
        if (request()->isAjax()) {
            $id = input('id/d');
            $data = AuthGroup::where('scopes', 'member')->where('member_id', $Member['id'])->find($id);
            if ($data['status'] === 0) {
                $data['status'] = 1;
            } elseif ($data['status'] === 1) {
                $data['status'] = 0;
            };
            $data->save();
            return $this->success('successful');

        }
    }


}
